<?php

$txt['spmod_copyright'] = 'Simple Purchases for SMF {version} | &copy; <a href="http://spmod.ru">spmod.ru</a>';
$txt['spmod_unknownver'] = 'unknown version';
$txt['spmod'] = 'SPmod';
$txt['spmod_title'] = 'Simple Purchases for SMF';
$txt['spmod_currency'] = ' USD';
$txt['spmod_itemcountlbl'] = ' pcs.';
$txt['spmenubutton_label'] = 'Simple Purchases';
$txt['spmod_spmod'] = 'Simple Purchases';
$txt['spmod_isguest'] = 'You are not allowed to access to this page';
$txt['spmod_capt'] = 'Categories and purchases management';
$txt['spmod_cats'] = 'Categories';
$txt['spmod_procurements'] = 'Purchases';
$txt['spmod_goods'] = 'Goods';
$txt['spmod_isguest'] = 'It isn\'t allowed guests join to Group Purchases. You need to register on the forum.';
$txt['spmod_lines'] = 'Lines and parameters';
$txt['spmod_line'] = 'Line';
$txt['spmod_lineisopened'] = 'Line is opened: ';
$txt['spmod_lineiscomplited'] = 'Line is complited: ';
$txt['spmod_parser'] = 'Parser';
$txt['spmod_catslist'] = 'Your categories list';
$txt['spmod_nocats'] = 'Your hadn\'t created categories list yet.';
$txt['spmod_catname'] = 'Catalogue name';
$txt['spmod_itemcount'] = 'Items count';
$txt['spmod_action'] = 'Action';
$txt['spmod_edit'] = 'Edit';
$txt['spmod_download'] = 'Download';
$txt['spmod_nocatid'] = 'Unknown Category ID.';
$txt['spmod_noitemid'] = 'Unknown purchase or good ID.';
$txt['spmod_nouserid'] = 'Unknown User ID.';
$txt['spmod_noneworg'] = 'Unknown organizer fee.';
$txt['spmod_nomanualtr'] = 'Unknown Transportation Costs.';
$txt['spmod_noprocname'] = 'Unknown purchase name.';
$txt['spmod_notopicid'] = 'Unknown topic ID.';
$txt['spmod_noboardid'] = 'Unknown forum category for creating topic';
$txt['spmod_itemlist'] = 'Goods list';
$txt['spmod_noitems'] = 'Category is empty';
$txt['spmod_photo'] = 'Picture';
$txt['spmod_description'] = 'Description';
$txt['spmod_extparams'] = 'Ext. Parameters:';
$txt['spmod_articul'] = 'Reference';
$txt['spmod_procisclosed'] = 'Purchase is closed.';
$txt['spmod_youcannotbuy'] = 'You can not buy it.';
$txt['spmod_catdelconf'] = 'Are you sure you want to delete this category? All goods will delete too!';
$txt['spmod_paramdelconf'] = 'Are you sure you want to delete this parameter?';
$txt['spmod_jointpurchases'] = 'Group Purchases';
$txt['spmod_itemdelconf'] = 'Are you sure you want to delete this good?';
$txt['spmod_delerror'] = 'You can not delete this good.';
$txt['spmod_itemaddingerror'] = 'You can add goods to your purchases only.';
$txt['spmod_nopurchgooduser'] = 'Unknown user ID, purchases ID or good ID.';
$txt['spmod_noitem'] = 'Unknown good ID';
$txt['spmod_noeditedpurchid'] = 'Unknown purchases ID';
$txt['spmod_count'] = 'Count';
$txt['spmod_addtobuscket'] = 'Buy';
$txt['spmod_nopurch'] = 'Unknown purchases ID.';
$txt['spmod_nocat'] = 'Unknown category ID.';
$txt['spmod_noparam'] = 'Unknown parameter ID.';
$txt['spmod_parsingerror'] = 'Parser error';
$txt['spmod_filetoolarge'] = 'Too large file';
$txt['spmod_erroruploading'] = 'Load error';
$txt['spmod_t_orgname'] = 'Organizer';
$txt['spmod_t_orgphonenumber'] = 'Phone: ';
$txt['spmod_t_orgperc'] = 'Organizer fee: ';
$txt['spmod_t_catlist'] = 'Category list: ';
$txt['spmod_t_table'] = 'Order grid: ';
$txt['spmod_notpermissions'] = 'You don\'t have permission to do this!';
$txt['spmod_topiccreationerror'] = 'The topic wasn\'t created!';
$txt['spmod_imgloaderror'] = 'Error while processing image. Please try again.';
$txt['spmod_nocustomers'] = 'There are no orders';
$txt['spmod_c_name'] = 'Name';
$txt['spmod_c_count'] = 'Order count';
$txt['spmod_c_summa'] = 'Sum';
$txt['spmod_c_org'] = 'Organizer fee';
$txt['spmod_c_itogo'] = 'Total';
$txt['spmod_c_tr'] = 'Cost Of Transportation';
$txt['spmod_c_bank'] = 'Bank fee';
$txt['spmod_successmsg'] = 'Item was successfully added to your cart. Go to ';
$txt['spmod_proctopic'] = 'purchase topic';
$txt['spmod_myproc'] = 'My purchases';
$txt['spmod_nogoodname'] = 'Unknown item name';
$txt['spmod_nogoodprice'] = 'Unknown item price';
$txt['spmod_trmode1'] = '% from order sum';
$txt['spmod_trmode2'] = ' to divide into equal parts';
$txt['spmod_trmode3'] = ' in proportion to order price';
$txt['spmod_trmode4'] = ' in proportion to order count';
$txt['spmod_trmode5'] = ' manual calculating';
$txt['spmod_distrib'] = 'Distribution';
$txt['spmod_myprocempty'] = 'You aren\'t taking the part of any purchase';
$txt['spmod_spadmin'] = 'Purchases Administration';
$txt['spmod_rocempty'] = 'There are no purchases';
$txt['spmod_datefrom'] = ' from ';
$txt['spmod_dateto'] = ' to ';
$txt['spmod_dateopen'] = 'Open date';
$txt['spmod_comission'] = 'Fee';
$txt['spmod_newordersubj'] = 'New order in your purchase';
$txt['spmod_userspace'] = 'User ';
$txt['spmod_spacemakesorderinproc'] = ' has made the order in your purchase';
$txt['spmod_delordersubj'] = 'Cancelling the order in your purchase';
$txt['spmod_spacedelsorderinproc'] = ' has cancelled the order in your purchase';
$txt['spmod_summaorgpercent'] = 'Total';
$txt['spmod_targetprocnotfound'] = 'Purchase was not found';
$txt['spmod_paymentmessagesubject'] = 'Purchase payment';
$txt['spmod_paymentmessagebody'] = 'User {user} has paid the order "{procname}" {procurl}.';
$txt['spmod_paymessagesubject'] = 'Pay your order';
$txt['spmod_paymessagebody'] = 'You have ordered item in [url={procurl}]{procname}[/url]. Order status was changed to "Payment". Paymant card number is {card}';
$txt['spmod_payment'] = 'Payment';
$txt['spmod_paymentcomplite'] = 'Paid-up';
$txt['spmod_paymentdate'] = 'Payment Date';
$txt['spmod_nopayment'] = 'There are no payment';
$txt['spmod_koplate'] = 'To pay';
$txt['spmod_sphelp'] = 'Help';
$txt['spmod_paymentmessage'] = 'Payment! Input payment information after you pay [url={scripturl}?action=spmod;sa=myproc]here[/url] please.';
$txt['spmod_print'] = 'Print';
$txt['spmod_buyoutnumber'] = 'Purchase №';
$txt['spmod_delcats'] = 'Delete catalogues';
$txt['spmod_delcatsconfirm'] = 'Are you sure you want do delete these catalogues? All items will be deleted also.';
$txt['spmod_delgoods'] = 'Delete items';
$txt['spmod_delgoodsconfirm'] = 'Are you sure you want do delete these items?';
$txt['spmod_closebyspadminsubject'] = 'Your procurement was closed by administrator';
$txt['spmod_closebyspadmin'] = 'Your procurement "{procname}" {procurl} was closed by administrator {spadmin}.';
$txt['spmod_deletedbyspadminsubject'] = 'Your procurement was removed by administrator';
$txt['spmod_deletedbyspadmin'] = 'Your procurement "{procname}" {procurl} was removed by administrator {spadmin}.';
$txt['spmod_delprocs'] = 'Remove selected procurements';
$txt['spmod_delprocconfirm'] = 'Are you sure you want do delete these procurements?';
$txt['spmod_nocard'] = 'none';
$txt['hName'] = 'Name';			$txt['hName2'] = 'Title';		$txt['hName3'] = 'Item';
$txt['hArticul'] = 'Articul';	$txt['hArticul2'] = 'Number';	$txt['hArticul3'] = 'identificator';
$txt['hDescr'] = 'Description';	$txt['hDescr2'] = 'Detail';		$txt['hDescr3'] = 'Other';
$txt['hPrice'] = 'Price';		$txt['hPrice2'] = 'Cost';		$txt['$hPrice3'] = 'Value';
$txt['hPic'] = 'Picture';		$txt['hPic2'] = 'Image';		$txt['$hPic3'] = 'Photo'; 
$txt['hLink'] = 'Link';			$txt['hLink2'] = 'URL';			$txt['$hLink3'] = 'address';
$txt['hRow'] = 'Row';			$txt['hRow2'] = 'Count';		$txt['$hRow3'] = 'amount';
$txt['hParam'] = 'Parameter'; 	$txt['hParam2'] = 'Size';		$txt['$hParam3'] = 'Color';
$txt['hValues'] = 'Values'; 	$txt['hValues2'] = 'parameters';$txt['$hValues3'] = 'Sizes';
$txt['spmod_processing'] = ' is processing.';
$txt['spmod_processing2'] = 'processing...';
$txt['spmod_waiting'] = 'waiting...';
$txt['spmod_taskisdone'] = 'complited';
$txt['spmod_taskispaused'] = 'paused';
$txt['spmod_taskisdeleted'] = 'deleted';
$txt['spmod_taskisunknown'] = 'unknown';
$txt['spmod_notaskalbumgroupid'] = 'Unknown task id, album id or group id.';
$txt['spmod_notaskid'] = 'Unknown task id.';
$txt['spmod_tasklist'] = 'Task list';
$txt['spmod_notask'] = 'You haven\'t created any task yet';
$txt['spmod_albumname'] = 'Album name';
$txt['spmod_taskstart'] = 'Start time';
$txt['spmod_interval'] = 'Interval';
$txt['spmod_process'] = 'Process';
$txt['spmod_taskstopconfirm'] = 'Are you sure you want to stop the task?';
$txt['spmod_taskstartconfirm'] = 'Are you sure you want to start the task?';
$txt['spmod_taskdelconfirm'] = 'Are you sure you want to remove the task?';
$txt['spmod_pause'] = 'Pause';
$txt['spmod_start'] = 'Start';
$txt['spmod_notocken'] = 'Unknown tocken.';
$txt['spmod_tasks'] = 'Tasks';
$txt['spmod_albumcreateerror'] = 'Creating VK album error';
$txt['spmod_nonexistenttaskstatus'] = 'Unknown task status';
$txt['spmod_nocatidorparserurl'] = 'Unknown catalog id or parser URL.';
$txt['spmod_notlogged'] = 'You aren\'t logged in.';
$txt['spmod_catinuse'] = 'You cann\'t remove this catalog because it is using in the current procurement.';

//Parsers
$txt['spmod_nopageurl'] = 'Unknown purchases page address';
$txt['spmod_nocurl'] = 'cURL extension is not available on your server! Some mod functions will be unavailable.';

//Template text
$txt['spmod_addingparams'] = 'Additional parameters (for example colour or size)';
$txt['spmod_noaddingparams'] = 'There are no additional params.';
$txt['spmod_pages'] = 'Pages';
$txt['spmod_paramname'] = 'Parameter name';
$txt['spmod_paramalias'] = 'Parameter name in the catalogue';
$txt['spmod_vallist'] = 'Values list';
$txt['spmod_delete'] = 'Delete';
$txt['spmod_decrement'] = 'Decrease';
$txt['spmod_edit'] = 'Edit';
$txt['spmod_loadcat'] = 'Load catalogue from XML file';
$txt['spmod_loadcsv'] = 'Load catalogue from CSV file';
$txt['spmod_file'] = 'File';
$txt['spmod_filefielddescr'] = 'The file has to contain the optional columns \'Наименование\', \'Артикул\', \'Описание\', \'Цена\', \'Картинка\', \'Ссылка\', \'Ряд\', \'Параметр\', \'Значения\'</i>.';
$txt['spmod_xmlfilefielddescr'] = 'Please choose the XML file';
$txt['spmod_load'] = 'Upload';
$txt['spmod_createnewcat'] = 'Create new catalogue';
$txt['spmod_editcat'] = 'Edit catalogue';
$txt['spmod_name'] = 'Name';
$txt['spmod_namedescr'] = 'Input catalogue name';
$txt['spmod_linecount'] = 'Line count';
$txt['spmod_linecountdescr'] = 'Input minimum items count in package.';
$txt['spmod_usercat'] = 'User\'s catalogue';
$txt['spmod_usercatdescr'] = 'Users may add items to this catalogue'; 
$txt['spmod_param1'] = 'List of parameters for this catalogue';
$txt['spmod_paramdescr'] = 'Choose parameter\'s list (size, colour e.t.c.) for this catalogue';
$txt['spmod_param2'] = 'List of additional parameters for this catalogue';
$txt['spmod_param2descr'] = 'You may choose additional parameters for this catalogue';
$txt['spmod_save'] = 'Save';
$txt['spmod_addparams'] = 'Add new parameter list';
$txt['spmod_editparams'] = 'Edit parameter list';
$txt['spmod_paramsnamedescr'] = 'Parameter list name. For example: "Sizes for catalogue Old Navy"';
$txt['spmod_paramsdescr'] = 'Parameter name in buying area. For example: "Size" or "Colour"';
$txt['spmod_values'] = 'Values';
$txt['spmod_valuesdescr'] = 'Comma separated values. For example: "Red, Yellow, Blue, Purple"';
$txt['spmod_catchoose'] = 'Choose catalogue';
$txt['spmod_curcat'] = 'Current catalogue';
$txt['spmod_curcatdescr'] = 'You may move this item to another catalogue';
$txt['spmod_additem'] = 'Add item';
$txt['spmod_edititem'] = 'Edit item';
$txt['spmod_itemdescr'] = 'Item name';
$txt['spmod_description'] = 'Description';
$txt['spmod_descriptiondescr'] = 'Short item description';
$txt['spmod_articul'] = 'Article';
$txt['spmod_articuldescr'] = 'Item article';
$txt['spmod_imgurl'] = 'Image URL';
$txt['spmod_imgurldescr'] = 'Input URL of item image';
$txt['spmod_imgfile'] = 'Or choose file on your computer';
$txt['spmod_imgfiledescr'] = 'Loading image from your computer';
$txt['spmod_link'] = 'Link';
$txt['spmod_linkdescr'] = 'Link to item on producer web site.';
$txt['spmod_price'] = 'Price';
$txt['spmod_pricedescr'] = 'Item price';
$txt['spmod_addingparam'] = 'Additional parameter';
$txt['spmod_addingparamdescr'] = 'Additional parameter: colour, size e.t.c.';
$txt['spmod_addingparam2'] = 'Second additional parameter';
$txt['spmod_dontadd'] = 'None';
$txt['spmod_choose'] = 'Choose';
$txt['spmod_ordered'] = 'Added to order!';
$txt['spmod_orderunbay'] = 'Your order was removed!';
$txt['spmod_orderdel'] = 'The order was removed!';                     
$txt['spmod_addorder'] = 'Added to order!';
$txt['spmod_mypurchase'] = 'My Purchases';
$txt['spmod_purchaselist'] = 'Purchases list';
$txt['spmod_closedpurchaselist'] = 'Closed Purchases';
$txt['spmod_purchaseopen'] = 'Open';
$txt['spmod_purchasestopdate'] = 'Stop Date';
$txt['spmod_purchasedelconf'] = 'Are you sure you want to delete this purchase?';
$txt['spmod_newpurchase'] = 'New purchase';
$txt['spmod_createpurchase'] = 'Edit purchase';
$txt['spmod_purchstat_open'] = 'Open';
$txt['spmod_purchstat_stop'] = 'Stop';
$txt['spmod_purchstat_wait'] = 'Waiting bill';
$txt['spmod_purchstat_pay'] = 'Payment';
$txt['spmod_purchstat_shipment'] = 'Shipping';
$txt['spmod_purchstat_overorder'] = 'Additional orders';
$txt['spmod_purchstat_stopoverorder'] = 'Stop. Additional orders';
$txt['spmod_purchstat_inway'] = 'In transit';
$txt['spmod_purchstat_close'] = 'Closed';
$txt['spmod_purchstat_doclose'] = 'Close';
$txt['spmod_purchstat_gone'] = 'Came';
$txt['spmod_purchstat_get'] = 'Received';
$txt['spmod_purchstat_issuance'] = 'Distribution';
$txt['spmod_purchstat_vocation'] = 'Vocation';
$txt['spmod_purchstat_wasnt'] = 'It didn\'t happen';
$txt['spmod_purchname'] = 'Purchase name';		
$txt['spmod_purchnamedescr'] = 'Purchase description';
$txt['spmod_purchstopdescr'] = 'Purchase stop date';
$txt['spmod_orgpercent'] = 'Organizer fee (%)';
$txt['spmod_orgpercentdescr'] = 'Input your organizer fee in percent';
$txt['spmod_minsum'] = 'Minimal order sum or minimal amount';
$txt['spmod_minsumdescr'] = 'Input minimal sum or minimal amount. Input "0" if it don\'t have any limit';
$txt['spmod_topicid'] = 'Group Purchases category on your forum';
$txt['spmod_topiciddescr'] = 'Choose "Group Purchases" category on your forum, where topic will be created';
$txt['spmod_status'] = 'Status';
$txt['spmod_statusdescr'] = 'Choose the status of your purchase';
$txt['spmod_banner'] = 'Banner';
$txt['spmod_bannerdescr'] = 'Input URL-address of image for your purchase. Banner size is 200x120 pixeles. string begins with "http"';
$txt['spmod_catsdescr'] = 'Choose the catalogues for your purchase';
$txt['spmod_selectall'] = 'Select all';
$txt['spmod_purchcode'] = 'BB Code';
$txt['spmod_catlistcode'] = 'The catalogue list code your purchase';
$txt['spmod_catlistcodedescr'] = 'You may copy and paste this code into your topic.';
$txt['spmod_tablecode'] = 'The BB code for view the table of your purchase';
$txt['spmod_tablecodedescr'] = 'You may copy and paste this code into your topic.';
$txt['spmod_purchiderror'] = 'Unknown current purchase ID.';
$txt['spmod_catalog'] = 'Catalogue';
$txt['spmod_catalogdescr'] = 'Items will be added to this catalogue';
$txt['spmod_newcatalog'] = 'New Catalogue';
$txt['spmod_newcatalogdescr'] = 'Input the name for your new catalogue';
$txt['spmod_url'] = 'URLs';
$txt['spmod_urldescr'] = 'The list of URL addresses with a picture and a description of a item on a vendor (or on a shop) web site. It can be html source code of web page:';
$txt['spmod_execute'] = 'Apply';
$txt['spmod_done'] = 'Done!';
$txt['spmod_orgfi'] = 'Name and Surname';
$txt['spmod_orgfidescr'] = 'Input your name and your surname';
$txt['spmod_orgphone'] = 'Phone number';
$txt['spmod_orgphonedescr'] = 'Input your phone number';
$txt['spmod_orgrequisites'] = 'Payment details';
$txt['spmod_orgrequisitesdescr'] = 'Input your card number. It will be sent to members of your purchases through a personal message.';
$txt['spmod_orgvkpage'] = 'Your VK page';
$txt['spmod_orgvkpagedescr'] = 'Input URL of your vk.com page';
$txt['spmod_website'] = 'Vendor web site';
$txt['spmod_websitedescr'] = 'Input web site address of vendor or shop';
$txt['spmod_customers'] = 'Purchase report ';
$txt['spmod_report'] = 'Report';
$txt['spmod_turnover'] = 'Total purchase sum:';
$txt['spmod_ordersumma'] = 'Total order sum:';
$txt['spmod_c_comission'] = 'Web site fee:';
$txt['spmod_orgpercsumma'] = 'Organizer profit:';
$txt['spmod_goodsearch'] = 'Search (by name or by article)';
$txt['spmod_goodsearchbutton'] = 'Search';
$txt['spmod_bankfee'] = 'Bank fee';
$txt['spmod_bankfeedescr'] = 'Input bank fee percentage.';
$txt['spmod_tr'] = 'Transportation costs';
$txt['spmod_trdescr'] = 'Transportation costs will be divide to all members. If you choose "Fixed percent" then insert a percentage of the purchase amount else insert total transportation costs.';
$txt['spmod_pastecode'] = 'BB Code';
$txt['spmod_yourproclistcode'] = 'BB Code of your purchases list';
$txt['spmod_yourproclistcodedescr'] = 'You can insert this BB code to your signature or to any topic.';
$txt['spmod_p_trmode1'] = 'Fixed percent';
$txt['spmod_p_trmode2'] = 'Divide evenly';
$txt['spmod_p_trmode3'] = 'Divide in proportion to the order';
$txt['spmod_p_trmode4'] = 'Divide in proportion to the amount';
$txt['spmod_p_trmode5'] = 'Manual calculating';
$txt['spmod_p_minsum'] = 'Minimal value';
$txt['spmod_p_mincount'] = 'Minimal count';
$txt['spmod_summaryproccode'] = 'BB code of table with final sums';
$txt['spmod_summaryproccodedescr'] = 'You can insert this code to purchase topic';
$txt['spmod_nick'] = 'Name';
$txt['spmod_orgp'] = 'Organizer %';
$txt['spmod_forpay'] = 'Total to pay';
$txt['spmod_distrdistr'] = 'Choose city district for distribution';
$txt['spmod_distrdistrdescr'] = 'Fill it if you are going to made order';
$txt['spmod_defaultcaption'] = 'size';
$txt['spmod_buyout'] = 'Purchase number';
$txt['spmod_buyoutdescr'] = 'Input number of your purchase.';
$txt['spmod_another'] = 'Another';
$txt['spmod_anothercomment'] = 'Another';
$txt['spmod_orgaddress'] = 'Shipping address';
$txt['spmod_orgaddressdescr'] = 'Input your post address';
$txt['spmod_parserparams'] = 'Parser Parameters';
$txt['spmod_nochoose'] = 'none';
$txt['spmod_any'] = 'any';
$txt['spmod_vkproclink'] = 'VK application link';
$txt['spmod_vkproclinkdescr'] = 'You may invite your friends from VK.com social network using this link';
$txt['spmod_openpurch'] = 'Purchase is opened';
$txt['spmod_chooseonsite'] = 'Choose on this web site';
$txt['spmod_vkinviting'] = 'You can make order in the application {vklink}
Or in this topic {topiclink}

Organizer purchases will contact you to clarify details of the payment and delivery.
Wish you a pleasant shopping :)

#groupbuying #spmod';
$txt['spmod_createtask'] = 'Create task';
$txt['spmod_vkprocdescr'] = 'Catalogs of this purchase will be upload to your VK album';
$txt['spmod_vkgroup'] = 'Choose the VK public';
$txt['spmod_vkgroupdescr'] = 'List of VK publics where your are the administrator or the editor';
$txt['spmod_vkcreatealbum'] = 'Create album';
$txt['spmod_vkcreatealbumdescr'] = 'Input name for new album.';
$txt['spmod_vkchoosealbum'] = 'Or choose';
$txt['spmod_vkchoosealbumdescr'] = 'List of albums where you have rigth to add new pictures.';
$txt['spmod_vkdelay'] = 'Interval';
$txt['spmod_vkdelaydescr'] = 'Pause';
$txt['spmod_sec'] = 'sec.';
$txt['spmod_min'] = 'min.';
$txt['spmod_hour'] = 'hour';
$txt['spmod_vkstarttask'] = 'Start time';
$txt['spmod_vkstarttaskdescr'] = 'Choose task start data and time';
$txt['spmod_vkstart'] = 'Start';
$txt['spmod_recive_email_notification'] = 'Enable e-mail notifications';
$txt['spmod_recive_email_notification_descr'] = 'Receive notitfications about new orders and actions.';
$txt['spmod_uploadingcat'] = 'Upload To Catalog';
$txt['spmod_uploadingcat_descr'] = 'Items will be upload from the file to this catalog.  If you сreate a new catalog it will be ignored.';

//Sub
$txt['spmod_username'] = 'User name';
$txt['spmod_pic'] = 'Image';
$txt['spmod_purchname'] = 'Purchase name';
$txt['spmod_priceandcount'] = 'Price and amount';
$txt['spmod_add'] = 'Add';
$txt['spmod_ordertoo'] = 'Made order';
$txt['spmod_refuse'] = 'Refuse';
$txt['spmod_no'] = 'Not available';
$txt['spmod_yes'] = 'Are available';
$txt['spmod_metoo'] = 'I want it too!';
$txt['spmod_notavailable'] = 'Not available';
$txt['spmod_totalsum'] = 'Total order sum';
$txt['spmod_totalcount'] = 'Ordered';
$txt['spmod_collected'] = 'Collected';
$txt['spmod_stillneeded'] = 'Still needed';                     
$txt['spmod_procnotfound'] = '<font color="red">[Purchase has not been created]</font>';
$txt['spmod_info'] = 'Information';
$txt['spmod_orginfo'] = 'Contact Information';
$txt['spmenubutton_enabled'] = 'Enable the menu button';
$txt['spmenubutton_enabled_sub'] = 'Enable or disable the menu button "Group Purchases"';
$txt['spmod_spcats'] = 'List of Group Purchases categories';
$txt['spmod_spcats_sub'] = 'List format: id1, id2, id3 ...';
$txt['spmod_sparchive'] = 'Archival category';
$txt['spmod_sparchive_sub'] = 'Input archival category ID.';
$txt['spmod_premoderate'] = 'Premoderation';
$txt['spmod_premoderate_sub'] = 'Administrator or moderator must approve new purchase topics.';
$txt['spmod_maxpercent'] = 'Maximum percentage'; 
$txt['spmod_maxpercent_sub'] = 'Maximum percentage on your website'; 
$txt['spmod_commission'] = 'Web site fee'; 
$txt['spmod_commission_sub'] = 'Web site fee which organizers will pay your';
$txt['spmod_proctemplate'] = 'Purchase template';
$txt['spmod_proctemplate_sub'] = 'You may use BB codes and this keys: {title} - Purchase name, {descr} - Purchase description, {name} - Organizer name, {phone} - Phone number, {satus} - Purchase status, {percent} - organizer percentage, {bank} - bank fee, {tr} - Transportation costs , {trmode} - Transportation costs mode, {website} - Shop web site, {stop} - Stop date, {min} - Minimum sum or minimum amount, {minmode} - Minimum mode, {catalogs} - Catalogues list, {table} - Orders table';
$txt['spmod_distriblist'] = 'Districts list for distribution';
$txt['spmod_distriblist_sub'] = 'Input each district in a new line';
$txt['spmod_catforbanner'] = 'Category ID for show a banner';
$txt['spmod_catforbanner_sub'] = 'After this category will be shown banner for recent purchases.';
$txt['spmod_checkbanner'] = 'Enable through banner';
$txt['spmod_checkbanner_sub'] = 'Banner for recent purchases will be shown on each page under main menu.';
$txt['spmod_vkdescrtemplate'] = 'Photo description template';
$txt['spmod_vkdescrtemplate_sub'] = 'Use the keys: {title} - Item Title, {descr} - Item Description, {price} - Price, {url} - Link to online store, {order} - Order Link.';
$txt['spmod_t_price'] = 'Price:';
$txt['spmod_t_org'] = 'Organizer %:';
$txt['spmod_t_count'] = 'Count:';
$txt['spmod_t_summa'] = 'Sum:';
$txt['spmod_orderstring'] = 'Order string';
$txt['spmod_os_name'] = 'Name, size, commentary';
$txt['spmod_os_price'] = 'Price without organizer %';
$txt['spmod_os_order'] = 'Order';
$txt['spmod_noorg'] = 'Unknown organizer';
$txt['spmod_onebuydelconf'] = 'Are you sure you want to reduce the amount of items?';
$txt['spmod_buydelconf'] = 'Are you sure you want to delete order?';
$txt['spmod_jointoline'] = 'For join to line please choose ';
$txt['spmod_join'] = 'Join';
$txt['spmod_purctable'] = 'Purchases table';
$txt['spmod_openlines'] = 'Opened rows';
$txt['spmod_addaddress'] = 'Shipping addresses';
$txt['spmod_addaddress_sub'] = 'Enable users shipping addresses';
$txt['spmod_vkpublic'] = 'VK Public';
$txt['spmod_vkpublic_sub'] = 'URL of your public for collective purchasing';
$txt['spmod_vkapp'] = 'VK Application';
$txt['spmod_vkapp_sub'] = 'URL of your VK Application';
$txt['spmod_vkid'] = 'VK Application ID';
$txt['spmod_vkid_sub'] = 'Input the VK Application ID';
$txt['spmod_vksecret'] = 'VK Application security key';
$txt['spmod_vksecret_sub'] = 'Input the VK Application security key';
$txt['spmod_vkhelp'] = 'Help page';
$txt['spmod_vkhelp_sub'] = 'URL address of the help page of the VK application';
$txt['spmod_begetlogin'] = 'Beget Login';
$txt['spmod_begetlogin_sub'] = 'Input your Beget Login';
$txt['spmod_begetpass'] = 'Beget Password';
$txt['spmod_begetpass_sub'] = 'Input your Beget password';
$txt['spmod_begetphppath'] = 'PHP Path';
$txt['spmod_begetphppath_sub'] = 'Input full PHP Path on your server. "PHP" will be used as default';
$txt['spmod_items_in_banner'] = 'Count of items for a banner';
$txt['spmod_items_in_banner_sub'] = 'How many items should be shown in a banner(5 as default)';
$txt['spmod_slider_enabled'] = 'Enable slider for banner';
$txt['spmod_slider_enabled_sub'] = 'Show banner as a slider.';
$txt['spmod_slider_speed'] = 'Slider autoplay speed';
$txt['spmod_slider_speed_sub'] = 'Slader auto play change interval.';
$txt['spmod_slider_titles'] = 'Show purchases titles in the slider';
$txt['spmod_slider_titles_sub'] = 'Show purchases titles under banner images.';
$txt['spmod_slider_titles_cut'] = 'Cut long titles';
$txt['spmod_slider_titles_cut_sub'] = 'Number of characters to cut titles in the slider.';
$txt['spmod_orders_hide_rows'] = 'Roll up orders list rows more then';
$txt['spmod_orders_hide_rows_sub'] = 'Roll up orders list with more rows (0 - disabled).';
$txt['spmod_orders_unhide_rows'] = 'Show rows when list is rolled';
$txt['spmod_orders_unhide_rows_sub'] = 'How many rows show when orders ist is rolled (0 - disabled).';
$txt['spmod_banner_order'] = 'Banner items order';
$txt['spmod_banner_order_last'] = 'last';
$txt['spmod_banner_order_random'] = 'random';
$txt['spmod_banner_order_sub'] = 'Slider and banner items order.';

//Permissions 
$txt['permissiongroup_spmod'] = 'SPMod';
$txt['permissionname_view_spbuy'] = 'Making purchases';
$txt['permissionhelp_view_spbuy'] = 'Allows users to see catalogues and making purchases';
$txt['permissionname_view_spmod'] = 'Organizer area';
$txt['permissionhelp_view_spmod'] = 'Allows users enter to organizer area.';
$txt['permissionname_view_spadmin'] = 'Group purchases administration';
$txt['permissionhelp_view_spadmin'] = 'Access to Group purchases administrator area';

//Print Page
$txt['spprintpage_proc'] = 'Purchase';
$txt['spprintpage_vsego'] = 'Total';
$txt['spprintpage_waspaied'] = 'Paid';
$txt['spprintpage_subtext'] = '<i>With love,<br />Group Purchases Team {boardurl}</i>';

//Banner template
$txt['spmod_t_recentproc'] = 'Recent Purchases';
//Permissions errors
$txt['cannot_view_spbuy'] = 'You don\'t have permission for buy it. Please ask administrator.';
$txt['cannot_view_spmod'] = 'You don\'t have permission for enter to purchase\'s area. Please ask administrator. Please ask administrator.';
$txt['cannot_view_spadmin'] = 'You don\'t have permission for enter to administrator area.';
//Parsers
$txt['sima-land.ru'] = 'sima-land.ru';
$txt['stok-m.ru'] = 'stok-m.ru';
$txt['natali37.ru'] = 'natali37.ru';
$txt['votonia.ru'] = 'votonia.ru';
$txt['sp-savage.ru'] = 'sp-savage.ru';
$txt['volgoshoes.ru'] = 'volgoshoes.ru';
$txt['odezhda-master.ru'] =  'odezhda-master.ru';
$txt['xn--80abzqel.xn--p1ai'] = 'Bairon';
$txt['ikea.com'] = 'Ikea';
//VK App
$txt['spmodvk_topublic'] = 'Public ';
$txt['spmodvk_allprocs'] = 'Current Procurement';
$txt['spmodvk_howtomakeorder'] = 'Help';

//proc/cron tasks
$txt['spmodtask_makeorder'] = 'Make an order using this link ';

//Help
$txt['spmod_helptitle'] = 'Help';
$txt['spmod_help'] = '
<div class="sp_index">
<center><h1>Simple Purchases Mod for SMF</h1></center>
<ol>	
	<li>
		<a href="#mainfutures">Features</a>
		<ol>
			<li>
				<a href="#formembers">Мembers</a>
			</li>
			<li>
				<a href="#fororganizers">Оrganizers</a>
			</li>
			<li>
				<a href="#foradmins">Administrators</a>
			</li>
		</ol>
	</li>	
	<li>
		<a href="#settings">Settings</a>
	</li>
	<li>
		<a href="#memberlk">Мember Area</a>
		<ol>
			<li>
				<a href="#membercontact">Contact</a>
			</li>
			<li>
				<a href="#membersp">My Buys</a>
			</li>
		</ol>
	</li>
	<li>
		<a href="#orglk">Оrganizer Area</a>
		<ol>
			<li>
				<a href="#orgcontact">Contact</a>
			</li>
			<li>
				<a href="#orgcatalogs">Categories</a>				
			</li>
			<li>
				<a href="#orggoods">Items</a>
			</li>			
			<li>
				<a href="#orgprocs">Purchases</a>
			</li>	
			<li>
				<a href="#orgparameters">Sizes</a>
			</li>
			<li>
				<a href="#orgparser">Parser</a>
			</li>			
			<li>
				<a href="#orgsp">My Buys</a>
			</li>			
		</ol>
	</li>
	<li>
		<a href="#admin">Administrator Area</a>		
	</li>
	<li>
		<a href="#order">How Make The Order</a>		
	</li>
	<li>
		<a href="#vkontakte">VK Social Network Integration</a>		
	</li>	
	<li>
		<a href="#buy">Buy The Mod</a>		
	</li>	
</ol>
</div>
<br />
<div class="sp_help">
<ol>	
	<li>
		<a name="mainfutures"></a>
		<span>Features</span><br /><br />
		<ol>
			<li>
				<a name="formembers"></a>
				<span>Мember features</span><br /><br />
				<ul>
					<li>You may buy some goods from catalogs.</li>
					<li>Memberes may add items to catalogs.</li>
					<li>Search items in catalogs by name or number.</li>
					<li>Members may add orders to table directly.</li>
					<li>Мembers area with your purchases list.</li>
					<li>Sending personal messages about payment information.</li>
					<li>Archive of closed purchases.</li>
					<li>Office for distribution.</li>
				</ul>
			</li>
			<br />


			<li>
				<a name="fororganizers"></a>
				<span>Оrganizer features</span><br /><br />
				<ul>
					<li>Purchases making and editing.</li>
					<li>Purchase templates.</li>
					<li>Administrate fee.</li>
					<li>Payment reports.</li>
					<li>Payment progress bar.</li>
					<li>Print version of detail order reports.</li>
					<li>Automatic calculation of organizer fee, bank fee and shipping fee.</li>
					<li>Import/export items to Excel file.</li>
					<li>Additional item parameters like size and color.</li>
					<li>Parsers of popular internet shops.</li>
				</ul>
			</li>
			<br />


			<li>
				<a name="foradmins"></a>
				<span>Administrator features</span><br /><br />
				<ul>
					<li>Administrator area.</li>
					<li>Detail reports.</li>
					<li>Organizer and administrator fees.</li>
					<li>Recent purchase banner on main page.</li>
					<li>You may install another helpfull modification like Portal, Tickers, Bulletin e.t.c.</li>
					<li>Simple way to change themes.</li>
					<li>VK.com social network integration.</li>
				</ul>
			</li>
		</ol>
	</li>		
	<br />
	<li>
		<a name="settings"></a>
		<span>Installation</span>
		<br />
		<p>
		At the first you have installed smf software on a webserver. SMF is free to download on official website <a href="http://download.simplemachines.org/">simplemachines.org</a>. Then install Group Purchases Modification using Package Manager: Admin -> Main -> Package Manager. After it has finished installing the modification you will be redirected on the <a href="?action=admin;area=modsettings">setting page</a> (Admin -> Configuration -> Modification Settings...).
		</p>
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-settings-1.png"></div>
		<p>
			Group Purchases Administrator may change next settings:
			<ul>
				<li><b>Enable the menu button.</b> Enable or disable the menu button "Group Purchases".</li>
				<li><b>List of Group Purchases categories.</b> List format: "id1, id2, id3 ....". Leave empty field for create purchase topics in any forum category.</li>
				<li><b>Archival category.</b> Input archival category ID. All purchase topics with <i>"closed"</i> status will be moved th this category.</li>
				<li><b>Premoderation.</b> Administrator or moderator must approve new purchase topics. Don\'t forget enable Post Moderation  feature in <a href="/index.php?action=admin;area=corefeatures">General Settings</a>. 
				<li><b>Enable through banner.</b> Recent purchases banner will be shown on each page under main menu.</li>
				<li><b>Category ID for show a banner.</b> Before this category will be shown recent purchases banner.<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-banner.png"></div><br /></li>
				<li><b>Maximum percentage.</b> Maximum percentage on your website.</li>
				<li><b>Web site fee.</b> Web site fee which organizers will pay your.</li>
				<li><b>Shipping addresses.</b> Enable users shipping addresses.</li>
				<li><b>Purchase template.</b> You may use BB codes and this keys: <i>{title}</i> - Purchase name, <i>{descr}</i> - Purchase description, <i>{name}</i> - Organizer name, <i>{phone}</i> - Phone number, <i>{satus}</i> - Purchase status, <i>{percent}</i> - organizer percentage, <i>{bank}</i> - bank fee, <i>{tr}</i> - Transportation costs , <i>{trmode}</i> - Transportation costs mode, <i>{website}</i> - Shop web site, <i>{stop}</i> - Stop date, <i>{min}</i> - Minimum sum or minimum amount, <i>{minmode}</i> - Minimum mode, <i>{catalogs}</i> - Catalogues list, <i>{table}</i> - Orders table</li>
				<li><b>Districts list for distribution.</b> Input each district in a new line.</li>
			</ul>
			<br />
			Set <a href="?action=admin;area=permissions;sa=index">permissions</a> for administrators, organaizers and clients groups.
			<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-settings-2.png"></div>
		</p>
	</li>
	<li>
		<a name="memberlk"></a>
		<span>Member Area</span>
		<p>Use "Simple Purchases" menu button for enter to the Client Area.</p>
		<p>
		<ol>
			<li>
				<a name="membercontact"></a>
				<span>Information</span>
				<p>
				This page to input detail information about members: name, surname, phone number and distribution district. 
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-settings-3.png"></div>
				</p>
			</li>
			<li>
				<a name="membersp"></a>
				<span>My purchases</span>
				<p>
				The list of your purchases. You made order in thes purchases before. You may input payment amount into "To pay" field and post e-mail to organazer.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-memberarea2.png"></div>
				</p>
			</li>
		</ol>
		</p>
	</li>
	<li>
		<a name="orglk"></a>
		<span>Оrganizer Area</span>
		<p>Use "Simple Purchases" menu button for enter to the Organazer Area</p>
		<p>
		<ol>
			<li>
				<a name="orgcontact"></a>
				<span>Information</span>
				<p>
				This page to input detail information about organazer: name, surname, phone number and distribution district. <font color=red>Attention! This information will be published in purchase topic.</font>
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-orgarea-1.png"></div>
				</p>
			</li>
			<li>
				<a name="orgcatalogs"></a>
				<span>Categories</span>
				<p>
				The list of catalogs of goods. Clients may choose items in your catalogs or on another internet shops. 				
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-orgarea-2.png"></div>
				</p>
				<p>You can see catalog names and item count in this table. Also you can edit, remove and export your catalogs to XML files.</p>
				<p>Check "User\'s catalog" allows clients to add new items to the catalog. But organazer can edit and remove this item. 
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-catalog-2.png"></div>
				</p>
				<p>You can upload catalogs from CVS files. You may use Excel for create and edit CVS file. Examples of CVS files: : <a href="https://yadi.sk/d/ImavJYlOkwmvk">catalog1</a>, <a href="https://yadi.sk/d/ntS5WXWgkwmvr">catalog2</a>, <a href="https://yadi.sk/d/3UXn1Rmrkwmvv">catalogs3</a>.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-catalog-3.png"></div>
				</p>
				<p>You can upload catalogs from XML files as well using "Load catalogue from XML file" area.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-catalog-4.png"></div>
				</p>				
			</li>
			<li>
				<a name="orggoods"></a>
				<span>Goods</span>
				<p>
				You can edit and add new items to your catalogs on this page.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-orgarea-3.png"></div>
				</p>
				<p>This page allows you to add new items, change prices, edit description, links e.t.c.  
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-goodsarea-2.png"></div>
				</p>
				<p>Sometimes the purchase organizer can order only a complete range of sizes. In this case input count of sizes. If you have to change all sizes in the catalog you can use "List of parameters for this catalogue" on the Categories page.</p>			
			</li>			
			<li>
				<a name="orgprocs"></a>
				<span>Purchases</span>
				<p>
				The organaizer can create and edit purchases on this page.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-orgarea-4.png"></div>
				</p>
				<p>
				You can see all current purchases in statuses "Open", "Stop", "Waiting bill", "Payment", "Shipping", "Additional orders", "Stop. Additional orders", "In transit", "Came", "Received", "Distribution" in table "Current purchases". The purchases in statuses "Closed" and "It didn\'t happen" will be moved in the table "Archival Purchases". Change status or edit purchases using buttons in "Action" field.
				</p>
				<table>
				<tr>
				<td><img src="/SPMod/images/sp/edit.png" /></td><td>The "Edit" button opens purchase editor where you can change any parameters of your purchase.</td>
				</tr>
				<tr>
				<td><img src="/SPMod/images/sp/stop.png" /></td><td>The "Stop" button changes purchases status to "Stop" and now users can\'t make orders.</td>
				</tr>
				<tr>
				<td><img src="/SPMod/images/sp/creditcard.png" /></td><td>The "Payment" button changes purchase status to "Payment" and create the message with payment table.</td>
				</tr>
				<tr>
				<td><img src="/SPMod/images/sp/closed.png" /></td><td>The "Close" button changes status to "Closed" and moves topic to archive board.</td>
				</tr>
				<tr>
				<td><img src="/SPMod/images/sp/money.png" /></td><td>The "Report" button opens detail and print reports.</td>
				</tr>
				</table>
				<p>
				Input values to the fields on "Create new purchase" area. If you want edit existing purchase press "Edit" button.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-proc-2.png"></div>
				Input values to the fields:
				<ul>
					<li><b>Name.</b> Input the name of your purchase here. It is the purchase topic name also.</li>
					<li><b>Description.</b> Input description of your purchase and examples of items.</li>
					<li><b>Web-site.</b> Input the shop web-site.</li>
					<li><b>Stop Date.</b> Choose posible date of finish your purchase in the calendar.</li>
					<li><b>Organizer fee (%)</b> Choose organizer fee which will be added to order cost.</li>
					<li><b>Bank fee.</b> You can input the bank fee here.</li>
					<li><b>Transportation costs.</b> Transportation costs will be devided for the all purchase members. You can choose one of the following options: <i>"The fixed percentage", "Equally", "Proportional to the price", "By items count"</i>. If you choose <i>"The fixed percentage"</i> then input a percentage to this field, else input the monetary value.</li>
					<li><b>Minimal order sum or minimal amount.</b> The purchase will be complited when sum or count will reach this value. Input 0 for ignore this field.</li>
					<li><b>Group Purchases category on your forum.</b> Choose a board for creation new topic for purchase.</li>
					<li>
					<b>Status.</b> Organizer can change the purchases status: 
					<ul>
						<li><i>Open.</i> The purchase is open and members can order items.</li>
						<li><i>Stop.</i> Members can\'t make new orders. The organizer check items and edits the order table.</li>
						<li><i>Waiting bill.</i> The organizer sends order list to a provider. The provider is billed.</li>
						<li><i>Payment.</i> Members get messages and transfers payment to the organizer.</li>
						<li><i>Shipping.</i> Shop is ready to send order to the organizer.</li>
						<li><i>Additional orders.</i> Members can make new orders but can\'t cancel the previous order.</li>
						<li><i>Stop. Additional orders.</i> Receiving Orders is terminated.</li>
						<li><i>In transit.</i> The order was sended organizer.</li>
						<li><i>Came.</i> The order has been delivered to a transport office.</li>
						<li><i>Received.</i> The order has been delivered to the organizer.</li>
						<li><i>Distribution.</i> The order is ready to distibution.</li>
						<li><i>Closed.</i> All members have got they orders. The purchase is closed.</li>
						<li><i>Vocation.</i> The purchase is temporarily suspended.</li>
					</ul>
					</li>
					<li><b>Banner.</b> The organizer can input image url-address here which will be displayed on main page. Recommended size is 200x120 pixeles.</li>
					<li><b>Catalogs.</b> You may add some catalogs to purchase from your catalog list. It\'s not obligatory but it is more convenient for the members, because they will be able to make purchases by clicking on "Add to the cart" button.</li>
				</ul>
				</p>
				<p>
				Click the "Save" button for create purchase topic whith rules, catalogs list and orders table. The forum administrator can edit the tampate of first message of this topic on modification settings page.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-proc-1.png"></div>
				</p>
				<p>
				The organaizer can edit standart message. Also he can change any purchase terms in the organaizer area: the purchase name, description, web-site address and others. This parameters and first topic message are synchronized.
				</p>
				<p>
				The BB-code from the "BB Code" field on the buttom of the page may be used in the signature or in the any message on the forum. So members can advertise their services in this way.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-orgarea-5.png"></div>
				</p>
				<p>
				Just copy this code to the signature field in the your profile. The list of your purchases will be showed under every your post. Alse you may add any additional information here and use BB-code for change the color, font, size e.t.c. 
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-orgarea-6.png"></div>
				</p>
				<p>
				The organaizer can see the detail purchase report. Open the "Purchase" page and click on the "Report" link on the purchase table.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-report.png"></div>
				</p>
				<p>If you need to change the organaizer fee for somebody. Use "Organaizer fee" field on the report page for set personal organaizer fee.</p>
				<p>Date and paymant value is displeyed in the report table also. The Organaizer can edit this information.</p>
				<p>On the buttom of the report page you can see summary informations including the organaizer profit and web-site administrator fee.</p>
				<p>The "BB Code" field conteins report table without real names and without phone numbers. You can copu and paste this code to message but it will be publish automatically when you change purchase status to "Payment".
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-payment.png"></div></p>
				<p>The organaizer may create print version of the report using "Print" button.  
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-print.png"></div></p>
				</p>
			</li>	
			<li>
				<a name="orgparameters"></a>
				<span>Lines and Parameters</span>
				<p>
				You can add item sizes, colors and other parameters on the page "Lines and parameters". 
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-orgarea-7.png"></div>
				</p>
				<p>Add new parameter: input a parameter name (for example "Sizes for Old Navy brand"), parameter name in the catalogue (for example "size") and values (for example "24,26,28,30"). See details in paragraphs 4.2 and 4.3.</p>
			</li>
			<li>
				<a name="orgparser"></a>
				<span>Parser</span>
				<p>
				A parser is a software component that takes input data from external shop web-site and creates catalogs for your purchases. <font color=red>A parsing result depends of external web-site. It may not work after some changes on this web-sites.</font>
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-orgarea-8.png"></div>
				</p>
				<p>You may as create new catalog as parse to exist catalog. Next web-sites is avalaible now:
				<ul>
					<li><a href="http://sima-land.ru" target="_blank">sima-land.ru</li></a>
					<li><a href="http://stok-m.ru" target="_blank">stok-m.ru</li></a>
					<li><a href="http://natali37.ru" target="_blank">natali37.ru</li></a>
					<li><a href="http://votonia.ru" target="_blank">votonia.ru</li></a>
					<li><a href="http://sp-savage.ru" target="_blank">sp-savage.ru</li></a>
					<li><a href="http://volgoshoes.ru" target="_blank">volgoshoes.ru</li></a>
					<li><a href="http://odezhda-master.ru" target="_blank">odezhda-master.ru</li></a>
					<li><a href="http://байрон.рф" target="_blank">Bairon</li></a>
					<li><a href="http://ikea.com/ru/ru" target="_blank">Ikea</li></a>
				</ul>
				</p><p>Copy and paste links to the field "URLs". You have to input each URL address in new line. You may upload to one catalog items from different shops. All items will be added to exist items if catalog is not empty. After parsing you will be redirect to the catalog page with new items.</p>
				<p>Also you can paste html source code of shop web page enstead URL address if you need log on website at first for get some special prices for example.
				</p>
			</li>			
			<li>
				<a name="orgsp"></a>
				<span>My Buys</span>
				<p>
				This page conteins list of all your orders. You can see purchase status and payment calculation including organaizer percentage and transportation costs. You may input payment information for organaizer. He will recievied email with your order payment details.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-memberarea2.png"></div>
				</p>
			</li>			
		</ol>
		</p>	
	</li>
	<li>
		<a name="admin"></a><b>Administrator Area</b>
		<p>	
		The administrator can query a list of all purchases for the period on the "Purchases Administration" page. You can choose any period (for the last month by default). The administrator can can see a detail report for each purchases also.  
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-adminarea-1.png"></div>		
		</p>
	</li>	
	<li>
		<a name="order"></a><b>How Make The Order</b>
		<p>		
		For a start enter to the Simple Purchases board and choose a purchas topic. You can see a status and a compliting progress bar near name of the topic.
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-buy-1.png"></div>		
		</p>
		<p>
		The first message of this topic conteins description of a purchase, catalog list and order table.
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-proc-1.png"></div>
		</p>
		<p>
		Choose a catalog and follow this link. The page with catalog items will be opened. You can find an item by name or by articul number using search box on the page top.    
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-buy-2.png"></div>
		</p>
		<p>
		Browse the catalog and choose items. Select size or color and input a count. Then press "Buy" button and add selected item to the order table. The organaizer will recieve email with information about new order.
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-buy-3.png"></div>
		</p>
		<p>
		If there are no catalogs in the purchase then you can make order through Order string in the last table row. Just enter URL address of item page (optional), item name, size, color, others parameters (in the second field) and price. Click the button "Order".
		</p>
		<p>
		Also you can order item from any table row by clicking "I want it too!".
		</p>		
	</li>	
	<li>
		<a name="vkontakte"></a><b>VK Social Network Integration</b>
		<p>	<a href="https://new.vk.com/editapp?act=create">Create</a> 2 VK Applications. Choose the type of first Application as "IFrame/Flash", enter the name "Simple Purchase". The type of second application is "Web site". Use name of your web site as a name of your second application. Associate the first IFrame application with your public in the VK social network and edit the following settings.
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/spset-vk-1.png"></div>			
		</p>
		<p>	Edit the settings of the second application. Input VK Application ID and Security Key to modification settings.
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/spset-vk-2.png"></div>
		</p>
		<p>		
		Members can make orders on web site <a href="vk.com">Vkontakte</a>. All the orders are stored in union table. So common report is available for the organaizer.
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-vk-1.png"></div>		
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-vk-2.png"></div>
		</p>
	</li>		
	<li>
		<a name="buy"></a><b>Buy The Mod</b>
		<p>
		You can download free version of this modification from the <a href="http://custom.simplemachines.org/mods/index.php?mod=4109">official SMF web site</a>. 
		Please contact us via e-mail at sale@spmod.ru to request the pro version of SPmod Software. The modification price is $150.  			 
		</p>
	</li>	
</ol>
</div>
';

// Admin Area
$txt['spmod_admin_area_desc'] = 'All settings for Simple Purchases.';
$txt['spmod_admin_area_settings'] = 'Common Settings';
$txt['spmod_admin_area_settings_desc'] = 'Common mod settings.';
$txt['spmod_admin_area_banner'] = 'Banners';
$txt['spmod_admin_area_banner_desc'] = 'Banners and slider settingd.';
$txt['spmod_admin_area_vkontakte'] = 'VK Application';
$txt['spmod_admin_area_vkontakte_desc'] = 'VK application settings.';
$txt['spmod_admin_area_beget'] = 'Beget';
$txt['spmod_admin_area_beget_desc'] = 'Beget hosting settings for cron tasks.';
$txt['spmod_admin_area_permissions'] = 'Permissions';
$txt['spmod_admin_area_permissions_desc'] = 'Group permissions for SP functions.';
$txt['spmod_admin_area_info'] = 'Info';
$txt['spmod_admin_area_info_desc'] = 'Additional system info and support request.';

// Admin Area: Info
$txt['spmod_admin_area_version_spmod'] = 'SPmod version';
$txt['spmod_admin_area_version'] = 'version';
$txt['spmod_admin_area_version_curl'] = 'PHP Curl version';
$txt['spmod_admin_area_forum_url'] = 'Forum URL';
$txt['spmod_admin_area_forum_protocol'] = 'Web schema';
$txt['spmod_admin_area_forum_version'] = 'Forum version';
$txt['spmod_admin_area_forum_theme'] = 'Forum theme';
$txt['spmod_admin_area_forum_lang'] = 'Forum language';
$txt['spmod_admin_area_forum_utf8'] = 'UTF-8';
$txt['spmod_admin_area_phpinfo'] = 'Show all PHP settings';

?>