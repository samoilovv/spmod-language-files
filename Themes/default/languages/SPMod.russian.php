<?php

$txt['spmod_copyright'] = '���������� ������� ��� SMF {version} | &copy; <a href="http://spmod.ru">spmod.ru</a>';
$txt['spmod_unknownver'] = 'unknown version';
$txt['spmod'] = '���������� �������';
$txt['spmod_title'] = '���������� ������� ��� SMF';
$txt['spmod_currency'] = ' �.';
$txt['spmod_itemcountlbl'] = ' ��.';
$txt['spmenubutton_label'] = '���������� �������';
$txt['spmod_spmod'] = '���������� �������';
$txt['spmod_isguest'] = '� ��� ��� ���� ��� ������� � ������ ������� ������������';
$txt['spmod_capt'] = '���������� ���������� � ���������';
$txt['spmod_cats'] = '��������';
$txt['spmod_procurements'] = '�������';
$txt['spmod_goods'] = '������';
$txt['spmod_isguest'] = '����� �� ����� ����������� � ���������� ��������. ��� ���������� ������������������ �� ������.';
$txt['spmod_lines'] = '���� � �������';
$txt['spmod_line'] = '���';
$txt['spmod_lineisopened'] = '��� ������: ';
$txt['spmod_lineiscomplited'] = '��� ������: ';
$txt['spmod_parser'] = '������';
$txt['spmod_catslist'] = '������ ����� ���������';
$txt['spmod_nocats'] = '�� ��� �� ������� �� ������ ��������.';
$txt['spmod_catname'] = '�������� ��������';
$txt['spmod_itemcount'] = '���������� �������';
$txt['spmod_action'] = '��������';
$txt['spmod_edit'] = '�������������';
$txt['spmod_download'] = '���������';
$txt['spmod_nocatid'] = '�� ����� ������������� �������������� ��������.';
$txt['spmod_noitemid'] = '�� ������ ������������� ������� ��� ������.';
$txt['spmod_nouserid'] = '�� ������ ������������� ������������.';
$txt['spmod_noneworg'] = '�� ������ ��������������� �������.';
$txt['spmod_nomanualtr'] = '�� ������� ����� ������������ ��������.';
$txt['spmod_noprocname'] = '�� ������� �������� �������.';
$txt['spmod_notopicid'] = '�� ����� ������������� ����!';
$txt['spmod_noboardid'] = '�� ����� ������ ��� �������� ����!';
$txt['spmod_itemlist'] = '������ ������� ��������';
$txt['spmod_noitems'] = '��������� ������� ����';
$txt['spmod_photo'] = '����';
$txt['spmod_description'] = '��������';
$txt['spmod_extparams'] = '���. ���������:';
$txt['spmod_articul'] = '�������';
$txt['spmod_procisclosed'] = '������� �������.';
$txt['spmod_youcannotbuy'] = '�� �� ������ ��������� �������.';
$txt['spmod_catdelconf'] = '�� �������, ��� ������ ������� ���� �������? ��� ������ � ��� ����� ����� �������!';
$txt['spmod_paramdelconf'] = '�� �������, ��� ������ ������� ���� ��������?';
$txt['spmod_jointpurchases'] = '���������� �������';
$txt['spmod_itemdelconf'] = '�� �������, ��� ������ ������� ��� �������?';
$txt['spmod_delerror'] = '�� �� ������ ������� ���� �����.';
$txt['spmod_itemaddingerror'] = '�� ������ ��������� ������ ������ � ���� �������.';
$txt['spmod_nopurchgooduser'] = '�� ������ ������������� �������, ������ ��� ������������';
$txt['spmod_noitem'] = '�� ������ ������������� �������';
$txt['spmod_noeditedpurchid'] = '�� ������ ������������� ������������� �������';
$txt['spmod_count'] = '���-��';
$txt['spmod_addtobuscket'] = '�������� � �������';
$txt['spmod_nopurch'] = '�� ������ ������������� �������.';
$txt['spmod_nocat'] = '�� ������ ������������� ��������.';
$txt['spmod_noparam'] = '�� ����� ������������� ������� ����������.';
$txt['spmod_parsingerror'] = '������ ��������';
$txt['spmod_filetoolarge'] = '���� ������� �������';
$txt['spmod_erroruploading'] = '������ ��������';
$txt['spmod_t_orgname'] = '�����������';
$txt['spmod_t_orgphonenumber'] = '������� ��� �����: ';
$txt['spmod_t_orgperc'] = '���. �������: ';
$txt['spmod_t_catlist'] = '������ ���������: ';
$txt['spmod_t_table'] = '������� �������: ';
$txt['spmod_notpermissions'] = '� ��� ��� ���� ��� ���������� ����� ��������!';
$txt['spmod_topiccreationerror'] = '�� ������� ������� ����!';
$txt['spmod_imgloaderror'] = '�� ����� ��������� ����������� ��������� ������. ���������� �����!';
$txt['spmod_nocustomers'] = '� ������� ���� ��� ��� �����������';
$txt['spmod_c_name'] = '���';
$txt['spmod_c_count'] = '���������� �������';
$txt['spmod_c_summa'] = '�����';
$txt['spmod_c_org'] = '���. �������';
$txt['spmod_c_itogo'] = '�����';
$txt['spmod_c_tr'] = '��';
$txt['spmod_c_bank'] = '����';
$txt['spmod_successmsg'] = '����� ������� �������� � �������. ������� � ';
$txt['spmod_proctopic'] = '���� �������';
$txt['spmod_myproc'] = '�������� � ��������';
$txt['spmod_nogoodname'] = '�� ������� �������� ������';
$txt['spmod_nogoodprice'] = '�� ������� ���� ������';
$txt['spmod_trmode1'] = '% �� ����� ������';
$txt['spmod_trmode2'] = ' ����� ������� �� ���� ���������� �������';
$txt['spmod_trmode3'] = ' ����� ��������������� ������';
$txt['spmod_trmode4'] = ' ����� �� ���������� ���������� �������';
$txt['spmod_trmode5'] = ' ������������� �������������';
$txt['spmod_distrib'] = '�������';
$txt['spmod_myprocempty'] = '�� ���� �� ���������� �� � ����� �������';
$txt['spmod_spadmin'] = '����������������� ��';
$txt['spmod_rocempty'] = '��� �� ����� �������';
$txt['spmod_datefrom'] = ' �� ';
$txt['spmod_dateto'] = ' �� ';
$txt['spmod_dateopen'] = '���� ��������';
$txt['spmod_comission'] = '��������';
$txt['spmod_newordersubj'] = '����� ����� � ����� �������';
$txt['spmod_userspace'] = '������������ ';
$txt['spmod_spacemakesorderinproc'] = ' ������ ����� � ����� �������';
$txt['spmod_delordersubj'] = '������ ������ � ����� �������';
$txt['spmod_spacedelsorderinproc'] = ' ��������� �� ������ � ����� �������';
$txt['spmod_summaorgpercent'] = '����� ���.%';
$txt['spmod_targetprocnotfound'] = '������� �� �������';
$txt['spmod_paymentmessagesubject'] = '�������� �������';
$txt['spmod_paymentmessagebody'] = '������������ {user} �������� ������ � ����� ������� "{procname}" {procurl}.';
$txt['spmod_paymessagesubject'] = '���������� �������';
$txt['spmod_paymessagebody'] = '�� ������ ����� � ������� [url={procurl}]{procname}[/url] . ������ ������� ��������� �� "����������". ����� ��� ������: {card}';
$txt['spmod_payment'] = '������';
$txt['spmod_paymentcomplite'] = '��������';
$txt['spmod_paymentdate'] = '���� ������';
$txt['spmod_nopayment'] = '������ �� ����';
$txt['spmod_koplate'] = '� ������';
$txt['spmod_sphelp'] = '�������';
$txt['spmod_paymentmessage'] = '����������! ����� ������, ����������, ������� ������������� ����� [url={scripturl}?action=spmod;sa=myproc]�����[/url].';
$txt['spmod_print'] = '������';
$txt['spmod_buyoutnumber'] = '����� �';
$txt['spmod_delcats'] = '������� ��������� ��������';
$txt['spmod_delcatsconfirm'] = '�������, ��� ������ ������� ��������� ��������? ��� ������ � ���� ��������� ����� ����� �������.';
$txt['spmod_delgoods'] = '������� ��������� �������';
$txt['spmod_delgoodsconfirm'] = '�������, ��� ������ ������� ��������� �������?';
$txt['spmod_closebyspadminsubject'] = '���� ������� ������� ���������������';
$txt['spmod_closebyspadmin'] = '���� ������� "{procname}" {procurl} ���� ������� ��������������� ���������� ������� {spadmin}.';
$txt['spmod_deletedbyspadminsubject'] = '���� ������� ������� ���������������';
$txt['spmod_deletedbyspadmin'] = '���� ������� "{procname}" {procurl} ���� ������� ��������������� ���������� ������� {spadmin}.';
$txt['spmod_delprocs'] = '������� ��������� �������';
$txt['spmod_delprocconfirm'] = '�������, ��� ������ ������� ��������� �������?';
$txt['spmod_nocard'] = '�� �������';
$txt['hName'] = '������������';	$txt['hName2'] = '��������';		$txt['hName3'] = '���';
$txt['hArticul'] = '�������';	$txt['hArticul2'] = '�����';		$txt['hArticul3'] = '�������';
$txt['hDescr'] = '��������';	$txt['hDescr2'] = '��������������';	$txt['hDescr3'] = '������';
$txt['hPrice'] = '����';		$txt['hPrice2'] = '���������';		$txt['$hPrice3'] = '����';
$txt['hPic'] = '�������';		$txt['hPic2'] = '����������';		$txt['$hPic3'] = '��������'; 
$txt['hLink'] = '������';		$txt['hLink2'] = 'URL';				$txt['$hLink3'] = '�����';
$txt['hRow'] = '���';			$txt['hRow2'] = '����������';		$txt['$hRow3'] = '���-��';
$txt['hParam'] = '��������'; 	$txt['hParam2'] = '������';			$txt['$hParam3'] = '����';
$txt['hValues'] = '��������'; 	$txt['hValues2'] = '����.';			$txt['$hValues3'] = '�������';
$txt['spmod_processing'] = ' ��� ��������������.';
$txt['spmod_processing2'] = '�����������...';
$txt['spmod_waiting'] = '��������...';
$txt['spmod_taskisdone'] = '���������';
$txt['spmod_taskispaused'] = '��������������';
$txt['spmod_taskisdeleted'] = '�������';
$txt['spmod_taskisunknown'] = '����������';
$txt['spmod_notaskalbumgroupid'] = '�� ������ ������������� ������, ������� ��� ������.';
$txt['spmod_notaskid'] = '�� ������ ������������� ������.';
$txt['spmod_tasklist'] = '������ ����� �����';
$txt['spmod_notask'] = '�� ���� ��� �� ������� �� ����� ������';
$txt['spmod_albumname'] = '�������� �������';
$txt['spmod_taskstart'] = '����� ������';
$txt['spmod_interval'] = '��������';
$txt['spmod_process'] = '�������';
$txt['spmod_taskstopconfirm'] = '�� �������, ��� ������ ���������� ������?';
$txt['spmod_taskstartconfirm'] = '�� �������, ��� ������ ��������� ������?';
$txt['spmod_taskdelconfirm'] = '�� �������, ��� ������ ������� ������?';
$txt['spmod_pause'] = '�����';
$txt['spmod_start'] = '�����';
$txt['spmod_notocken'] = '�� ������ �����.';
$txt['spmod_tasks'] = '������';
$txt['spmod_albumcreateerror'] = '������ ��� �������� ������� ���������';
$txt['spmod_nonexistenttaskstatus'] = '�������������� ������ ������';
$txt['spmod_nocatidorparserurl'] = '�� ������ ������������� �������� ��� ����� ��� ��������.';
$txt['spmod_notlogged'] = '�� �� ������������ �� ������.';
$txt['spmod_catinuse'] = '�� �� ������ ������� ���� �������, ��� ��� �� ����������� � ������� �������.';

//Parsers
$txt['spmod_nopageurl'] = '�� ������ ����� ��������';
$txt['spmod_nocurl'] = '������� � ��������� ������ ������� �� ����� �������� �� ����� �������� ��-�� ���������� ��������� PHP cURL!';

//Template text
$txt['spmod_addingparams'] = '�������������� ��������� �������, ����� ��� ������ � ����';
$txt['spmod_noaddingparams'] = '�� ���� ��� �� ������� �� ������ ���� ��� ���������.';
$txt['spmod_pages'] = '��������';
$txt['spmod_paramname'] = '�������� ���������';
$txt['spmod_paramalias'] = '����������� � ��������';
$txt['spmod_vallist'] = '������ ��������';
$txt['spmod_delete'] = '�������';
$txt['spmod_decrement'] = '���������';
$txt['spmod_edit'] = '��������';
$txt['spmod_loadcat'] = '��������� ������� �� XML �����';
$txt['spmod_loadcsv'] = '��������� ������� �� CSV �����';
$txt['spmod_file'] = '����';
$txt['spmod_filefielddescr'] = '���� ������ ���� �������� � ��������� Windows ANSI � ��������� ������� <i>\'������������\', \'�������\', \'��������\', \'����\', \'��������\', \'������\', \'���\', \'��������\', \'��������\'</i>. ������������ �������� ���� ����� � �������. ������� �������: <a href="https://yadi.sk/d/ImavJYlOkwmvk">������� ��� �����</a>, <a href="https://yadi.sk/d/ntS5WXWgkwmvr">������� � ������ ��� ��������</a>, <a href="https://yadi.sk/d/3UXn1Rmrkwmvv">������� � ������ � ���������</a>.';
$txt['spmod_xmlfilefielddescr'] = '�������� XML-����, � ������� �� ����� ��������� �������';
$txt['spmod_load'] = '���������';
$txt['spmod_createnewcat'] = '������� ����� �������';
$txt['spmod_editcat'] = '������������� �������';
$txt['spmod_name'] = '��������';
$txt['spmod_namedescr'] = '������� �������� ��������';
$txt['spmod_linecount'] = '���������� � ����';
$txt['spmod_linecountdescr'] = '��� ������� ��� ����� �������� � ������ ���� 0';
$txt['spmod_usercat'] = '���������������� �������';
$txt['spmod_usercatdescr'] = '� ������� ������������ ������ ���� ��������� ������'; 
$txt['spmod_param1'] = '������� ������������� ������� � ��������';
$txt['spmod_paramdescr'] = '�������� ������� ���������� ������� (������, ���� � �.�.) � ��������';
$txt['spmod_param2'] = '�������������� ������� ������������� ������� � ��������';
$txt['spmod_param2descr'] = '��� ������������� �� ������ ������� ������ ������� ���������� ������� ������� ��������';
$txt['spmod_save'] = '���������';
$txt['spmod_addparams'] = '�������� ����� ������� ����������';
$txt['spmod_editparams'] = '������������� ������� ����������';
$txt['spmod_paramsnamedescr'] = '�������� ������� ����������. ��������: "������� ��� �������� Old Navy"';
$txt['spmod_paramsdescr'] = '����������� �������������� ���������� ������ � ��������. ��������: "������" ��� "����"';
$txt['spmod_values'] = '��������� ��������';
$txt['spmod_valuesdescr'] = '����������� ����� ������� ��� ��������� �������� ���������. ��������: "�������, ������, �����, ����������"';
$txt['spmod_catchoose'] = '����� ��������';
$txt['spmod_curcat'] = '������� �������';
$txt['spmod_curcatdescr'] = '�� ������ ����������� ������� � ������ �������';
$txt['spmod_additem'] = '�������� �������';
$txt['spmod_edititem'] = '������������� �������';
$txt['spmod_itemdescr'] = '�������� ������';
$txt['spmod_description'] = '��������';
$txt['spmod_descriptiondescr'] = '������� �������� ������';
$txt['spmod_articul'] = '�������';
$txt['spmod_articuldescr'] = '������� � �������� ����������';
$txt['spmod_imgurl'] = 'URL �����������';
$txt['spmod_imgurldescr'] = '���������� ������� ����� ��������';
$txt['spmod_imgfile'] = '��� �������� ���� �� �����';
$txt['spmod_imgfiledescr'] = '�������� ����������� � ������ ����������';
$txt['spmod_link'] = '������';
$txt['spmod_linkdescr'] = '������ �� ����� � �������� ����������';
$txt['spmod_price'] = '����';
$txt['spmod_pricedescr'] = '���� ������';
$txt['spmod_addingparam'] = '���. ��������';
$txt['spmod_addingparamdescr'] = '����� ��������������� ���������: ����, ������ � �.�.';
$txt['spmod_addingparam2'] = '������ ���. ��������';
$txt['spmod_dontadd'] = '�� ���������';
$txt['spmod_choose'] = '�������';
$txt['spmod_ordered'] = '��������� � �����!';
$txt['spmod_orderunbay'] = '��� ����� ������!';
$txt['spmod_orderdel'] = '����� ������!';                     
$txt['spmod_addorder'] = '��������� � ������!';
$txt['spmod_mypurchase'] = '��� �������';
$txt['spmod_purchaselist'] = '������� �������';
$txt['spmod_closedpurchaselist'] = '�������� �������';
$txt['spmod_purchaseopen'] = '�������';
$txt['spmod_purchasestopdate'] = '���� �����';
$txt['spmod_purchasedelconf'] = '�� �������, ��� ������ ������� ��� �������?';
$txt['spmod_newpurchase'] = '������� ����� �������';
$txt['spmod_createpurchase'] = '������������� �������';
$txt['spmod_purchstat_open'] = '�������';
$txt['spmod_purchstat_stop'] = '����';
$txt['spmod_purchstat_wait'] = '���� ����';
$txt['spmod_purchstat_pay'] = '����������';
$txt['spmod_purchstat_shipment'] = '��������';
$txt['spmod_purchstat_overorder'] = '��������';
$txt['spmod_purchstat_stopoverorder'] = '����. ��������';
$txt['spmod_purchstat_inway'] = '� ����';
$txt['spmod_purchstat_close'] = '�������';
$txt['spmod_purchstat_doclose'] = '�������';
$txt['spmod_purchstat_gone'] = '��������';
$txt['spmod_purchstat_get'] = '��������';
$txt['spmod_purchstat_issuance'] = '���������';
$txt['spmod_purchstat_vocation'] = '������� �� ���������';
$txt['spmod_purchstat_wasnt'] = '�� ����������';
$txt['spmod_purchname'] = '�������� �������';		
$txt['spmod_purchnamedescr'] = '������� �������� ���������� �������';
$txt['spmod_purchstopdescr'] = '���������� ������� ���� ��������� �������';
$txt['spmod_orgpercent'] = '���. �������';
$txt['spmod_orgpercentdescr'] = '������� ��������������� ������� �������';
$txt['spmod_minsum'] = '����������� ����� ��� ����������� ���������� ������';
$txt['spmod_minsumdescr'] = '������� ����������� ����� ������ ��� ����������� ���������� ������. ���� ������ 0, �� ����� ��� ���������� �� ����� �����������';
$txt['spmod_topicid'] = '������ ��';
$txt['spmod_topiciddescr'] = '�������� ������, ��� ����� ������� ���� � ��������';
$txt['spmod_status'] = '������';
$txt['spmod_statusdescr'] = '�������� ������ �������';
$txt['spmod_banner'] = '������';
$txt['spmod_bannerdescr'] = '�� ������ ����� ������� URL-����� ����������� ����� �������. ������������� ������ ������� 200x120 �����. ����� ������ �� ����������� ������ ���������� �� http://';
$txt['spmod_catsdescr'] = '�������� ������ �������� ��������';
$txt['spmod_selectall'] = '�������� ���';
$txt['spmod_purchcode'] = '��� ��� ������� � ���������';
$txt['spmod_catlistcode'] = '��� ������ ��������� �� ����� �������';
$txt['spmod_catlistcodedescr'] = '��� ������������� �������������� ���������� � �������� ���� ��� ������ ��������� � �������� ����� �������';
$txt['spmod_tablecode'] = '��� ������� ������� ����� �������';
$txt['spmod_tablecodedescr'] = '��� ������������� �������������� ���������� � �������� ���� ��� ������� � �������� ����� �������';
$txt['spmod_purchiderror'] = '�� ������ ������������� ������� �������.';
$txt['spmod_catalog'] = '��� �������� �� ������';
$txt['spmod_catalogdescr'] = '�������� ���� �� ������������ ���������. ���� ������� �������� �������� � ���������� ������, �� ����� ����� ��������������.';
$txt['spmod_newcatalog'] = '�������';
$txt['spmod_newcatalogdescr'] = '������� �������� ������ �������� ��� ��������.';
$txt['spmod_url'] = '������ URL-�������';
$txt['spmod_urldescr'] = '������� URL-������ �������, � ������� ����� ������������ �������� �������. ������ ����� ���������� ��������� � ����� ������. ��� ���������� � �������� ���� �������� html ��� ��������. ��� ������� ����� ��������� ������������� �� ������ ���������:';
$txt['spmod_execute'] = '���������';
$txt['spmod_done'] = '���������!';
$txt['spmod_orgfi'] = '��� � �������';
$txt['spmod_orgfidescr'] = '������� ���� ��� � �������';
$txt['spmod_orgphone'] = '����� ��������';
$txt['spmod_orgphonedescr'] = '������� ��� ����� ��������';
$txt['spmod_orgrequisites'] = '��������� ��� ������';
$txt['spmod_orgrequisitesdescr'] = '������� ����� ����� �����. �� ����� ������ ���������� ����� ������� � ������ ���������.';
$txt['spmod_orgvkpage'] = '�������� ���������';
$txt['spmod_orgvkpagedescr'] = '������� ����� ����� �������� ���������';
$txt['spmod_website'] = '���� ����������';
$txt['spmod_websitedescr'] = '������� ����� ����� ���������� ��� ��������';
$txt['spmod_customers'] = '����� �� ������� ';
$txt['spmod_report'] = '�����';
$txt['spmod_turnover'] = '����� ����� �������:';
$txt['spmod_ordersumma'] = '����� ����� ������:';
$txt['spmod_c_comission'] = '�������� �����:';
$txt['spmod_orgpercsumma'] = '������� ������������:';
$txt['spmod_goodsearch'] = '����� �� �������� ��� ��������';
$txt['spmod_goodsearchbutton'] = '�����';
$txt['spmod_bankfee'] = '�������� ����� �� �������';
$txt['spmod_bankfeedescr'] = '������� � ��������� �������� ����� �� ������� �������';
$txt['spmod_tr'] = '������������ �������';
$txt['spmod_trdescr'] = '����� �� ����� �������� ����� ����� ����������� ������� � ������������ � ��������� �������. ���� ������ "������������� �������", �� � ���� ���������� ������ ������� �� ����� �������. � ��������� ������� �������� ����� ����� �� � ������.';
$txt['spmod_pastecode'] = '��� �������';
$txt['spmod_yourproclistcode'] = '��� ������ ����� �������';
$txt['spmod_yourproclistcodedescr'] = '�� ������ �������� ���� ��� � ���� ������� ��� � ����� ��������� �� ������';
$txt['spmod_p_trmode1'] = '������������� �������';
$txt['spmod_p_trmode2'] = '����� �������';
$txt['spmod_p_trmode3'] = '����� ��������������� ������';
$txt['spmod_p_trmode4'] = '����� �� ���������� �������';
$txt['spmod_p_trmode5'] = '������������� �������������';
$txt['spmod_p_minsum'] = '����������� �����';
$txt['spmod_p_mincount'] = '����������� ����������';
$txt['spmod_summaryproccode'] = '��� �������� ������� �� �������';
$txt['spmod_summaryproccodedescr'] = '�� ������ �������� ���� ��� � ���� � ��������';
$txt['spmod_nick'] = '���';
$txt['spmod_orgp'] = '���.%';
$txt['spmod_forpay'] = '����� � ������';
$txt['spmod_distrdistr'] = '����� ����� ��� ������� ��� ����������������';
$txt['spmod_distrdistrdescr'] = '�����������, ���� �� ����������� ����������� � ���������� ��������';
$txt['spmod_defaultcaption'] = '������';
$txt['spmod_buyout'] = '����� ������';
$txt['spmod_buyoutdescr'] = '������� ����� ������ ����� �������.';
$txt['spmod_another'] = '������';
$txt['spmod_anothercomment'] = '������ (�� �������������� � ��������������)';
$txt['spmod_orgaddress'] = '����� ��������';
$txt['spmod_orgaddressdescr'] = '������� ���� �������� �����, �� ������� ����� ������ ��� �����';
$txt['spmod_parserparams'] = '��������� �������';
$txt['spmod_nochoose'] = '�� ������';
$txt['spmod_any'] = '�����';
$txt['spmod_vkproclink'] = '����������� � �������';
$txt['spmod_vkproclinkdescr'] = '�� ������ ���������� ������������� �� ���������� ���� ��������� ��� ������� � ����� �������. ���������� � ����������� ����� �� ����� ����������.';
$txt['spmod_openpurch'] = '������� �������';
$txt['spmod_chooseonsite'] = '�������� �� �����';
$txt['spmod_vkinviting'] = '������� ����� ����� ����� {vklink}
��� �� ������ {topiclink}

����� ������ � ���� �������� ����������� ������� ��� ��������� ������������ ������ � ��������.
���� �������� ������� :)

#����������������� #spmod';
$txt['spmod_createtask'] = '������� ������';
$txt['spmod_vkprocdescr'] = '�������� �� ���� ������� ���������� � ������ ������ ���������� ���������';
$txt['spmod_vkgroup'] = '�������� ����������';
$txt['spmod_vkgroupdescr'] = '����������, � ������� �� ��������� ��������������� ��� ����������';
$txt['spmod_vkcreatealbum'] = '������� ������';
$txt['spmod_vkcreatealbumdescr'] = '������� ��� ������ ������� � ����� ������. ���� �� �������� ��� ���� ������, �� �������� ����� ����������� � ������, ��������� � ��������� ������.';
$txt['spmod_vkchoosealbum'] = '��� �������� ������������ ������';
$txt['spmod_vkchoosealbumdescr'] = '�������, � ������� �� ������ ��������� ����������. ���� �� ���� ����� ��� ������ �������, �� ��� ���� ����� ���������������';
$txt['spmod_vkdelay'] = '�������� ��������';
$txt['spmod_vkdelaydescr'] = '����� ����� ��������� ������ ����������';
$txt['spmod_sec'] = '���.';
$txt['spmod_min'] = '���.';
$txt['spmod_hour'] = '���';
$txt['spmod_vkstarttask'] = '����� ������';
$txt['spmod_vkstarttaskdescr'] = '�������� ���� � ����� ������ ������';
$txt['spmod_vkstart'] = '������';
$txt['spmod_recive_email_notification'] = '�������� �� e-mail ����������� �� �������';
$txt['spmod_recive_email_notification_descr'] = '�������� �� ����������� ����� ����������� � ��������� ��������� � �������������� ���� ��������.';
$txt['spmod_uploadingcat'] = '������� ��� ��������';
$txt['spmod_uploadingcat_descr'] = '������ �� ����� ����� ��������� � ��������� �������. ���� ���� �� ����� �������� ������ ��������, �� ����� ����� ��������������.';

//Sub
$txt['spmod_username'] = '��� ������������';
$txt['spmod_pic'] = '�����������';
$txt['spmod_purchname'] = '�������� �������';
$txt['spmod_priceandcount'] = '���� � ����������';
$txt['spmod_add'] = '��������';
$txt['spmod_ordertoo'] = '�������� ����';
$txt['spmod_refuse'] = '����������';
$txt['spmod_no'] = '��� � �������';
$txt['spmod_yes'] = '���� � �������';
$txt['spmod_metoo'] = '� ��� ���� �����!';
$txt['spmod_notavailable'] = '����������';
$txt['spmod_totalsum'] = '����� ����� ������';
$txt['spmod_totalcount'] = '��������';
$txt['spmod_collected'] = '�������';
$txt['spmod_stillneeded'] = '�������� �������';                     
$txt['spmod_procnotfound'] = '<font color="red">[������� ��� �� �������]</font>';
$txt['spmod_info'] = '����������';
$txt['spmod_orginfo'] = '���������� ����������';
$txt['spmenubutton_enabled'] = '�������������� ������ ����';
$txt['spmenubutton_enabled_sub'] = '�������� ��� ��������� ������ ���� "������ ������� ������������ ��"';
$txt['spmod_spcats'] = '������ �� ���������';
$txt['spmod_spcats_sub'] = '������� ����� ������� �������������� ��������� ������, � ������� ����� ����������� ���� � ���������.';
$txt['spmod_sparchive'] = '�������� ������';
$txt['spmod_sparchive_sub'] = '������� ������������� ��������� �������, ���� ����� ������������ ���� ������� �� �������� "�������" ��� "�� ����������".';
$txt['spmod_premoderate'] = '������������ �������';
$txt['spmod_premoderate_sub'] = '��������� ������� ������ �������� ������������� ��� ��������� �������. ��� ������ ������� ����������, �����, �������� ������� ������������ � <a href="/index.php?action=admin;area=corefeatures">����� ����������</a> ������.';
$txt['spmod_maxpercent'] = '������������ �������'; 
$txt['spmod_maxpercent_sub'] = '������������ ��������������� �������, ������� ��������� ������������� ����� ������������� ��'; 
$txt['spmod_commission'] = '�������� �����'; 
$txt['spmod_commission_sub'] = '������� �������� � ���������, ������� ������������ ������ ��������� ������������� �����';
$txt['spmod_proctemplate'] = '������ �������';
$txt['spmod_proctemplate_sub'] = '�� ������ ������������ BB-����, � ����� ����� ��� ���������������: {title} - �������� �������, {descr} - �������� �������, {name} - ��� ������������, {phone} - ����� ��������, {satus} - ������ �������, {percent} - ���. �������, {bank} - �������� �����, {tr} - ������������ �������, {trmode} - ������ ������� ��, {website} - ����� �����, {stop} - ���� �����, {min} - ����������� ����� ������ ��� ����������� ����������, {minmode} - �������� ����� "���������" (����� ��� ����������), {catalogs} - ������ ���������, {table} - ������� �������';
$txt['spmod_distriblist'] = '������ ������� ������';
$txt['spmod_distriblist_sub'] = '������� ������ ����� ������� �� ����� ������';
$txt['spmod_catforbanner'] = '������������� ��������� ��� �������';
$txt['spmod_catforbanner_sub'] = '������� ������������� ���������, ��� ������� ����� �������� ������ � ������� ��������� ���������.';
$txt['spmod_checkbanner'] = '�������� �������� ������';
$txt['spmod_checkbanner_sub'] = '������ � ������� ��������� ��������� ����� ������������ �� ������ �������� ������ ��� ������� ����.';
$txt['spmod_vkdescrtemplate'] = '������ ������� � ����������� ����';
$txt['spmod_vkdescrtemplate_sub'] = '����������� ����� ��� ���������������: {title} - �������� ������, {descr} - �������� ������, {price} - ����, {url} - ������ �� �������� ��������, {order} - ������ ��� ������.';
$txt['spmod_t_price'] = '����:';
$txt['spmod_t_org'] = '���.%:';
$txt['spmod_t_count'] = '���-��:';
$txt['spmod_t_summa'] = '�����:';
$txt['spmod_orderstring'] = '������ ������';
$txt['spmod_os_name'] = '��������, ������, �����������';
$txt['spmod_os_price'] = '���� ��� ���.%';
$txt['spmod_os_order'] = '��������';
$txt['spmod_noorg'] = '�� ������ �����������';
$txt['spmod_onebuydelconf'] = '�� �������, ��� ������ ���������� �� ����� ������?';
$txt['spmod_buydelconf'] = '�� �������, ��� ������ ������� ���� �����? ������ ������� ����� ������� �� ������ ���������.';
$txt['spmod_jointoline'] = '��� �������� � ���� �������� ';
$txt['spmod_join'] = '�����������';
$txt['spmod_purctable'] = '������� �������';
$txt['spmod_openlines'] = '�������� ����';
$txt['spmod_addaddress'] = '������ ��������';
$txt['spmod_addaddress_sub'] = '�������� ������ �������� � �������������';
$txt['spmod_vkpublic'] = '��� ������ ���������';
$txt['spmod_vkpublic_sub'] = '������ �� ������ ���������, � ������� �� ���������� �������������� ���������� �������';
$txt['spmod_vkapp'] = '���������� ���������';
$txt['spmod_vkapp_sub'] = '������ �� ���������� ���������';
$txt['spmod_vkid'] = 'ID ���������� ���������';
$txt['spmod_vkid_sub'] = '������� ������������� ������ ���������� ���������';
$txt['spmod_vksecret'] = '���������� ���� ���������� ���������';
$txt['spmod_vksecret_sub'] = '������� ���������� ���� ������ ���������� ���������';
$txt['spmod_vkhelp'] = '�������� ������';
$txt['spmod_vkhelp_sub'] = '����� �������� ������ �� ������� ����� ���������� ���������';
$txt['spmod_begetlogin'] = '����� Beget';
$txt['spmod_begetlogin_sub'] = '������� ����� ������������ Beget ��� ������� � cron-�������';
$txt['spmod_begetpass'] = '������ Beget';
$txt['spmod_begetpass_sub'] = '������� ������ ������������ Beget ��� ������� � cron-�������';
$txt['spmod_begetphppath'] = '���� php';
$txt['spmod_begetphppath_sub'] = '������� ������ ���� � �������������� php �� ����� ������� ��� ���������� ���������� �����';
$txt['spmod_items_in_banner'] = '���������� ������� � �������';
$txt['spmod_items_in_banner_sub'] = '���������� �������, ������� ����� ��������� � ������� (�� ��������� - 5)';
$txt['spmod_slider_enabled'] = '�������� ������� ��� �������';
$txt['spmod_slider_enabled_sub'] = '������ ����� ������������ � �������� � ���������� ���������.';
$txt['spmod_slider_autoplay'] = '������������� ��������';
$txt['spmod_slider_autoplay_sub'] = '�������������� ��������� ��������� � ��������.';
$txt['spmod_slider_speed'] = '�������� ������������� ��������';
$txt['spmod_slider_speed_sub'] = '���������� ������, ����� ������� ����� �������������� �������� � ��������.';
$txt['spmod_slider_titles'] = '���������� �������� ������� � ��������';
$txt['spmod_slider_titles_sub'] = '���������� �������� ������� ��� ������������� � ��������.';
$txt['spmod_slider_titles_cut'] = '�������� ������� �������� �������';
$txt['spmod_slider_titles_cut_sub'] = '���������� �������� �� �������� �������� �������� ������� ��� ����������� � ��������.';
$txt['spmod_orders_hide_rows'] = '����������� ������ �������, � ������� ����� ������';
$txt['spmod_orders_hide_rows_sub'] = '����������� ������ ������� � ���� �������, ���� � ��� ������ �����, ��� ������ (0 - ���������).';
$txt['spmod_orders_unhide_rows'] = '���������� ����� � ��������� ������� �������';
$txt['spmod_orders_unhide_rows_sub'] = '���� �������� ������������ ������� ������� �������, ����� ������ ���������� ��������� �����, ������� ����� ��� ����� ������������ (0 - ���������).';
$txt['spmod_banner_order'] = '������� ������� � �������';
$txt['spmod_banner_order_last'] = '�����';
$txt['spmod_banner_order_random'] = '���������';
$txt['spmod_banner_order_sub'] = '������� ������ ������� � �������/��������.';

//Permissions 
$txt['permissiongroup_spmod'] = '���������� �������';
$txt['permissionname_view_spbuy'] = '������ �������';
$txt['permissionhelp_view_spbuy'] = '������������� ��������� ������������� �������� � ������ �������';
$txt['permissionname_view_spmod'] = '������ ������� ������������';
$txt['permissionhelp_view_spmod'] = '��������� ������������� �������� ������ � ������ ������� ������������ ��';
$txt['permissionname_view_spadmin'] = '����������������� ��';
$txt['permissionhelp_view_spadmin'] = '������ � ������� �������� �������������� ���������� �������';

//Print Page
$txt['spprintpage_proc'] = '�������';
$txt['spprintpage_vsego'] = '�����';
$txt['spprintpage_waspaied'] = '�����������';
$txt['spprintpage_subtext'] = '<i>� �������,<br />������� ������������� �� {boardurl}</i>';

//Banner template
$txt['spmod_t_recentproc'] = '������� �������� �������';

//Permissions errors
$txt['cannot_view_spbuy'] = '� ��� ��� ���� ��������� �������. ����������, ���������� � ��������������.';
$txt['cannot_view_spmod'] = '� ��� ��� ������� � ������ ������� ������������ ��. ����������, ���������� � ��������������.';
$txt['cannot_view_spadmin'] = '� ��� ��� ���� ������� � ����������������� ������ ��.';

//Parsers
$txt['sima-land.ru'] = '����-����';
$txt['stok-m.ru'] = '���� ������';
$txt['natali37.ru'] = '������';
$txt['votonia.ru'] = '������';
$txt['sp-savage.ru'] = 'Savage';
$txt['volgoshoes.ru'] = '��������';
$txt['odezhda-master.ru'] =  '������ ������';
$txt['xn--80abzqel.xn--p1ai'] = '������';
$txt['ikea.com'] = '����';

//VK App
$txt['spmodvk_topublic'] = '� ������ ';
$txt['spmodvk_allprocs'] = '��� ������� �������';
$txt['spmodvk_howtomakeorder'] = '��� ������� �����';

//proc/cron tasks
$txt['spmodtask_makeorder'] = '�������� ����� � ������������ ��� ������ �������� �� ������ ';

//Help
$txt['spmod_helptitle'] = '�������';
$txt['spmod_help'] = '
<div class="sp_index">
<center><h1>����������� "���������� �������" ��� ������� SMF</h1></center>
<ol>	
	<li>
		<a href="#mainfutures">�������� �����������</a>
		<ol>
			<li>
				<a href="#formembers">��� ����������</a>
			</li>
			<li>
				<a href="#fororganizers">��� �������������</a>
			</li>
			<li>
				<a href="#foradmins">��� ��������������</a>
			</li>
		</ol>
	</li>	
	<li>
		<a href="#settings">��������� � ���������</a>
	</li>
	<li>
		<a href="#memberlk">������ ������� ��������� �������</a>
		<ol>
			<li>
				<a href="#membercontact">���������� ����������</a>
			</li>
			<li>
				<a href="#membersp">�������� � ��������</a>
			</li>
		</ol>
	</li>
	<li>
		<a href="#orglk">������ ������� ������������</a>
		<ol>
			<li>
				<a href="#orgcontact">���������� ����������</a>
			</li>
			<li>
				<a href="#orgcatalogs">��������</a>				
			</li>
			<li>
				<a href="#orggoods">������</a>
			</li>			
			<li>
				<a href="#orgprocs">�������</a>
			</li>	
			<li>
				<a href="#orgparameters">���� � �������</a>
			</li>
			<li>
				<a href="#orgparser">������</a>
			</li>			
			<li>
				<a href="#orgsp">�������� � ��������</a>
			</li>			
		</ol>
	</li>
	<li>
		<a href="#admin">������ ������� �������������� ��</a>		
	</li>
	<li>
		<a href="#order">��� ������ ������</a>		
	</li>
	<li>
		<a href="#vkontakte">���������� � ���������� ����� ���������</a>		
	</li>	
	<li>
		<a href="#buy">������ �����������</a>		
	</li>	
</ol>
</div>
<br />
<div class="sp_help">
<ol>	
	<li>
		<a name="mainfutures"></a>
		<span>�������� �����������</span><br /><br />
		<ol>
			<li>
				<a name="formembers"></a>
				<span>��� ����������</span><br /><br />
				<ul>
					<li>����������� ������� � ������� ����� "� ���� ����" �� �������� �������.</li>
					<li>���������������� ��������. ��������� ������� ����� �������������� ��������� ������� � ����������� ������� � ����� ������ �� ���� �������.</li>
					<li>����� ������ � �������� �� �������� ��� �� ��������.</li>
					<li>����� �������� ������ ������ � �������������� "������ ������".</li>
					<li>������ ������� ��������� �������, ��� ������������ ������ �������, � ������� �� ���������� �������, � �� ������.</li>
					<li>�������������� �� ������ ������ ����� ������� ������ ���������.</li>
					<li>����� �������� �������.</li>
					<li>����� ��������������� ����� ������.</li>
				</ul>
			</li>
			<br />
			<li>
				<a name="fororganizers"></a>
				<span>��� �������������</span><br /><br />
				<ul>
					<li>��������, �������������� � �������� ������� � ��������� � ��������.</li>
					<li>�������������� �������� ��� � �������� �� ������� ��������������� ������� � ������������ �������� ����� ��������� � �������� �������.</li>
					<li>��������� ���������������� �������� � ������� ��� �� ������� ������ � �����������.</li>
					<li>��������� ��� ������������� ������ ������ �� ���������� �������.</li>
					<li>��������� ������� � �������� ����� �������.</li>
					<li>������������ � ������ ��������� � ������ ������� � ��������� ����������� � ������� ���������� �������.</li>
					<li>�������������� ������ ����� ���������������� ��������, ����������� �������� � ��������.</li>
					<li>������/������� ������� � ����� Excel.</li>
					<li>���� ������� ������.</li>
					<li>������� ���������� ��������-��������� ��� �������������� �������� ������� � �������� �������������.</li>
				</ul>
			</li>
			<br />
			<li>
				<a name="foradmins"></a>
				<span>��� ��������������</span><br /><br />
				<ul>
					<li>������ ������� �������������� ���������� �������.</li>
					<li>������������ ������� �� ������������� � �������� ������ �������.</li>
					<li>��������� ����������� ������������ ���������������� �������� � �������� �����.</li>
					<li>������ ������� �������� ������� �� ������� �������� �����.</li>
					<li>����������� ��������� �������������� ���������� �������, ����� ��� ������, ������ ��������, ����� ���������� � �.�.</li>
					<li>������� � ������� ������ ��������� �������� ������������ ������� �����.</li>
					<li>���������� � ���������� ����� ���������.</li>
				</ul>
			</li>
		</ol>
	</li>		
	<br />
	<li>
		<a name="settings"></a>
		<span>��������� � ���������</span>
		<br />
		<p>
		����� ������� ��������� ����������� "SPmod ���������� �������" ��� ���������� ����� ������������� ����� SMF. ������� ��� ����� ��������� �� <a href="http://download.simplemachines.org/">����������� ����� Simplemachines</a>. ������ ���������� ������� ��������������� ����� �������� �������, ��� � ����� ������ ����������� ��� ������ SMF: ������� -> ������ -> �������� �������. ����� ��������� �� ������ �������������� �� <a href="?action=admin;area=modsettings">�������� ��������</a> (������� -> ������������ -> ��������� �����).
		</p>
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-settings-1.png"></div>
		<p>
			�� �������� �������� ����������� ������������� ����� �������� ��������� ���������:
			<ul>
				<li><b>�������������� ������ ����.</b> ������ ��������� �������� ��� ��������� ������ �������� ���� "���������� �������".</li>
				<li><b>������ �� ���������.</b> ����� ������� ����� ������� �������������� ��������� ������, � ������� ����� ����������� ���� � ���������. ���� ������ �� �������, �� ������������ ������ ��������� ������� � ����� ������� ������.</li>
				<li><b>�������� ������.</b> ������� ������������� ��������� �������, ���� ����� ������������ ���� ������� �� �������� <i>"�������"</i> ��� <i>"�� ����������"</i>.</li>
				<li><b>������������ �������.</b> ��� ��������� ������ ����� ��� ����������� �� ����� ������� ������ ��������� � �������� �������� ��� ������������� ������. ����� ���������� �������� ������������ � <a href="/index.php?action=admin;area=corefeatures">����� ����������</a> ������ � ��������� ����������� ������� ��. 
				<li><b>�������� �������� ������.</b> ������ � ������� ��������� ��������� ����� ������������ �� ������ �������� ������ ��� ������� ����.</li>
				<li><b>������������� ��������� ��� �������.</b> ������� ������������� ��������� ������, ����� ������� ����� �������� ������ � ������� ��������� ���������, ��� �������� �� ��������.<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-banner.png"></div><br /></li>
				<li><b>������������ �������.</b> ������������ ��������������� �������, ������� ��������� ������������� ����� ������������� ���������� �������.</li>
				<li><b>�������� �����.</b> ������� �������� � ���������, ������� ������������ ������ ��������� ������������� �����.</li>
				<li><b>������ ��������.</b> ���� ���� ������������ ����� ������������ ������� ������� ������ ��� ������������� ����������, �� �������� ��� �����. ��������� ������� ������ ��������� ���� ������ � ������ ��������, � ������������� ����� �������� ��� ���������� � �������� ������� �� �������������� ��������.</li>
				<li><b>������ �������.</b> �� ������ ���� ����������� ������ ����� � ����� �������, ������� ����� ����������� �� ����� ������. � ������ ������� ����������� ����� ���������������, � ������� ����� ������������� ����������� ��������, ������������ ������ �������������� � ����� ������ ���������: <i>{title}</i> - �������� �������, <i>{descr}</i> - �������� �������, <i>{name}</i> - ��� ������������, <i>{phone}</i> - ����� ��������, <i>{satus}</i> - ������ �������, <i>{percent}</i> - ���. �������, <i>{bank}</i> - �������� �����, <i>{tr}</i> - ������������ �������, <i>{trmode}</i> - ������ ������� ��, <i>{website}</i> - ����� �����, <i>{stop}</i> - ���� �����, <i>{min}</i> - ����������� ����� ������ ��� ����������� ����������, <i>{minmode}</i> - �������� ����� "���������" (����� ��� ����������), <i>{catalogs}</i> - ������ ���������, <i>{table}</i> - ������� �������</li>
				<li><b>������ ������� ������.</b> ������� ������ ����� (��� ����) ������ � ����� ������. ��������� ������� ������ ������� ���� �� ������� � ����� ������ ���������.</li>
			</ul>
			<br />
			�� �������� �� <a href="?action=admin;area=permissions;sa=index">�������� �������� ���� �������</a> ���������� ����� ��� ��� ����� �������������, ������� ������ ���������������� �� ������, ��������� ��� ����������� � ��������.
			<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-settings-2.png"></div>
		</p>
	</li>
	<li>
		<a name="memberlk"></a>
		<span>������ ������� ��������� �������</span>
		<p>��� ����� � ������ ������� ��������� ��� ������������ ������� ����� ������������ ������ �������� ���� ������ "���������� �������". ����� ��� ���������� ������� �������� 3 ��������: <i>"���������� ����������", "�������� � ��������", "�������"</i>.</p>
		<p>
		<ol>
			<li>
				<a name="membercontact"></a>
				<span>���������� ����������</span>
				<p>
				�� �������� ���������� ���������� ��������� ������� �� ������� ����� ������� ���� ��� � �������, � ����� ����� �������� � ���������������� ����� (����) �������. ��� ���������� ����� �������� ������ ������������� ��� �������, � ������� �� ����������.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-settings-3.png"></div>
				</p>
			</li>
			<li>
				<a name="membersp"></a>
				<span>�������� � ��������</span>
				<p>
				�� ������ �������� ������������ ������ ���� �������, � ������� �� ����������. ����� ��������� ������ ������ ������� � ������ ��������� ������ � ��������� ����������, ���������������� �������� � ������������ ��������. � ������� "������" �� ������ ������� �����, ������� ����������� ������������. � ���� ������ ����������� ������� ����� ��������������� � ���� � ����� ������ ����� ���� ������ �������, � ����� ������� ����������� �� �����.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-memberarea2.png"></div>
				</p>
			</li>
		</ol>
		</p>
	</li>
	<li>
		<a name="orglk"></a>
		<span>������ ������� ������������</span>
		<p>��� ����� � ������ ������� ��������� ��� ������������ ������� ����� ������������ ������ �������� ���� ������ "���������� �������". ����� ��� ������������ ������� �������� ��������� ��������: <i>"���������� ����������", "��������", "������", "�������", "���� � �������", "������", "�������� � ��������", "�������"</i>.</p>
		<p>
		<ol>
			<li>
				<a name="orgcontact"></a>
				<span>���������� ����������</span>
				<p>
				�� �������� ���������� ���������� ������������ ������� ��������� ���� ��� � �������, � ����� ����� ��������. <font color=red>��������! ��� ���������� ����� ������������ � ���� �������.</font> ����� ����������� ����� ���� ���������� ������� ������ �������������. � ���� ������ ���������� ������� ���������������� ����� (����) �������.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-orgarea-1.png"></div>
				</p>
			</li>
			<li>
				<a name="orgcatalogs"></a>
				<span>��������</span>
				<p>
				�� ������ �������� ������������ ������ ��������� ������������� ���������. ��� ������� ������� �������� �� �����������, �� ��� ������ ����� ������� ��� ������������� ������� �������. ��������� ������� ����� �������� ������ �� �������� �� ������� ��� ���� � ����� ������ ����� ������ ������� �� ������ <i>"�������� � �������"</i>.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-orgarea-2.png"></div>
				</p>
				<p>� ������� ��������� ������������ �������� ��������� � ���������� ������� � ������ �� ���. ����� ���� �� ������ �������, ������������� ��� ��������� ���� �������� � XML ����, ��������� ��� ����� ��������������� ������ � �������. �������� � XML ���� ����� �������������, ��������, ���� �� ������ �������� ���� ������� ������� ������������ ��� ������ ��������� ��� � ���� �� ����������. ����������� ���� ����� ������������ � ������� ��� �������������� ���� ������� �������� ��� ��� �������� ����� ��������� �� ������ ���� ��� � ������ �������. ������� �������� ����� �������. ��� ����� ������ �������� �������� �������� � ������� �� ������ <i>"������� ��������� ��������"</i>.</p>
				<p>����� ������� ����� �������, ������� ��� ��������. ���� �� ��������� ������� "���������������� �������", �� ��������� ������� ���� ������ ��������� ������ � ���� �������, �������� ��������, ����, ������ �� �������� � ������ ��������������. � ���������� ��� ����������� ����� ������� ������� �� ������� ���������, ��������������� � ��������� � ������ ���� ��������. ��� ���� ������� �������� ����� ������� ��� ������ �������������. ��� ����� ���� ������, ����, �����, ����� �.�.�. ������� ������������� ��������� �� �������� <i>"���� � �������"</i>. ���� � ��� �� ����� ��������, ������ � �.�. �� ������ ������������ ��� ���������� ���������.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-catalog-2.png"></div>
				</p>
				<p>����� �� ��������� ������ ������� ������� � �� ������� ���������� �� ������� ������ �� ������ ��������������� ��������� �������� �� CSV �����. ��� ������, ����� �� ��������� ������ �� ���������� � ���� Excel ������ ��� ����� �� �������� ����� ������� ��������������. ����� ��������������� ��������� ����������, ����� ��������� ���� ��� �������� � ��������� Windows ANSI � �������� ��������� ����� ��������: <i>"������������", "�������", "��������", "����", "��������", "������"</i>. ������� ���� �������� �������������, �� ����������, ����� ��� ����������� �������� � ���� ������. �������� ��������, ��� ����� �������� ������ ����� ��������� (���������� � ������� ����� � �� ����� ������ ��������). ����� ����, ��� �� �������� Excel ������� � ������������ � ������������ ��������������, ��������� �� ��� CVS ����. ������������ ��� ���� ������ �������� ����� � �������. ������� ������ ����� ������� �����: <a href="https://yadi.sk/d/ImavJYlOkwmvk">������� ��� �����</a>, <a href="https://yadi.sk/d/ntS5WXWgkwmvr">������� � ������ ��� ��������</a>, <a href="https://yadi.sk/d/3UXn1Rmrkwmvv">������� � ������ � ���������</a>.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-catalog-3.png"></div>
				</p>
				<p>���� �� ����� ��������� �����-���� ���� ������� �� �������, �� �� ������ ������������ ��� (��� ������� ��� �����) ��������� ���� "��������� ������� �� XML �����".
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-catalog-4.png"></div>
				</p>				
			</li>
			<li>
				<a name="orggoods"></a>
				<span>������</span>
				<p>
				�� ������ �������� ������������ ������� ������� � ��������� �������� � ������������ �������������� � ���������� ����� �������. 
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-orgarea-3.png"></div>
				</p>
				<p>�� ������ ��������� ����� �������, ������������� ����, ��������, ������ � �.�. � ����� ���������� ������� �� ������ �������� � ������. ��� ����� ��������� �� ������ "��������" � �������� � ���� "������� �������" �� ����������� ������ ��� ������ �������� ��� ������� �������. ����� �� ������ �������� ��������, ��������, �������, ����� ����������� �.�.�. ����� ��������� ���� �����������.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-goodsarea-2.png"></div>
				</p>
				<p>���� ����� ������� ������ ���������� ������, �� ������� ���������� ������� � ����. ����� � �������� ������� �������� ������� ����� �������������� �������������. ����� ��� ����� �������� �� ��������� � � ������ ������������� ����� ������ �����. � ��� ������, ���� ��� ������ ���������� �������� ����� ������-���� ��������������� ��������� (������, ����, �����, ����� � �.�.), �� ���������� ������ �������� ����� ��������� � ���� "����������� � ��������" � ������������ ����� ������� ��� �������� ��������� � ���� "��������� ��������".  � ������ ������ ��������� ������ ����� ��������� ��������� �������: <i>22,23,24,25,26,27</i>. ���������� � ���� ������ ���� ��������, ��� ���� ����� �������������� ��� �����, �� ���� ��������� ������� ������ �������� ����� ���������� ����� ��������. ���� ����������� ����� ����������� ���� ������, �� � ������ ������ ��� ���������� ������ �������� 6 � ���� <i>"���������� � ����"</i>, ��� ��� � ��������� ��������� ����������� 6 ��������� ��� ������ ��������. ����������� ��������� ������������� �������� � ����� ����. ��������,  ���� � ���� ���� 2 ���� 24-�� �������, �� �� ����� ������� � ������ ��������� �������: <i>22,23,24,24,25,26,27</i>. �������������� ���������� � ���� ����� ����� 7-��. ���������� ��� ���� ������� �������, ����� � ���� ���������� ��������� ���������� ������, �� ������� �������������� ����������. � ���� ������ ������ �� ���������� ���� <i>"����������� � ��������"</i> � <i>"��������� ��������"</i>.</p>
				<p>����� � ����� �������� ��� ������ ����� ���������� ������� ���� ����������. ������� ����� ��������� ����� �� ��������� ������� �� ���� �������� <i>"���������� � ����"</i>, <i>"����������� � ��������"</i> � <i>"��������� ��������"</i>, �� ������ �� �������� <i>"���� � �������"</i> ������� ������� ���������� � ����� ������ �������� �� � ���� <i>"���. ��������"</i>. � ���������� ��� ��������� �������� ���������� � ����������� � ����� ��������������. ����� ���������� ������ ��������� ����������������� ������� ���������� ����� �� ����� ��������. ��� ����� ��������� �� �������� ���������, ������� �� ������ <i>"��������"</i> �������� �������� � �� �������� �������������� �������� � ���� <i>"������� ������������� ������� � ��������"</i> �������� ����������������� ������� ����������. ��� �������� ����� � ������������� ������ � ������ �������� �������������.</p>
			</li>			
			<li>
				<a name="orgprocs"></a>
				<span>�������</span>
				<p>
				������ �������� �������� ��������, � ������� ��������� �������� ������������. ����� �� ������� ������� � ��������� ���. 
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-orgarea-4.png"></div>
				</p>
				<p>
				� ������� "������� �������" ������� ��� ���� ������� �� ��������� <i>"�������", "����", "���� ����", "����������", "��������", "��������", "����. ��������.", "� ����", "��������", "��������"</i> ��� <i>"���������"</i>. � ������� "�������� �������" ������������ ������� �� �������� <i>"�������"</i> ��� <i>"�� ����������"</i>. ������ � ������� "��������" ��������� ������ ������ ��� ������������� �������.
				</p>
				<table>
				<tr>
				<td><img src="/SPMod/images/sp/edit.png" /></td><td>������ "��������" ��������� �������� �������, ��� ����� ������ ����� ��������� �������.</td>
				</tr>
				<tr>
				<td><img src="/SPMod/images/sp/stop.png" /></td><td>������ "����" �������� ������ ������� �� "����", ����������� ������ ������ � ������������� �����������. ����� ������� �� ������ "����" ����������� ���������������� � ������� �������.</td>
				</tr>
				<tr>
				<td><img src="/SPMod/images/sp/creditcard.png" /></td><td>������ "����������" �������� ������ ������� �� "����������", ��������� � ���� ������� � ��������� � ������ � �������������� �� ��� �������.</td>
				</tr>
				<tr>
				<td><img src="/SPMod/images/sp/closed.png" /></td><td>������ "�������" �������� ������ �� "�������" � ���������� ������� � "��������".</td>
				</tr>
				<tr>
				<td><img src="/SPMod/images/sp/money.png" /></td><td>������ "�����" ��������� ��������� ����� �� ������� � ������������ ������ ��������� ��� ��������� �������.</td>
				</tr>
				</table>
				<p>
				��� �������� ����� ������� ��������� ����������� ���� � ����� <i>"������� ����� �������"</i>, ������� ��������� ����. � ��� �������������� ������������ ������� �� ������ "��������" � �������.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-proc-2.png"></div>
				��� �������������� ��� �������� ������� ���������� ��������� ��������� ����:
				<ul>
					<li><b>��������.</b> ���������� � ������� �������� �������� ��� ����� �������. ����� ����� ���������� � ���� �� ������.</li>
					<li><b>��������.</b> ������� ����� ������� �������� ������� � ��������� ������������ � ������� �������.</li>
					<li><b>���� ����������.</b> ������� ����� ����� ���������� ��� ��������-��������, � �������� ����������� ����������� �������. ������������ ����� ���������� �� ���������� ������, ����� �������� ������ � �������� ����.</li>
					<li><b>���� �����.</b> �������� � ��������� �������������� ���� ����� ����� �������.</li>
					<li><b>���. �������.</b> �������� �� ����������� ������ ��������������� �������, ������� ����� ����������� ��� ������� ������������� ��������� �������.</li>
					<li><b>�������� ����� �� �������.</b> � ������ ���� �� ���������� ������ ���������� ����� ����, �� ������ ������� ����� �������� ����� �� ������� �������.</li>
					<li><b>������������ �������.</b> ����� �� ����� �������� ����� ����� ����������� ������� � ������������ � ��������� �������. �������� 4 ������� ������� ������������ ��������: <i>"������������� �������", "����� �������", "����� ��������������� ������", "����� �� ���������� �������"</i>. ���� ������ <i>"������������� �������"</i>, �� � ���� ���������� ������ ������� �� ����� �������. � ��������� ������� �������� ����� ����� �� � ������. ��� ������ ������� ������� ����������� ������ ����������������� ������������ ������ � ��������������� � ����������� �������. ��������, ��� ������� ����� �������������� �� ������ �� ���������� ���������� ���, ��� ��� ����� ���������� ������� ��� ���������. ��� ������ ������ ������ ������� ������ ��������������� ����� ������.</li>
					<li><b>����������� ����� ��� ����������� ���������� ������.</b> ���� ����� � ����������� ���� ���������� �� ����������� ����� ������. � ���� ������ ������� ���������, ���� ����� ���������� ��������� ����� �����. ������ ������ ����� ����������� ���������� �� ����������� ���������� � ������. ���� �� � ���� ���� ������� 0, �� ����� ��� ���������� �� ����� ����������� � �� ����� ������������ ��������� ���������� ������� � ���������.</li>
					<li><b>������ ��.</b> � ���������� ������ �������� ������, ��� ����� ������� ���� ��� ������� �� ����� ��������.</li>
					<li>
					<b>������.</b> ����������� �� ����� ������ ������ �������: 
					<ul>
						<li><i>�������.</i> ������� ������� � � ��� ����� ������ ������ ��������� �������. ����� �������� ���������� �� ����� ���������� ������.</li>
						<li><i>����.</i> ����� ������� ����������. ����������� ��������� ������� ������� � �������� � ����������� ������� �������.</li>
						<li><i>���� ����.</i> ����� �������� ����������, ������� ��������� ������� � ���������� ����.</li>
						<li><i>����������.</i> ��������� ������� ����������� ������ ������ ������ ������������ � ������ ������� �� ������ � ������ ��������. ��� ��������� ������� �� "����������" ��� ��������� ������� �������� ����������� ����� ������ ��������� � ������������� ���������� ������.</li>
						<li><i>��������.</i> ��������� ����� ��������� ����� ������������.</li>
						<li><i>��������.</i> ��������� ������� ����� ��������� �������, �� �� ������������ �� ����� ��������� �������</li>
						<li><i>����. ��������.</i> ����� ��������� ������������� ���������.</li>
						<li><i>� ����.</i> ����� ��������� ����������� �� ����� ������������.</li>
						<li><i>�������.</i> ������� ��������� � �������.</li>
						<li><i>��������.</i> ����� ������ � ���� ������������ ��������.</li>
						<li><i>��������.</i> ����� ������� �������������.</li>
						<li><i>���������.</i> ����� ������������� ������������� � ����� � ������.</li>
						<li><i>������� �� ���������.</i> ������� �������� ��������������.</li>
					</ul>
					</li>
					<li><b>������.</b> ����������� ����� ������� ����� URL-����� ��������, ������� ����� �������������� � �������� ������� �� ������� �������� ������. ������������� ������ ����������� 200x120 �����. ����� ������ �� ����������� ������ ���������� �� <i>"http://"</i>.</li>
					<li><b>��������.</b> ��������� �������� �� �����������, �� ��� ������� ����������� ����� ������� �� �� ������ ����� ���������. ��� ������� ������� ������� ����� ������� ��� �������������. � ���, ��� ��������� � ������������� ��������, ��. ����� 4.2 �������.</li>
				</ul>
				</p>
				<p>
				����� �������� ������� � ��������� ������������� ������� ������ ������������� �������� ��������������� ����, ���������� ����������� �������, �������, ������ ��������� � ������� �������. ������ ������ ������� ��������� � ����� ���� ����������� ��� ���� ����������� �� ����� ������� � ����� ��������������� ���������������.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-proc-1.png"></div>
				</p>
				<p>
				����������� ����� ������������� ��������� ����, ��� ���� ���� �� ������, �������� ���� ������� ��� ���������� ����������� �����. ����� �� ����� �������� ����� ������� ������� ����� ������ �������: ��������, ��������, ����� ����� � �.�. ��� ��������� ����� �������� � � ���� �������.
				</p>
				<p>
				����� �������� "�������" ���������� ���� "��� �������". ������ BB ��� ����� ������������ ��� ������� ����� ������� � ����� ������� (��� � ����� ������ ��������� �� ������). 
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-orgarea-5.png"></div>
				</p>
				<p>
				������ ���������� ���� ��� � �������� � ������� � ������� ������ ��������. ����� ������ �������, ������� ����� ����������� ����� ������������ ��� ������ ��� ����������. ����� ������� �� ������ ������������� ���� ������� ������ ������� �� ������ � ������� �����������. ����������� ������������ � ������ BB ����, ����� �������� ���� ��� ������ ������ ������ �������.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-orgarea-6.png"></div>
				</p>
				<p>
				� ����� ������ ����������� ����� ���������� ����� �� ����� ��������. ��� ����� � ������ �������� ������������ �� �������� "�������" ��������� �� ������ "�����" � ������� �������. ��������� ������ ���������� ����� ������� � ��������� ���������� � �������� ��������� �� �������.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-report.png"></div>
				</p>
				<p>������ ����� ������������� �������� �������� ���. �������� ������ ��� ������ ������������� ��������� �������. ��������, ���� � �������� �������, ��� ��� ������ ������ ������������ �����, ��������������� ������� ���������, �� ����������� ����� ��������������� ���� ������ � ��������� ���. ������� ��� ��� ����������, ������� ��������� ��� �������.</p>
				<p>����� � ������ ������������ ���������� � ���� � ����� ������������� ������. ���� �������� ������� �� ������ � ����� ������ �������� ���� ������, �� ��� ����� ������� � �����������.</p>
				<p>����� ������� ���������� ������� ������������ �������� ���������� �� ����� ����� ������, �������������� �������� ����� � ������� ������������.</p>
				<p>� ����� "��� �������" ���������� BB ��� ������ (��� ���� � ������� ���������) ��� ������� � ���� �������. � ��������� ������ ��� ������������ ��� ������� � ����������� � ������ � ������.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-payment.png"></div></p>
				<p>� ������� ������ "������" ����������� ����� ���������� �������� ��� ������� � ��������, �� ������� ����� ������� ��� ��������� �������, ��� ������������, ����� �������, �������� ���� �������, ������������� � ���������� ����� � ������. 
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-print.png"></div></p>
				</p>
			</li>	
			<li>
				<a name="orgparameters"></a>
				<span>���� � �������</span>
				<p>
				�� �������� ���������� ����� ��������� �������, �����, ����� � ������ ��������� �������. 
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-orgarea-7.png"></div>
				</p>
				<p>�������� ����� ������� ����������, ������ ��������, ����������� � �������� � ���������� ����� ������� ��� ��������� �������� ������� ���������. �� �������� ����� �������������� ����� ����� ��������� � ��������� ��� � �����-���� ������� � �����������. ��� ��� ������� ������� � ������� 4.2 � 4.3 �������. ����������� � �������� - ��� �����, ������� ����� ������ ������������, ����� ����� � ��������. ��������, ���� ��� ����� ������� ������, �� ������� � ��� ���� ����� "������". ��� ���� � ���� "��������� ��������" ����������� ����� ������� ��� �������, ��������� ��� ������. ��������� ���� ����� �������� ����� ����������� ���������� ������, �� �������� ������������ ����� �������� ���������� ������, ����� ����� � ��������. ���������� �������� ����� �������� ������ �������� ����� ��� ����, �����, �����, ����� � �.�.</p>
			</li>
			<li>
				<a name="orgparser"></a>
				<span>������</span>
				<p>
				������ - ��� ���������, ��������������� ��� ��������������� ���������� ��������� ������� ��������� ����� �����������. <font color=red>��������! ������ �������� ������� �� ��������� ������ � � ������ �����-���� ��������� �� ���� ������, ������ ����� ����� �����������������.</font>
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-orgarea-8.png"></div>
				</p>
				<p>� ������� ������� �� ������ ��������� ������ � ������������ ��� ����� �������� �������� �� ��������-��������� ��� � ������ �����������. �� ������ ������ �������� ������� ��������� ���������: 
				<ul>
					<li><a href="http://sima-land.ru" target="_blank">����-����</li></a>
					<li><a href="http://stok-m.ru" target="_blank">���� ������</li></a>
					<li><a href="http://natali37.ru" target="_blank">������</li></a>
					<li><a href="http://votonia.ru" target="_blank">������</li></a>
					<li><a href="http://sp-savage.ru" target="_blank">Savage</li></a>
					<li><a href="http://volgoshoes.ru" target="_blank">��������</li></a>
					<li><a href="http://odezhda-master.ru" target="_blank">������ ������</li></a>
					<li><a href="http://������.��" target="_blank">������</li></a>
					<li><a href="http://ikea.com/ru/ru" target="_blank">����</li></a>
				</ul>
				</p><p>���������� � �������� � ���� "������ URL-�������" ������ �� �������� � ��������. ������ ����� ������ ���������� � ����� ������. ��� ������� ����� ��������� �������������, ������ ��� ������������� � ���� ������� ����� ��������� ������ �� ������ ���������. ���� � ��������� �������� ��� ���� ������, �� � ���� ����� ��������� ����� �������. � ���������� ������ ������� �� ������ �������������� �� �������� ������������ ������ �������� ��������.</p>
				<p>������ ������ URL ������� ��� ������������� �� ������ �������� ��������� ��� �������� � ��������. � ���� ������ ��� ������� ����� ����������� �������������. ����� ����������� html ��� ������� ������ ������� ���� �� �������� ����� ��������, �������� ����� "�������� ���� ��������", ���������� ���� ����� ��������� ���� � ����������� ���� � �������� � ���� "������ URL-�������". ����� ������� ������ "���������". ������������� ��������� ���� ������ URL ������� ����� ��������� ��������, ����� ������ ������ �� �����-���� �������� �� ��������. ���, ��������, ����� �� ������ ��������� ��������� ������������ ���������� ���� ������ ����� �����������. � ���� ������ ����� ����� ������������ �������� ����� �������� ��� ��������, ��� ��� ��� ����� ��������� ���������� ����. 
				</p>
			</li>			
			<li>
				<a name="orgsp"></a>
				<span>�������� � ��������</span>
				<p>
				�� ������ �������� ������������ ������ ���� �������, � ������� �� ������� �������, ��� �������� �������. ����� ��������� ������ ������ ������� � ������ ��������� ������ � ��������� ����������, ���������������� �������� � ������������ ��������. � ������� "������" �� ������ ������� �����, ������� ����������� ������������. � ���� ������ ����������� ������� ����� ��������������� � ���� � ����� ������ ����� ���� ������ �������, � ����� ������� ����������� �� �����.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-memberarea2.png"></div>
				</p>
			</li>			
		</ol>
		</p>	
	</li>
	<li>
		<a name="admin"></a><b>������ ������� �������������� ��</b>
		<p>		
		�� �������� "����������������� ��" ������������� ����� ��������� ������ ���� ������� �� ����� �� ��������� �������� ���. �� ��������� ������������ ������� �� ��������� �����. �� ������ ������� �������� ��������� ������. 
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-adminarea-1.png"></div>		
		</p>
	</li>	
	<li>
		<a name="order"></a><b>��� ������ ������</b>
		<p>		
		��� ������ ������� � ������ ���������� ������� ������ � �������� ������������ ��� �������. ����� ������� �������� ���� �� ������� ������ ������� � ������� ����������� � ���������.
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-buy-1.png"></div>		
		</p>
		<p>
		����� � ���� �� ������� �������� ������� �������, ������ ��������� � ������� �������.  
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-proc-1.png"></div>
		</p>
		<p>
		�������� ������� � ��������� �� ������. ��������� �������� � �������� ������� ��������. �� ������ ����� ������������ ��� ����� �� �������� ��� �� �������� ���������������� �������� ������ � ������� ����� ������.  
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-buy-2.png"></div>
		</p>
		<p>
		����������� ������� � ������������ � �������� ��������. �������� ��������� ��� ������ ��� ���� � ������� ����������. ����� ������� �� ������ "�������� � �������" ����� ��������� � ������� �������. � ����������� ������� ������� ����������� �� ����� � ����� ������ � ��� �������.
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-buy-3.png"></div>
		</p>
		<p>
		���� � ������� ��� ���������, �� �� ������ ��������� ������� ����� ������ ������ � ������ ����� �������. ������ ������� ����� ������ �� �������� ������ (�� �����������), �� ������ ������� ������� �������� �������, �� ������, ���� � ������ �������������� (���� ���������) � ����. � ������� ������ "��������".
		</p>
		<p>
		����� �����, ���� �� ������� � ������� �����-���� �����, ���������� ������ ���������� �������, �� �� ����� ������ ��� �������� ����� �� ������ "� ��� ���� �����". 
		</p>		
	</li>	
	<li>
		<a name="vkontakte"></a><b>���������� � ���������� ����� ���������</b>
		<p>	<a href="https://new.vk.com/editapp?act=create">��������</a> 2 ���������� ���������. �������� ��� ������� ���������� "IFrame/Flash", ������� �������� "���������� �������". ��� ������� ���������� "���-����", � � �������� �������� ����� ������������ ����� ������ �����. ������ ���������� IFrame ������� � ����� ����������� ��������� � ������� ��� ���� ��������� ���������.
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/spset-vk-1.png"></div>			
		</p>
		<p>	��������� ������ ����������, ��� �������� �� �������� ����. �� ����� ����� � ���������� ����������� ������� ID ���������� � ���������� ���� �� ����� ����������	"���-����".
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/spset-vk-2.png"></div>
		</p>
		<p>		
		��������� ������� ����� ������ ������ ����� �� ����� ���������. ��� ������ �������� � ������������ � ������ �������, ������������ �������� ����� ����� �� ���� �������, ��������� ��� �� �����, ��� � ���������.
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-vk-1.png"></div>		
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-vk-2.png"></div>
		</p>
	</li>		
	<li>
		<a name="buy"></a><b>������ �����������</b>
		<p>
		���������� ������ ����������� ���������� ������� ����� ������� � <a href="http://custom.simplemachines.org/mods/index.php?mod=4109">������������ ����� SMF</a>.
		����� ������ ������� ������, �������� ������ �� ����� sale@spmod.ru � ��������� ������ �����, ��� �� ���������� ������������� ������� ����. ��������� ������ �������� ���������� <b>10 ���. �.</b> � �������� � ���� ����������� ��������� � ��� ���������� ����������. �������� ��������� �� 5 �������.	
		</p>
	</li>	
</ol>
</div>
';

// Admin Area
$txt['spmod_admin_area_desc'] = '����� ������� ��� ��������� ����������� � ������� ���������� �������.';
$txt['spmod_admin_area_settings'] = '���������';
$txt['spmod_admin_area_settings_desc'] = '����� ��������� c��������� �������.';
$txt['spmod_admin_area_banner'] = '�������';
$txt['spmod_admin_area_banner_desc'] = '��������� ����������� �������� � ��������.';
$txt['spmod_admin_area_vkontakte'] = '���������� ���������';
$txt['spmod_admin_area_vkontakte_desc'] = '��������� ���������� ���������.';
$txt['spmod_admin_area_beget'] = 'Beget';
$txt['spmod_admin_area_beget_desc'] = '��������� �������� Beget ��� ���������� cron-�����.';
$txt['spmod_admin_area_permissions'] = '����� �������';
$txt['spmod_admin_area_permissions_desc'] = '��������� ���� ������� ����� ������������� � �������� ��.';
$txt['spmod_admin_area_info'] = '���������� � ���������';
$txt['spmod_admin_area_info_desc'] = '�������������� ���������� � ������� � ������� ������������ �����������.';

// Admin Area: Info
$txt['spmod_admin_area_version_spmod'] = '������ SPmod';
$txt['spmod_admin_area_version'] = '������';
$txt['spmod_admin_area_version_curl'] = '������ PHP Curl';
$txt['spmod_admin_area_forum_url'] = '����� ������';
$txt['spmod_admin_area_forum_protocol'] = '���-��������';
$txt['spmod_admin_area_forum_version'] = '������ ������';
$txt['spmod_admin_area_forum_theme'] = '���� ����������';
$txt['spmod_admin_area_forum_lang'] = '���� ������';
$txt['spmod_admin_area_forum_utf8'] = '������������ UTF-8';
$txt['spmod_admin_area_phpinfo'] = '�������� ��� ��������� PHP';

?>