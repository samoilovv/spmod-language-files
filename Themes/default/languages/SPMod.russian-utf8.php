<?php

$txt['spmod_copyright'] = 'Совместные Покупки для SMF {version} | &copy; <a href="http://spmod.ru">spmod.ru</a>';
$txt['spmod_unknownver'] = 'unknown version';
$txt['spmod'] = 'Совместные Покупки';
$txt['spmod_title'] = 'Совместные Покупки для SMF';
$txt['spmod_currency'] = ' р.';
$txt['spmod_itemcountlbl'] = ' шт.';
$txt['spmenubutton_label'] = 'Совместные покупки';
$txt['spmod_spmod'] = 'Совместные покупки';
$txt['spmod_isguest'] = 'У вас нет прав для доступа в личный кабинет организатора';
$txt['spmod_capt'] = 'Управление каталогами и закупками';
$txt['spmod_cats'] = 'Каталоги';
$txt['spmod_procurements'] = 'Закупки';
$txt['spmod_goods'] = 'Товары';
$txt['spmod_isguest'] = 'Гости не могут участвовать в совместных покупках. Вам необходимо зарегистрироваться на форуме.';
$txt['spmod_lines'] = 'Ряды и размеры';
$txt['spmod_line'] = 'Ряд';
$txt['spmod_lineisopened'] = 'Ряд открыт: ';
$txt['spmod_lineiscomplited'] = 'Ряд собран: ';
$txt['spmod_parser'] = 'Парсер';
$txt['spmod_catslist'] = 'Список ваших каталогов';
$txt['spmod_nocats'] = 'Вы еще не создали ни одного каталога.';
$txt['spmod_catname'] = 'Название каталога';
$txt['spmod_itemcount'] = 'Количество позиций';
$txt['spmod_action'] = 'Действие';
$txt['spmod_edit'] = 'Редактировать';
$txt['spmod_download'] = 'Выгрузить';
$txt['spmod_nocatid'] = 'Не задан идентификатор редактируемого каталога.';
$txt['spmod_noitemid'] = 'Не указан идентификатор закупки или товара.';
$txt['spmod_nouserid'] = 'Не указан идентификатор пользователя.';
$txt['spmod_noneworg'] = 'Не указан организаторский процент.';
$txt['spmod_nomanualtr'] = 'Не указана сумма транспортных расходов.';
$txt['spmod_noprocname'] = 'Не указано название закупки.';
$txt['spmod_notopicid'] = 'Не задан идентификатор темы!';
$txt['spmod_noboardid'] = 'Не задан раздел для создания темы!';
$txt['spmod_itemlist'] = 'Список товаров каталога';
$txt['spmod_noitems'] = 'Выбранный каталог пуст';
$txt['spmod_photo'] = 'Фото';
$txt['spmod_description'] = 'Описание';
$txt['spmod_extparams'] = 'Доп. параметры:';
$txt['spmod_articul'] = 'Артикул';
$txt['spmod_procisclosed'] = 'Закупка закрыта.';
$txt['spmod_youcannotbuy'] = 'Вы не можете совершить покупку.';
$txt['spmod_catdelconf'] = 'Вы уверены, что хотите удалить этот каталог? Все товары в нем также будут удалены!';
$txt['spmod_paramdelconf'] = 'Вы уверены, что хотите удалить этот параметр?';
$txt['spmod_jointpurchases'] = 'Совместные покупки';
$txt['spmod_itemdelconf'] = 'Вы уверены, что хотите удалить эту позицию?';
$txt['spmod_delerror'] = 'Вы не можете удалить этот заказ.';
$txt['spmod_itemaddingerror'] = 'Вы можете добавлять товары только в свои закупки.';
$txt['spmod_nopurchgooduser'] = 'Не указан идентификатор закупки, товара или пользователя';
$txt['spmod_noitem'] = 'Не указан идентификатор позиции';
$txt['spmod_noeditedpurchid'] = 'Не указан идентификатор редактируемой закупки';
$txt['spmod_count'] = 'Кол-во';
$txt['spmod_addtobuscket'] = 'Добавить в корзину';
$txt['spmod_nopurch'] = 'Не указан идентификатор закупки.';
$txt['spmod_nocat'] = 'Не указан идентификатор каталога.';
$txt['spmod_noparam'] = 'Не задан идентификатор линейки параметров.';
$txt['spmod_parsingerror'] = 'Ошибка парсинга';
$txt['spmod_filetoolarge'] = 'Файл слишком большой';
$txt['spmod_erroruploading'] = 'Ошибка загрузки';
$txt['spmod_t_orgname'] = 'Организатор';
$txt['spmod_t_orgphonenumber'] = 'Телефон для связи: ';
$txt['spmod_t_orgperc'] = 'Орг. процент: ';
$txt['spmod_t_catlist'] = 'Список каталогов: ';
$txt['spmod_t_table'] = 'Таблица заказов: ';
$txt['spmod_notpermissions'] = 'У вас нет прав для выполнения этого действия!';
$txt['spmod_topiccreationerror'] = 'Не удалось создать тему!';
$txt['spmod_imgloaderror'] = 'Во время обработки изображения произошла ошибка. Попробуйте снова!';
$txt['spmod_nocustomers'] = 'В закупке пока еще нет покупателей';
$txt['spmod_c_name'] = 'Имя';
$txt['spmod_c_count'] = 'Количество заказов';
$txt['spmod_c_summa'] = 'Сумма';
$txt['spmod_c_org'] = 'Орг. процент';
$txt['spmod_c_itogo'] = 'Итого';
$txt['spmod_c_tr'] = 'ТР';
$txt['spmod_c_bank'] = 'Банк';
$txt['spmod_successmsg'] = 'Товар успешно добавлен в корзину. Перейти в ';
$txt['spmod_proctopic'] = 'тему закупки';
$txt['spmod_myproc'] = 'Участвую в закупках';
$txt['spmod_nogoodname'] = 'Не указано название товара';
$txt['spmod_nogoodprice'] = 'Не указана цена товара';
$txt['spmod_trmode1'] = '% от суммы заказа';
$txt['spmod_trmode2'] = ' делим поровну на всех участников закупки';
$txt['spmod_trmode3'] = ' делим пропорционально заказу';
$txt['spmod_trmode4'] = ' делим по количеству заказанных позиций';
$txt['spmod_trmode5'] = ' расчитывается организатором';
$txt['spmod_distrib'] = 'Раздача';
$txt['spmod_myprocempty'] = 'Вы пока не участвуете ни в одной закупке';
$txt['spmod_spadmin'] = 'Администрирование СП';
$txt['spmod_rocempty'] = 'Нет ни одной закупки';
$txt['spmod_datefrom'] = ' от ';
$txt['spmod_dateto'] = ' до ';
$txt['spmod_dateopen'] = 'Дата открытия';
$txt['spmod_comission'] = 'Комиссия';
$txt['spmod_newordersubj'] = 'Новый заказ в Вашей закупке';
$txt['spmod_userspace'] = 'Пользователь ';
$txt['spmod_spacemakesorderinproc'] = ' сделал заказ в вашей закупке';
$txt['spmod_delordersubj'] = 'Отмена заказа в Вашей закупке';
$txt['spmod_spacedelsorderinproc'] = ' отказался от заказа в вашей закупке';
$txt['spmod_summaorgpercent'] = 'Сумма орг.%';
$txt['spmod_targetprocnotfound'] = 'Закупка не найдена';
$txt['spmod_paymentmessagesubject'] = 'Оплачена закупка';
$txt['spmod_paymentmessagebody'] = 'Пользователь {user} совершил оплату в вашей закупке "{procname}" {procurl}.';
$txt['spmod_paymessagesubject'] = 'Оплачиваем закупку';
$txt['spmod_paymessagebody'] = 'Вы делали заказ в закупке [url={procurl}]{procname}[/url] . Статус закупки изменился на "Оплачиваем". Карта для оплаты: {card}';
$txt['spmod_payment'] = 'Оплата';
$txt['spmod_paymentcomplite'] = 'Оплачено';
$txt['spmod_paymentdate'] = 'Дата оплаты';
$txt['spmod_nopayment'] = 'Оплаты не было';
$txt['spmod_koplate'] = 'К оплате';
$txt['spmod_sphelp'] = 'Справка';
$txt['spmod_paymentmessage'] = 'Оплачиваем! После оплаты, пожалуйсте, укажите перечисленную сумму [url={scripturl}?action=spmod;sa=myproc]здесь[/url].';
$txt['spmod_print'] = 'Печать';
$txt['spmod_buyoutnumber'] = 'Выкуп №';
$txt['spmod_delcats'] = 'Удалить выбранные каталоги';
$txt['spmod_delcatsconfirm'] = 'Уверены, что хотите удалить выбранные каталоги? Все товары в этих каталогах также будут удалены.';
$txt['spmod_delgoods'] = 'Удалить выбранные позиции';
$txt['spmod_delgoodsconfirm'] = 'Уверены, что хотите удалить выбранные позиции?';
$txt['spmod_closebyspadminsubject'] = 'Ваша закупка закрыта администратором';
$txt['spmod_closebyspadmin'] = 'Ваша закупка "{procname}" {procurl} была закрыта администратором совместных покупок {spadmin}.';
$txt['spmod_deletedbyspadminsubject'] = 'Ваша закупка удалена администратором';
$txt['spmod_deletedbyspadmin'] = 'Ваша закупка "{procname}" {procurl} была удалена администратором совместных покупок {spadmin}.';
$txt['spmod_delprocs'] = 'Удалить выбранные закупки';
$txt['spmod_delprocconfirm'] = 'Уверены, что хотите удалить выбранные закупки?';
$txt['spmod_nocard'] = 'не указана';
$txt['hName'] = 'Наименование';	$txt['hName2'] = 'Название';		$txt['hName3'] = 'Имя';
$txt['hArticul'] = 'Артикул';	$txt['hArticul2'] = 'Номер';		$txt['hArticul3'] = 'идентиф';
$txt['hDescr'] = 'Описание';	$txt['hDescr2'] = 'Характеристики';	$txt['hDescr3'] = 'Прочее';
$txt['hPrice'] = 'Цена';		$txt['hPrice2'] = 'Стоимость';		$txt['$hPrice3'] = 'Цены';
$txt['hPic'] = 'Картинк';		$txt['hPic2'] = 'Изображени';		$txt['$hPic3'] = 'Фотограф'; 
$txt['hLink'] = 'Ссылка';		$txt['hLink2'] = 'URL';				$txt['$hLink3'] = 'адрес';
$txt['hRow'] = 'Ряд';			$txt['hRow2'] = 'Количество';		$txt['$hRow3'] = 'Кол-во';
$txt['hParam'] = 'Параметр'; 	$txt['hParam2'] = 'Размер';			$txt['$hParam3'] = 'Цвет';
$txt['hValues'] = 'Значения'; 	$txt['hValues2'] = 'Знач.';			$txt['$hValues3'] = 'Размеры';
$txt['spmod_processing'] = ' уже обрабатывается.';
$txt['spmod_processing2'] = 'выполняется...';
$txt['spmod_waiting'] = 'ожидание...';
$txt['spmod_taskisdone'] = 'завершено';
$txt['spmod_taskispaused'] = 'приостановлена';
$txt['spmod_taskisdeleted'] = 'удалено';
$txt['spmod_taskisunknown'] = 'неизвестно';
$txt['spmod_notaskalbumgroupid'] = 'Не указан идентификатор задачи, альбома или группы.';
$txt['spmod_notaskid'] = 'Не указан идентификатор задачи.';
$txt['spmod_tasklist'] = 'Список ваших задач';
$txt['spmod_notask'] = 'Вы пока еще не создали ни одной задачи';
$txt['spmod_albumname'] = 'Название альбома';
$txt['spmod_taskstart'] = 'Время начала';
$txt['spmod_interval'] = 'Интервал';
$txt['spmod_process'] = 'Процесс';
$txt['spmod_taskstopconfirm'] = 'Вы уверены, что хотите остановить задачу?';
$txt['spmod_taskstartconfirm'] = 'Вы уверены, что хотите запустить задачу?';
$txt['spmod_taskdelconfirm'] = 'Вы уверены, что хотите удалить задачу?';
$txt['spmod_pause'] = 'Пауза';
$txt['spmod_start'] = 'Старт';
$txt['spmod_notocken'] = 'Не указан токен.';
$txt['spmod_tasks'] = 'Задачи';
$txt['spmod_albumcreateerror'] = 'Ошибка при создании альбома ВКонтакте';
$txt['spmod_nonexistenttaskstatus'] = 'Несуществующий статус задачи';
$txt['spmod_nocatidorparserurl'] = 'Не указан идентификатор каталога или адрес для парсинга.';
$txt['spmod_notlogged'] = 'Вы не авторизованы на форуме.';
$txt['spmod_catinuse'] = 'Вы не можете удалить этот каталог, так как он испоьзуется в текущей закупке.';

//Parsers
$txt['spmod_nopageurl'] = 'Не указан адрес страницы';
$txt['spmod_nocurl'] = 'Парсеры и некоторые другие функции не будут работать на вашем хостинге из-за отсутствия поддержки PHP cURL!';

//Template text
$txt['spmod_addingparams'] = 'Дополнительные параметры товаров, такие как размер и цвет';
$txt['spmod_noaddingparams'] = 'Вы пока еще не создали не одного ряда или параметра.';
$txt['spmod_pages'] = 'Страницы';
$txt['spmod_paramname'] = 'Название параметра';
$txt['spmod_paramalias'] = 'Обозначение в каталоге';
$txt['spmod_vallist'] = 'Список значений';
$txt['spmod_delete'] = 'Удалить';
$txt['spmod_decrement'] = 'Уменьшить';
$txt['spmod_edit'] = 'Изменить';
$txt['spmod_loadcat'] = 'Загрузить каталог из XML файла';
$txt['spmod_loadcsv'] = 'Загрузить каталог из CSV файла';
$txt['spmod_file'] = 'Файл';
$txt['spmod_filefielddescr'] = 'Файл должен быть сохранен в кодировке Windows ANSI и содержать столбцы <i>\'Наименование\', \'Артикул\', \'Описание\', \'Цена\', \'Картинка\', \'Ссылка\', \'Ряд\', \'Параметр\', \'Значения\'</i>. Разделителем является знак точки с запятой. Скачать примеры: <a href="https://yadi.sk/d/ImavJYlOkwmvk">каталог без рядов</a>, <a href="https://yadi.sk/d/ntS5WXWgkwmvr">каталог с рядами без размеров</a>, <a href="https://yadi.sk/d/3UXn1Rmrkwmvv">каталог с рядами и размерами</a>.';
$txt['spmod_xmlfilefielddescr'] = 'Выберите XML-файл, в который Вы ранее выгрузили каталог';
$txt['spmod_load'] = 'Загрузить';
$txt['spmod_createnewcat'] = 'Создать новый каталог';
$txt['spmod_editcat'] = 'Редактировать каталог';
$txt['spmod_name'] = 'Название';
$txt['spmod_namedescr'] = 'Введите название каталога';
$txt['spmod_linecount'] = 'Количество в ряде';
$txt['spmod_linecountdescr'] = 'При закупке без рядов оставьте в данном поле 0';
$txt['spmod_usercat'] = 'Пользовательский каталог';
$txt['spmod_usercatdescr'] = 'В каталог пользователи смогут сами добавлять товары'; 
$txt['spmod_param1'] = 'Линейка характеристик товаров в каталоге';
$txt['spmod_paramdescr'] = 'Выберите линейку параметров товаров (размер, цвет и т.д.) в каталоге';
$txt['spmod_param2'] = 'Дополнительная линейка характеристик товаров в каталоге';
$txt['spmod_param2descr'] = 'При необходимости вы можете выбрать вторую линейку параметров товаров данного каталога';
$txt['spmod_save'] = 'Сохранить';
$txt['spmod_addparams'] = 'Добавить новую линейку параметров';
$txt['spmod_editparams'] = 'Редактировать линейку параметров';
$txt['spmod_paramsnamedescr'] = 'Название линейки параметров. Например: "Размеры для каталога Old Navy"';
$txt['spmod_paramsdescr'] = 'Обозначение дополнительных параметров товара в каталоге. Например: "Размер" или "Цвет"';
$txt['spmod_values'] = 'Возможные значения';
$txt['spmod_valuesdescr'] = 'Перечислите через запятую все возможные значения параметра. Например: "Красный, Желтый, Синий, Фиолетовый"';
$txt['spmod_catchoose'] = 'Выбор каталога';
$txt['spmod_curcat'] = 'Текущий каталог';
$txt['spmod_curcatdescr'] = 'Вы можете переместить позицию в другой каталог';
$txt['spmod_additem'] = 'Добавить позицию';
$txt['spmod_edititem'] = 'Редактировать позицию';
$txt['spmod_itemdescr'] = 'Название товара';
$txt['spmod_description'] = 'Описание';
$txt['spmod_descriptiondescr'] = 'Краткое описание товара';
$txt['spmod_articul'] = 'Артикул';
$txt['spmod_articuldescr'] = 'Артикул в каталоге поставщика';
$txt['spmod_imgurl'] = 'URL изображения';
$txt['spmod_imgurldescr'] = 'Необходимо указать адрес картинки';
$txt['spmod_imgfile'] = 'Или выберите файл на диске';
$txt['spmod_imgfiledescr'] = 'Загрузка изображения с Вашего компьютера';
$txt['spmod_link'] = 'Ссылка';
$txt['spmod_linkdescr'] = 'Ссылка на товар в каталоге поставщика';
$txt['spmod_price'] = 'Цена';
$txt['spmod_pricedescr'] = 'Цена товара';
$txt['spmod_addingparam'] = 'Доп. параметр';
$txt['spmod_addingparamdescr'] = 'Выбор дополнительного параметра: цвет, размер и т.д.';
$txt['spmod_addingparam2'] = 'Второй доп. параметр';
$txt['spmod_dontadd'] = 'Не добавлять';
$txt['spmod_choose'] = 'Выбрать';
$txt['spmod_ordered'] = 'Добавлено в заказ!';
$txt['spmod_orderunbay'] = 'Ваш заказ удален!';
$txt['spmod_orderdel'] = 'Заказ удален!';                     
$txt['spmod_addorder'] = 'Добавлено к заказу!';
$txt['spmod_mypurchase'] = 'Мои закупки';
$txt['spmod_purchaselist'] = 'Текущие закупки';
$txt['spmod_closedpurchaselist'] = 'Архивные закупки';
$txt['spmod_purchaseopen'] = 'Открыта';
$txt['spmod_purchasestopdate'] = 'Дата стопа';
$txt['spmod_purchasedelconf'] = 'Вы уверены, что хотите удалить эту закупку?';
$txt['spmod_newpurchase'] = 'Создать новую закупку';
$txt['spmod_createpurchase'] = 'Редактировать закупку';
$txt['spmod_purchstat_open'] = 'Открыта';
$txt['spmod_purchstat_stop'] = 'Стоп';
$txt['spmod_purchstat_wait'] = 'Ждем счет';
$txt['spmod_purchstat_pay'] = 'Оплачиваем';
$txt['spmod_purchstat_shipment'] = 'Отгрузка';
$txt['spmod_purchstat_overorder'] = 'Дозаказы';
$txt['spmod_purchstat_stopoverorder'] = 'Стоп. Дозаказы';
$txt['spmod_purchstat_inway'] = 'В пути';
$txt['spmod_purchstat_close'] = 'Закрыта';
$txt['spmod_purchstat_doclose'] = 'Закрыть';
$txt['spmod_purchstat_gone'] = 'Приехало';
$txt['spmod_purchstat_get'] = 'Получена';
$txt['spmod_purchstat_issuance'] = 'Разбираем';
$txt['spmod_purchstat_vocation'] = 'Закупка на каникулах';
$txt['spmod_purchstat_wasnt'] = 'Не состоялась';
$txt['spmod_purchname'] = 'Название закупки';		
$txt['spmod_purchnamedescr'] = 'Краткое описание закупаемых товаров';
$txt['spmod_purchstopdescr'] = 'Необходимо указать дату окончания закупки';
$txt['spmod_orgpercent'] = 'Орг. процент';
$txt['spmod_orgpercentdescr'] = 'Введите организаторский процент закупки';
$txt['spmod_minsum'] = 'Минимальная сумма или минимальное количество заказа';
$txt['spmod_minsumdescr'] = 'Укажите минимальную сумму выкупа или минимальное количество заказа. Если указан 0, то сумма или количество не будет учитываться';
$txt['spmod_topicid'] = 'Раздел СП';
$txt['spmod_topiciddescr'] = 'Выберите раздел, где будет создана тема с закупкой';
$txt['spmod_status'] = 'Статус';
$txt['spmod_statusdescr'] = 'Выберите статус закупки';
$txt['spmod_banner'] = 'Баннер';
$txt['spmod_bannerdescr'] = 'Вы можете здесь указать URL-адрес изображения вашей закупки. Рекомендуемый размер баннера 200x120 точек. Адрес ссылки на изображение должен начинаться на http://';
$txt['spmod_catsdescr'] = 'Отметьте нужные каталоги галочкой';
$txt['spmod_selectall'] = 'Выделить все';
$txt['spmod_purchcode'] = 'Код для вставки в сообщение';
$txt['spmod_catlistcode'] = 'Код списка каталогов из Вашей закупки';
$txt['spmod_catlistcodedescr'] = 'При необходимости редактирования скопируйте и вставьте этот код списка каталогов в описание Вашей закупки';
$txt['spmod_tablecode'] = 'Код таблицы заказов Вашей закупки';
$txt['spmod_tablecodedescr'] = 'При необходимости редактирования скопируйте и вставьте этот код таблицы в описание Вашей закупки';
$txt['spmod_purchiderror'] = 'Не указан идентификатор текущей закупки.';
$txt['spmod_catalog'] = 'Или выберите из списка';
$txt['spmod_catalogdescr'] = 'Выберите один из существующих каталогов. Если введено название каталога в предыдущем пункте, то выбор будет проигнорирован.';
$txt['spmod_newcatalog'] = 'Каталог';
$txt['spmod_newcatalogdescr'] = 'Введите название нового каталога для выгрузки.';
$txt['spmod_url'] = 'Список URL-адресов';
$txt['spmod_urldescr'] = 'Укажите URL-адреса страниц, с которых будет призведенена выгрузка товаров. Каждый адрес необходимо указывать с новой строки. Или скопируйте и вставьте сюда исходный html код страницы. Тип парсера будет определен автоматически из списка доступных:';
$txt['spmod_execute'] = 'Выполнить';
$txt['spmod_done'] = 'Выполнено!';
$txt['spmod_orgfi'] = 'Имя и Фамилия';
$txt['spmod_orgfidescr'] = 'Введите ваше имя и фамилию';
$txt['spmod_orgphone'] = 'Номер телефона';
$txt['spmod_orgphonedescr'] = 'Введите ваш номер телефона';
$txt['spmod_orgrequisites'] = 'Реквизиты для оплаты';
$txt['spmod_orgrequisitesdescr'] = 'Введите номер вашей карты. Он будет выслан участникам вашей закупки в личном сообщении.';
$txt['spmod_orgvkpage'] = 'Страница ВКонтакте';
$txt['spmod_orgvkpagedescr'] = 'Введите адрес Вашей страницы ВКонтакте';
$txt['spmod_website'] = 'Сайт поставщика';
$txt['spmod_websitedescr'] = 'Укажите адрес сайта поставщика или магазина';
$txt['spmod_customers'] = 'Отчет по закупке ';
$txt['spmod_report'] = 'Отчет';
$txt['spmod_turnover'] = 'Общая сумма закупки:';
$txt['spmod_ordersumma'] = 'Общая сумма заказа:';
$txt['spmod_c_comission'] = 'Комиссия сайта:';
$txt['spmod_orgpercsumma'] = 'Прибыль организатора:';
$txt['spmod_goodsearch'] = 'Поиск по названию или артикулу';
$txt['spmod_goodsearchbutton'] = 'Найти';
$txt['spmod_bankfee'] = 'Комиссия банка за перевод';
$txt['spmod_bankfeedescr'] = 'Введите в процентах комиссию банку за перевод средств';
$txt['spmod_tr'] = 'Транспортные расходы';
$txt['spmod_trdescr'] = 'Сумма ТР будет поделена между всеми участниками закупки в соответствие с выбранным методом. Если выбран "Фиксированный процент", то в поле необходимо ввести процент от суммы закупки. В остальных случаях вводится общая сумма ТР в рублях.';
$txt['spmod_pastecode'] = 'Код вставки';
$txt['spmod_yourproclistcode'] = 'Код списка Ваших закупок';
$txt['spmod_yourproclistcodedescr'] = 'Вы можете вставить этот код в свою подпись или в любое сообщение на форуме';
$txt['spmod_p_trmode1'] = 'Фиксированный процент';
$txt['spmod_p_trmode2'] = 'Делим поровну';
$txt['spmod_p_trmode3'] = 'Делим пропорционально заказу';
$txt['spmod_p_trmode4'] = 'Делим по количеству позиций';
$txt['spmod_p_trmode5'] = 'Расчитывается организатором';
$txt['spmod_p_minsum'] = 'Минимальная сумма';
$txt['spmod_p_mincount'] = 'Минимальное количество';
$txt['spmod_summaryproccode'] = 'Код итоговой таблицы по закупке';
$txt['spmod_summaryproccodedescr'] = 'Вы можете вставить этот код в тему с закупкой';
$txt['spmod_nick'] = 'Ник';
$txt['spmod_orgp'] = 'Орг.%';
$txt['spmod_forpay'] = 'Итого к оплате';
$txt['spmod_distrdistr'] = 'Какой район для раздачи Вам предпочтительней';
$txt['spmod_distrdistrdescr'] = 'Заполняется, если вы собираетесь участвовать в совместных закупках';
$txt['spmod_defaultcaption'] = 'размер';
$txt['spmod_buyout'] = 'Номер выкупа';
$txt['spmod_buyoutdescr'] = 'Введите номер выкупа вашей закупки.';
$txt['spmod_another'] = 'Другое';
$txt['spmod_anothercomment'] = 'Другое (по договоренности с организаторами)';
$txt['spmod_orgaddress'] = 'Адрес доставки';
$txt['spmod_orgaddressdescr'] = 'Введите свой почтовый адрес, на который будет выслан Ваш заказ';
$txt['spmod_parserparams'] = 'Параметры парсера';
$txt['spmod_nochoose'] = 'не выбран';
$txt['spmod_any'] = 'любой';
$txt['spmod_vkproclink'] = 'Приглашение в закупку';
$txt['spmod_vkproclinkdescr'] = 'Вы можете пригласить пользователей из социальной сети ВКонтакте для участия в вашей закупке. Скопируйте и опубликуйте текст на стене сообщества.';
$txt['spmod_openpurch'] = 'Открыта закупка';
$txt['spmod_chooseonsite'] = 'Выбираем на сайте';
$txt['spmod_vkinviting'] = 'Сделать заказ можно здесь {vklink}
Или на форуме {topiclink}

После заказа с вами свяжется организатор закупки для уточнения подробностей оплаты и доставки.
Всем приятных покупок :)

#совместныепокупки #spmod';
$txt['spmod_createtask'] = 'Создать задачу';
$txt['spmod_vkprocdescr'] = 'Каталоги из этой закупки загрузятся в альбом вашего сообщества ВКонтакте';
$txt['spmod_vkgroup'] = 'Выберите сообщество';
$txt['spmod_vkgroupdescr'] = 'Сообщества, в которых вы являетесь администратором или редактором';
$txt['spmod_vkcreatealbum'] = 'Создать альбом';
$txt['spmod_vkcreatealbumdescr'] = 'Введите имя нового альбома в вашей группе. Если вы оставите это поле пустым, то выгрузка будет происходить в альбом, выбранный в следующем пункте.';
$txt['spmod_vkchoosealbum'] = 'Или выберите существующий альбом';
$txt['spmod_vkchoosealbumdescr'] = 'Альбомы, в которые вы можете добавлять фотографии. Если вы выше ввели имя нового альбома, то это поле будет проигнорировано';
$txt['spmod_vkdelay'] = 'Интервал загрузки';
$txt['spmod_vkdelaydescr'] = 'Пауза между загрузкой каждой фотографии';
$txt['spmod_sec'] = 'сек.';
$txt['spmod_min'] = 'мин.';
$txt['spmod_hour'] = 'час';
$txt['spmod_vkstarttask'] = 'Время старта';
$txt['spmod_vkstarttaskdescr'] = 'Выберите дату и время старта задачи';
$txt['spmod_vkstart'] = 'Запуск';
$txt['spmod_recive_email_notification'] = 'Получать на e-mail уведомления из закупок';
$txt['spmod_recive_email_notification_descr'] = 'Получать на электронную почту уведомления о действиях сделанных в организованных вами закупках.';
$txt['spmod_recive_email_buy'] = 'новый заказ';
$txt['spmod_recive_email_cancel'] = 'отмена заказа';
$txt['spmod_uploadingcat'] = 'Каталог для загрузки';
$txt['spmod_uploadingcat_descr'] = 'Товары из файла будут добавлены в выбранный каталог. Если выше вы ввели название нового каталога, то выбор будет проигнорирован.';

//Sub
$txt['spmod_username'] = 'Имя пользователя';
$txt['spmod_pic'] = 'Изображение';
$txt['spmod_purchname'] = 'Название покупки'; // TODO: dublicate $txt['spmod_purchname']
$txt['spmod_priceandcount'] = 'Цена и количество';
$txt['spmod_add'] = 'Добавить';
$txt['spmod_ordertoo'] = 'Заказать себе';
$txt['spmod_refuse'] = 'Отказаться';
$txt['spmod_no'] = 'Нет в наличии';
$txt['spmod_yes'] = 'Есть в наличии';
$txt['spmod_metoo'] = 'И мне тоже нужно!';
$txt['spmod_notavailable'] = 'недоступно';
$txt['spmod_totalsum'] = 'Общая сумма заказа';
$txt['spmod_totalcount'] = 'Заказано';
$txt['spmod_collected'] = 'Собрано';
$txt['spmod_stillneeded'] = 'Осталось собрать';                     
$txt['spmod_procnotfound'] = '<font color="red">[Закупка еще не создана]</font>';
$txt['spmod_info'] = 'Информация';
$txt['spmod_orginfo'] = 'Контактная информация';
$txt['spmenubutton_enabled'] = 'Активизировать кнопку меню';
$txt['spmenubutton_enabled_sub'] = 'Включить или выключить кнопку меню "Личный кабинет организатора СП"';
$txt['spmod_spcats'] = 'Список СП категорий';
$txt['spmod_spcats_sub'] = 'Укажите через запятую идентификаторы категорий форума, в которых будут создаваться темы с закупками.';
$txt['spmod_sparchive'] = 'Архивный раздел';
$txt['spmod_sparchive_sub'] = 'Укажите идентификатор архивного раздела, куда будут перемещаться темы закупок со статусом "Закрыта" или "Не состоялась".';
$txt['spmod_premoderate'] = 'Премодерация закупок';
$txt['spmod_premoderate_sub'] = 'Созданные закупки должен одобрять администратор или модератор раздела. Для работы функции необходимо, также, включить систему премодерации в <a href="/index.php?action=admin;area=corefeatures">общих настройках</a> форума.';
$txt['spmod_maxpercent'] = 'Максимальный процент'; 
$txt['spmod_maxpercent_sub'] = 'Максимальный организаторский процент, который разрешено устанавливать вашим организаторам СП'; 
$txt['spmod_commission'] = 'Комиссия сайта'; 
$txt['spmod_commission_sub'] = 'Введите комиссию в процентах, которую организаторы должны отчислять администрации сайта';
$txt['spmod_proctemplate'] = 'Шаблон закупки';
$txt['spmod_proctemplate_sub'] = 'Вы можете использовать BB-коды, а также ключи для автоподстановки: {title} - название закупки, {descr} - описание закупки, {name} - имя организатора, {phone} - номер телефона, {satus} - статус закупки, {percent} - орг. процент, {bank} - комиссия банка, {tr} - транспортные расходы, {trmode} - способ расчета ТР, {website} - адрес сайта, {stop} - дата стопа, {min} - минимальная сумма заказа или минимальное количество, {minmode} - критерий сбора "минималки" (сумма или количество), {catalogs} - список каталогов, {table} - таблица заказов';
$txt['spmod_distriblist'] = 'Список районов раздач';
$txt['spmod_distriblist_sub'] = 'Введите каждый район раздачи на новой строке';
$txt['spmod_catforbanner'] = 'Идентификатор категории для баннера';
$txt['spmod_catforbanner_sub'] = 'Укажите идентификатор категории, перед которым будет размещен баннер с недавно открытыми закупками.';
$txt['spmod_checkbanner'] = 'Включить сквозной баннер';
$txt['spmod_checkbanner_sub'] = 'Баннер с недавно открытыми закупками будет отображаться на каждой странице форума под главным меню.';
$txt['spmod_vkdescrtemplate'] = 'Шаблон подписи к выгружаемым фото';
$txt['spmod_vkdescrtemplate_sub'] = 'Используйте ключи для автоподстановки: {title} - название товара, {descr} - описание товара, {price} - цена, {url} - ссылка на страницу магазина, {order} - ссылка для заказа.';
$txt['spmod_t_price'] = 'Цена:';
$txt['spmod_t_org'] = 'Орг.%:';
$txt['spmod_t_count'] = 'Кол-во:';
$txt['spmod_t_summa'] = 'Сумма:';
$txt['spmod_orderstring'] = 'Строка заказа';
$txt['spmod_os_name'] = 'Название, размер, комментарии';
$txt['spmod_os_price'] = 'Цена без орг.%';
$txt['spmod_os_order'] = 'Заказать';
$txt['spmod_noorg'] = 'Не указан организатор';
$txt['spmod_onebuydelconf'] = 'Вы уверены, что хотите отказаться от этого заказа?';
$txt['spmod_buydelconf'] = 'Вы уверены, что хотите удалить весь заказ? Данная позиция будет удалена из заказа полностью.';
$txt['spmod_jointoline'] = 'Для участния в ряде выберите ';
$txt['spmod_join'] = 'Участвовать';
$txt['spmod_purctable'] = 'Таблица покупок';
$txt['spmod_openlines'] = 'Открытые ряды';
$txt['spmod_addaddress'] = 'Адреса доставки';
$txt['spmod_addaddress_sub'] = 'Включить адреса доставки у пользователей';
$txt['spmod_vkpublic'] = 'Ваш паблик ВКонтакте';
$txt['spmod_vkpublic_sub'] = 'Ссылка на паблик ВКонтакте, в котором вы планируете организовывать совместные покупки';
$txt['spmod_vkapp'] = 'Приложение ВКонтакте';
$txt['spmod_vkapp_sub'] = 'Ссылка на приложение ВКонтакте';
$txt['spmod_vkid'] = 'ID приложения ВКонтакте';
$txt['spmod_vkid_sub'] = 'Введите идентификатор вашего приложения ВКонтакте';
$txt['spmod_vksecret'] = 'Защищенный ключ приложения ВКонтакте';
$txt['spmod_vksecret_sub'] = 'Введите защищенный ключ вашего приложения ВКонтакте';
$txt['spmod_vkhelp'] = 'Страница помощи';
$txt['spmod_vkhelp_sub'] = 'Адрес страницы помощи по заказам через приложение ВКонтакте';
$txt['spmod_begetlogin'] = 'Логин Beget';
$txt['spmod_begetlogin_sub'] = 'Введите логин пользователя Beget для доступа к cron-задачам';
$txt['spmod_begetpass'] = 'Пароль Beget';
$txt['spmod_begetpass_sub'] = 'Введите пароль пользователя Beget для доступа к cron-задачам';
$txt['spmod_begetphppath'] = 'Путь php';
$txt['spmod_begetphppath_sub'] = 'Введите полный путь к интерпретатору php на вашем сервере для выполнения отложенных задач';
$txt['spmod_items_in_banner'] = 'Количество закупок в баннере';
$txt['spmod_items_in_banner_sub'] = 'Количество закупок, которые будут выводится в баннере (по умолчанию - 5)';
$txt['spmod_slider_enabled'] = 'Включить слайдер для баннера';
$txt['spmod_slider_enabled_sub'] = 'Баннер будет отображаться в слайдере с прокруткой элементов.';
$txt['spmod_slider_autoplay'] = 'Автопрокрутка слайдера';
$txt['spmod_slider_autoplay_sub'] = 'Автоматическая прокрутка элементов в слайдере.';
$txt['spmod_slider_speed'] = 'Скорость автопрокрутки слайдера';
$txt['spmod_slider_speed_sub'] = 'Количество секунд, через которое будут пролистываться элементы в слайдере.';
$txt['spmod_slider_titles'] = 'Показывать названия закупок в слайдере';
$txt['spmod_slider_titles_sub'] = 'Показывать названия закупок под изображениями в слайдере.';
$txt['spmod_slider_titles_cut'] = 'Обрезать длинные названия закупок';
$txt['spmod_slider_titles_cut_sub'] = 'Количество символов до которого обрезать названия закупок при отображении в слайдере.';
$txt['spmod_orders_hide_rows'] = 'Сворачивать списки заказов, в которых строк больше';
$txt['spmod_orders_hide_rows_sub'] = 'Сворачивать список заказов в теме закупки, если в нем больше строк, чем задано (0 - отключено).';
$txt['spmod_orders_unhide_rows'] = 'Отображать строк в свернутых списках заказов';
$txt['spmod_orders_unhide_rows_sub'] = 'Если включено сворачивание больших списков заказов, можно задать количество последних строк, которые будут все равно отображаться (0 - отключено).';
$txt['spmod_banner_order'] = 'Порядок закупок в баннере';
$txt['spmod_banner_order_last'] = 'новые';
$txt['spmod_banner_order_random'] = 'случайные';
$txt['spmod_banner_order_sub'] = 'Порядок вывода закупок в баннере/слайдере.';

//Permissions 
$txt['permissiongroup_spmod'] = 'Совместные Покупки';
$txt['permissionname_view_spbuy'] = 'Делать покупки';
$txt['permissionhelp_view_spbuy'] = 'Пользователям разрешено просматривать каталоги и делать покупки';
$txt['permissionname_view_spmod'] = 'Личный кабинет организатора';
$txt['permissionhelp_view_spmod'] = 'Позволяет пользователям получить доступ в личный кабинет организатора СП';
$txt['permissionname_view_spadmin'] = 'Администрирование СП';
$txt['permissionhelp_view_spadmin'] = 'Доступ к личному кабинету администратора совместных покупок';

//Print Page
$txt['spprintpage_proc'] = 'Закупка';
$txt['spprintpage_vsego'] = 'Всего';
$txt['spprintpage_waspaied'] = 'Перечислено';
$txt['spprintpage_subtext'] = '<i>С любовью,<br />команда организаторов СП {boardurl}</i>';

//Banner template
$txt['spmod_t_recentproc'] = 'Недавно открытые закупки';

//Permissions errors
$txt['cannot_view_spbuy'] = 'У вас нет прав совершать покупки. Пожалуйста, обратитесь к администратору.';
$txt['cannot_view_spmod'] = 'У вас нет доступа в личный кабинет организатора СП. Пожалуйста, обратитесь к администратору.';
$txt['cannot_view_spadmin'] = 'У вас нет прав доступа в администраторский раздел СП.';

//Parsers
$txt['sima-land.ru'] = 'Сима-ленд';
$txt['stok-m.ru'] = 'Сток Маркет';
$txt['natali37.ru'] = 'Натали';
$txt['votonia.ru'] = 'ВотОня';
$txt['sp-savage.ru'] = 'Savage';
$txt['volgoshoes.ru'] = 'Волгошуз';
$txt['odezhda-master.ru'] =  'Одежда Мастер';
$txt['xn--80abzqel.xn--p1ai'] = 'Байрон';
$txt['ikea.com'] = 'Икея';

//VK App
$txt['spmodvk_topublic'] = 'В группу ';
$txt['spmodvk_allprocs'] = 'Все текущие закупки';
$txt['spmodvk_howtomakeorder'] = 'Как сделать заказ';

//proc/cron tasks
$txt['spmodtask_makeorder'] = 'Сделайте заказ в комментариях или просто кликните по ссылке ';

//Help
$txt['spmod_helptitle'] = 'Справка';
$txt['spmod_help'] = '
<div class="sp_index">
<center><h1>Модификация "Совместные покупки" для форумов SMF</h1></center>
<ol>	
	<li>
		<a href="#mainfutures">Основные возможности</a>
		<ol>
			<li>
				<a href="#formembers">Для участников</a>
			</li>
			<li>
				<a href="#fororganizers">Для организаторов</a>
			</li>
			<li>
				<a href="#foradmins">Для администратора</a>
			</li>
		</ol>
	</li>	
	<li>
		<a href="#settings">Установка и настройка</a>
	</li>
	<li>
		<a href="#memberlk">Личный кабинет участника закупки</a>
		<ol>
			<li>
				<a href="#membercontact">Контактная информация</a>
			</li>
			<li>
				<a href="#membersp">Участвую в закупках</a>
			</li>
		</ol>
	</li>
	<li>
		<a href="#orglk">Личный кабинет организатора</a>
		<ol>
			<li>
				<a href="#orgcontact">Контактная информация</a>
			</li>
			<li>
				<a href="#orgcatalogs">Каталоги</a>				
			</li>
			<li>
				<a href="#orggoods">Товары</a>
			</li>			
			<li>
				<a href="#orgprocs">Закупки</a>
			</li>	
			<li>
				<a href="#orgparameters">Ряды и размеры</a>
			</li>
			<li>
				<a href="#orgparser">Парсер</a>
			</li>			
			<li>
				<a href="#orgsp">Участвую в закупках</a>
			</li>			
		</ol>
	</li>
	<li>
		<a href="#admin">Личный кабинет администратора СП</a>		
	</li>
	<li>
		<a href="#order">Как делать заказы</a>		
	</li>
	<li>
		<a href="#vkontakte">Интеграция с социальной сетью ВКонтакте</a>		
	</li>	
	<li>
		<a href="#buy">Купить модификацию</a>		
	</li>	
</ol>
</div>
<br />
<div class="sp_help">
<ol>	
	<li>
		<a name="mainfutures"></a>
		<span>Основные возможности</span><br /><br />
		<ol>
			<li>
				<a name="formembers"></a>
				<span>Для участников</span><br /><br />
				<ul>
					<li>Возможность выбрать и сделать заказ "в один клик" из каталога товаров.</li>
					<li>Пользовательские каталоги. Участники закупок могут самостоятельно добавлять позиции в специальный каталог и затем делать из него покупки.</li>
					<li>Поиск товара в каталоге по названию или по артикулу.</li>
					<li>Также возможно делать заказы с использованием "строки заказа".</li>
					<li>Личный кабинет Участника закупки, где отображается список закупок, в которых вы принимаете участие, и их статус.</li>
					<li>Информирование об оплате заказа через систему личных сообщений.</li>
					<li>Архив закрытых закупок.</li>
					<li>Выбор предпочитаемого офиса раздач.</li>
				</ul>
			</li>
			<br />
			<li>
				<a name="fororganizers"></a>
				<span>Для организаторов</span><br /><br />
				<ul>
					<li>Создание, редактирование и удаление закупок и каталогов с товарами.</li>
					<li>Автоматическое создание тем с закупкой по заранее подготовленному шаблону с возможностью внесения любых изменений в процессе закупки.</li>
					<li>Изменение организаторского процента в закупке или по каждому товару в отдельности.</li>
					<li>Интерфейс для автоматизации приема оплаты от Участников закупки.</li>
					<li>Индикация статуса и процесса сбора заказов.</li>
					<li>Формирование и печать вкладышей в пакеты заказов с подробной информацией о заказах участников закупок.</li>
					<li>Автоматический расчет суммы организаторского процента, банковского перевода и доставки.</li>
					<li>Импорт/экспорт товаров в файлы Excel.</li>
					<li>Сбор заказов рядами.</li>
					<li>Парсеры популярных интернет-магазинов для автоматической выгрузки товаров в каталоги организаторов.</li>
				</ul>
			</li>
			<br />
			<li>
				<a name="foradmins"></a>
				<span>Для администратора</span><br /><br />
				<ul>
					<li>Личный кабинет администратора совместных покупок.</li>
					<li>Формирование отчетов по организаторам и итоговым суммам покупок.</li>
					<li>Настройка максимально разрешенного организаторского процента и комиссии сайта.</li>
					<li>Баннер недавно открытых закупок на главной странице сайта.</li>
					<li>Возможность установки дополнительных бесплатных модулей, таких как портал, сервис линеечек, доска объявлений и т.д.</li>
					<li>Простой и удобный способ изменения внешнего графического дизайна сайта.</li>
					<li>Интеграция с социальной сетью ВКонтакте.</li>
				</ul>
			</li>
		</ol>
	</li>		
	<br />
	<li>
		<a name="settings"></a>
		<span>Установка и настройка</span>
		<br />
		<p>
		Перед началом установки модификации "SPmod Совместные покупки" вам необходимо иметь установленный форум SMF. Скачать его можно бесплатно на <a href="http://download.simplemachines.org/">официальном сайте Simplemachines</a>. Модуль совместных покупок устанавливается через менеджер пакетов, как и любая другая модификация для форума SMF: Админка -> Начало -> Менеджер пакетов. После установки вы будете перенаправлены на <a href="?action=admin;area=modsettings">страницу настроек</a> (Админка -> Конфигурация -> Настройка модов).
		</p>
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-settings-1.png"></div>
		<p>
			На странице настроек модификации Администратор может изменять следующие параметры:
			<ul>
				<li><b>Активизировать кнопку меню.</b> Данная настройка включает или выключает кнопку главного меню "Совместные Покупки".</li>
				<li><b>Список СП категорий.</b> Здесь укажите через запятую идентификаторы категорий форума, в которых будут создаваться темы с закупками. Если ничего не указано, то организаторы смогут создавать закупки в любом разделе форума.</li>
				<li><b>Архивный раздел.</b> Укажите идентификатор архивного раздела, куда будут перемещаться темы закупок со статусом <i>"Закрыта"</i> или <i>"Не состоялась"</i>.</li>
				<li><b>Премодерация закупок.</b> При включении данной опции все создаваемые на сайте закупки должен проверять и одобрять моератор или администратор форума. Также необходимо включить премодерации в <a href="/index.php?action=admin;area=corefeatures">общих настройках</a> форума и назначить модераторов раздела СП. 
				<li><b>Включить сквозной баннер.</b> Баннер с недавно открытыми закупками будет отображаться на каждой странице форума под главным меню.</li>
				<li><b>Идентификатор категории для баннера.</b> Укажите идентификатор категории форума, под которой будет размещен баннер с недавно открытыми закупками, как показано на картинке.<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-banner.png"></div><br /></li>
				<li><b>Максимальный процент.</b> Максимальный организаторский процент, который разрешено устанавливать вашим организаторам совместных покупок.</li>
				<li><b>Комиссия сайта.</b> Введите комиссию в процентах, которую организаторы должны отчислять администрации сайта.</li>
				<li><b>Адреса доставки.</b> Если ваши организаторы будут использовать доствку заказов почтой или транспортными компаниями, то включите эту опцию. Участники закупки смогут указывать свои адреса в личном кабинете, а организаторам будет доступна эта информации в итоговых отчетах по сформированным закупкам.</li>
				<li><b>Шаблон закупки.</b> Вы можете сами настраивать единую форму и текст закупок, которые будут создаваться на вашем форуме. В тексте шаблона используйте ключи автоподстановки, в которые будут автоматически вставляться значения, настраиваемы самими организаторами в своих личных кабинетах: <i>{title}</i> - название закупки, <i>{descr}</i> - описание закупки, <i>{name}</i> - имя организатора, <i>{phone}</i> - номер телефона, <i>{satus}</i> - статус закупки, <i>{percent}</i> - орг. процент, <i>{bank}</i> - комиссия банка, <i>{tr}</i> - транспортные расходы, <i>{trmode}</i> - способ расчета ТР, <i>{website}</i> - адрес сайта, <i>{stop}</i> - дата стопа, <i>{min}</i> - минимальная сумма заказа или минимальное количество, <i>{minmode}</i> - критерий сбора "минималки" (сумма или количество), <i>{catalogs}</i> - список каталогов, <i>{table}</i> - таблица заказов</li>
				<li><b>Список районов раздач.</b> Введите каждый район (или офис) раздач с новой строки. Участники закупок смогут выбрать один из районов в своиз личных кабинетах.</li>
			</ul>
			<br />
			Не забудьте на <a href="?action=admin;area=permissions;sa=index">странице настроек прав доступа</a> установить права для тех групп пользователей, которые смогут администрировать СП раздел, создавать или участвовать в закупках.
			<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-settings-2.png"></div>
		</p>
	</li>
	<li>
		<a name="memberlk"></a>
		<span>Личный кабинет участника закупки</span>
		<p>Для входа в Личный кабинет участника или организатора закупки можно использовать кнопку главного меню форума "Совместные покупки". Здесь для участников закупок доступно 3 страницы: <i>"Контактная информация", "Участвую в закупках", "Справка"</i>.</p>
		<p>
		<ol>
			<li>
				<a name="membercontact"></a>
				<span>Контактная информация</span>
				<p>
				На странице контактной информации участники закупок по желанию могут указать свои имя и фамилию, а также номер телефона и предпочтительный район (офис) раздачи. Эта информация будет доступна только организаторам тех закупок, в которых вы участвуете.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-settings-3.png"></div>
				</p>
			</li>
			<li>
				<a name="membersp"></a>
				<span>Участвую в закупках</span>
				<p>
				На данной странице отображается список всех закупок, в которых вы участвуете. Также выводится статус каждой закупки и расчет стоимости заказа с указанием количества, организаторского процента и транспортных расходов. В столбце "Оплата" вы можете указать сумму, которую перечислили организатору. В этом случае организатор закупки будет проинформирован о дате и сумме оплаты через свой личный кабинет, а также получит уведомление на почту.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-memberarea2.png"></div>
				</p>
			</li>
		</ol>
		</p>
	</li>
	<li>
		<a name="orglk"></a>
		<span>Личный кабинет организатора</span>
		<p>Для входа в Личный кабинет участника или организатора закупки можно использовать кнопку главного меню форума "Совместные покупки". Здесь для организатора закупок доступно следующие страницы: <i>"Контактная информация", "Каталоги", "Товары", "Закупки", "Ряды и размеры", "Парсер", "Участвую в закупках", "Справка"</i>.</p>
		<p>
		<ol>
			<li>
				<a name="orgcontact"></a>
				<span>Контактная информация</span>
				<p>
				На странице контактной информации организаторы закупок указывают свои имя и фамилию, а также номер телефона. <font color=red>Внимание! Эта информация будет опубликована в теме закупки.</font> Также организатор может быть участником закупок других организаторов. В этом случае необходимо выбрать предпочтительный район (офис) раздачи.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-orgarea-1.png"></div>
				</p>
			</li>
			<li>
				<a name="orgcatalogs"></a>
				<span>Каталоги</span>
				<p>
				На данной странице отображается список созданных организатором каталогов. Для ведения закупок каталоги не обязательны, но они делают более удобным для пользователей процесс покупок. Участники закупок могут выбирать товары из каталога не покидая ваш сайт и затем делать заказ просто нажимая на кнопку <i>"Добавить в корзину"</i>.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-orgarea-2.png"></div>
				</p>
				<p>В таблице каталогов отображается название каталогов и количество товаров в каждом из них. Кроме того вы можете удалять, редактировать или выгружать свои каталоги в XML файл, используя для этого соответствующую ссылку в таблице. Выгрузка в XML файл может потребоваться, например, если вы хотите передать свой каталог другому организатору или просто сохранить его у себя на компьютере. Сохраненный файл можно использовать в будущем для восстановления всех товаров каталога или для переноса своих каталогов на другой сайт или в другой аккаунт. Удалять каталоги можно группой. Для этого просто выберите ненужные каталоги и нажмите на кнопку <i>"Удалить выбранные каталоги"</i>.</p>
				<p>Чтобы создать новый каталог, введите его название. Если вы поставите галочку "Пользовательский каталог", то участники закупки сами смогут добавлять товары в этот каталог, указывая название, цену, ссылку на картинку и другие характеристики. В дальнейшем все добавленные таким образом позиции вы сможете проверить, отредактировать и перенести в другие свои каталоги. Для всех позиций каталога можно выбрать два набора характеристик. Это может быть размер, цвет, объем, форма и.т.д. Линейки характеристик создаются на странице <i>"Ряды и размеры"</i>. Один и тот же набор размеров, цветов и т.д. вы можете использовать для нескольких каталогов.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-catalog-2.png"></div>
				</p>
				<p>Чтобы не создавать каждую позицию вручную и не вводить информацию по каждому товару вы можете воспользоваться загрузкой каталога из CSV файла. Это удобно, когда вы получаете прайсы от поставщика в виде Excel таблиц или когда вы создаете такие таблицы самостоятельно. Чтобы воспользоваться выгрузкой необходимо, чтобы табличный файл был сохранен в кодировке Windows ANSI и содержал следующий набор столбцов: <i>"Наименование", "Артикул", "Описание", "Цена", "Картинка", "Ссылка"</i>. Наличие всех столбцов необязательно, но желательно, чтобы там содержалось название и цена товара. Обратите внимание, что имена столбцов должны точно совпадать (начинаться с большой буквы и не иметь лишних пробелов). После того, как вы измените Excel таблицу в соответствии с приведенными рекомендациями, сохарните ее как CVS файл. Разделителем при этом должна являться точка с запятой. Примеры файлов можно скачать здесь: <a href="https://yadi.sk/d/ImavJYlOkwmvk">каталог без рядов</a>, <a href="https://yadi.sk/d/ntS5WXWgkwmvr">каталог с рядами без размеров</a>, <a href="https://yadi.sk/d/3UXn1Rmrkwmvv">каталог с рядами и размерами</a>.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-catalog-3.png"></div>
				</p>
				<p>Если вы ранее сохраняли какой-либо свой каталог из таблицы, то вы можете восстановить его (или сделать его копию) используя блок "Загрузить каталог из XML файла".
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-catalog-4.png"></div>
				</p>				
			</li>
			<li>
				<a name="orggoods"></a>
				<span>Товары</span>
				<p>
				На данной странице отображается таблица товаров в выбранном каталоге с возможностью редактирования и добавлений новых позиций. 
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-orgarea-3.png"></div>
				</p>
				<p>Вы можете добавлять новые позиции, редактировать цены, описания, ссылки и т.д. А также переносить позиции из одного каталога в другой. Для этого перейдите по ссылке "Изменить" и выберите в поле "Текущий каталог" из выпадающего списка имя нового каталога для текущей позиции. Также вы можете изменить название, описание, артикул, адрес изображения и.т.д. Можно загружать свои изображения.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-goodsarea-2.png"></div>
				</p>
				<p>Если заказ данного товара происходит рядами, то укажите количество позиций в ряде. Тогда в процессе закупки контроль заказов будет осуществляться автоматически. Когда ряд будет заполнен он закроется и в случае необходимости будет открыт новый. В том случае, если для товара необходимо добавить выбор какого-либо дополнительного параметра (размер, цвет, форма, объем и т.д.), то необходимо ввести названия этого параметра в поле "Обозначение в каталоге" и перечислисть через запятую все значения параметра в поле "Возможные значения".  В данном случае резиновые сапоги имеют слебующие возможные размеры: <i>22,23,24,25,26,27</i>. Количество в ряде равное нулю означает, что сбор будет осуществляться без рядов, то есть участники закупки смогут заказать любое количество любых размеров. Если организатор хочет организоать сбор рядами, то в данном случае ему необходимо ввести значение 6 в поле <i>"Количестве в ряде"</i>, так как в возможных значениях перечислено 6 доступных для выбора размеров. Допускается указывать повторяющиеся значения в одном ряде. Например,  если в ряде есть 2 пары 24-го размера, то их нужно указать в списке следующем образом: <i>22,23,24,24,25,26,27</i>. Соответственно количество в ряде будет равно 7-ми. Существует еще один вариант закупки, когда в ряде необходимо собрирать одинаковые товары, не имеющих дополнительных параметров. В этом случае просто не заполняйте поля <i>"Обозначение в каталоге"</i> и <i>"Возможные значения"</i>.</p>
				<p>Часто в одном каталоге все товары имеют одинаковые значния этих параметров. Поэтому чтобы сократить время на изменение каждого из трех значений <i>"Количество в ряде"</i>, <i>"Обозначение в каталоне"</i> и <i>"Возможные значения"</i>, вы можете на странице <i>"Ряды и размеры"</i> создать линейку параметров и затем просто выбирать ее в поле <i>"Доп. параметр"</i>. В результате все остальные значения изменяться в соответсвие с вашей предустановкой. Также существует способ применить предустановленную линейку параметров сразу ко всему каталогу. Для этого перейдите на странице каталогов, нажмите на ссылку <i>"Изменить"</i> напротив каталога и на странице редактирования каталога в поле <i>"Линейка характеристик товаров в каталоге"</i> выберите предустановленную линейку параметров. Все значения рядов и характеристик товара в данном каталого перезапишутся.</p>
			</li>			
			<li>
				<a name="orgprocs"></a>
				<span>Закупки</span>
				<p>
				Данная страница является основной, с которой предстоит работать организатору. Здесь он создает закупки и управляет ими. 
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-orgarea-4.png"></div>
				</p>
				<p>
				В таблице "Текущие закупки" указаны все ваши закупки со статусами <i>"Открыта", "Стоп", "Ждем счет", "Оплачиваем", "Отгрузка", "Дозаказы", "Стоп. Дозаказы.", "В пути", "Приехало", "Получено"</i> или <i>"Разбираем"</i>. В таблицу "Архивные закупки" перемещаются закупки со статусом <i>"Закрыта"</i> или <i>"Не состоялась"</i>. Кнопки в столбце "Действия" позволяют менять статус или редактировать закупки.
				</p>
				<table>
				<tr>
				<td><img src="/SPMod/images/sp/edit.png" /></td><td>Кнопка "Изменить" открывает редактор закупки, где можно менять любые параметры закупки.</td>
				</tr>
				<tr>
				<td><img src="/SPMod/images/sp/stop.png" /></td><td>Кнопка "Стоп" изменяет статус закупки на "Стоп", возможность делать заказы у пользователей отключается. После нажатия на кнопку "Стоп" организатор перенаправляется в таблицу заказов.</td>
				</tr>
				<tr>
				<td><img src="/SPMod/images/sp/creditcard.png" /></td><td>Кнопка "Оплачиваем" изменяет статус закупки на "оплачиваем", публикует в теме таблицу с расчетами к оплате и перенаправляет на эту таблицу.</td>
				</tr>
				<tr>
				<td><img src="/SPMod/images/sp/closed.png" /></td><td>Кнопка "Закрыть" изменяет статус на "Закрыта" и перемещает закупку в "Архивные".</td>
				</tr>
				<tr>
				<td><img src="/SPMod/images/sp/money.png" /></td><td>Кнопка "Отчет" открывает подробный отчет по закупке с возможностью печати вкладышей для собранных пакетов.</td>
				</tr>
				</table>
				<p>
				Для создания новой закупки заполните необходимые поля в блоке <i>"Создать новую закупку"</i>, который находится ниже. А для редактирования существующей нажмите на кнопку "Изменить" в таблице.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-proc-2.png"></div>
				При редактировании или создании закупки необходимо заполнить следующие поля:
				<ul>
					<li><b>Название.</b> Придумайте и введите понятное название для вашей закупки. Также будет называться и тема на форуме.</li>
					<li><b>Описание.</b> Введите здесь краткое описание закупки с примерами предлагаемых к закупке товаров.</li>
					<li><b>Сайт поставщика.</b> Укажите адрес сайта поставщика или интернет-магазина, с которого планируется осуществить закупку. Пользователи будут переходить по указанному адресу, чтобы выбирать товары и смотреть цены.</li>
					<li><b>Дата стопа.</b> Выберите в календаре предполагаемую дату стопа сбора закупки.</li>
					<li><b>Орг. процент.</b> Выберите из выпадающего списка организаторский процент, который будет применяться для расчета окончательной стоимости заказов.</li>
					<li><b>Комиссия банка за перевод.</b> В случае если вы переводите деньги поставщику через банк, вы можете указать здесь комиссию банка за перевод средств.</li>
					<li><b>Транспортные расходы.</b> Сумма ТР будет поделена между всеми участниками закупки в соответствие с выбранным методом. Доступны 4 способа расчета транспортных расходов: <i>"Фиксированный процент", "Делим поровну", "Делим пропорционально заказу", "Делим по количеству позиций"</i>. Если выбран <i>"Фиксированный процент"</i>, то в поле необходимо ввести процент от суммы закупки. В остальных случаях вводится общая сумма ТР в рублях. При выборе способа расчета организатор должен руководствоваться особенностью товара и договоренностью с участниками закупки. Например, при закупке обуви целесообразнее ТР делить по количеству заказанных пар, так как обувь достаточно тяжелая для пересылки. При заказе легкой одежды удобнее делить пропорционально сумме заказа.</li>
					<li><b>Минимальная сумма или минимальное количество заказа.</b> Чаще всего у поставщиков есть требование на минимальную сумму заказа. В этом случае закупка состоится, если будет достигнута указанная здесь сумма. Иногда вместо суммы выдвигается требование на минимальное количество в партии. Если же в этом поле указать 0, то сумма или количество не будет учитываться и не будет отображаться индикация готовности закупки в процентах.</li>
					<li><b>Раздел СП.</b> В выпадающем списке выберите раздел, где будет создана тема для заказов из вашей закупкой.</li>
					<li>
					<b>Статус.</b> Организатор СП может менять статус закупки: 
					<ul>
						<li><i>Открыта.</i> Закупка открыта и в ней могут делать заказы участники закупки. Также возможно отказаться от ранее сделанного заказа.</li>
						<li><i>Стоп.</i> Прием заказов остановлен. Организатор проверяет наличие заказов в магазине и редактирует таблицу заказов.</li>
						<li><i>Ждем счет.</i> Заказ переслан поставщику, который формирует посылку и выставляет счет.</li>
						<li><i>Оплачиваем.</i> Участники закупки перечисляют оплату своего заказа организатору и делают отметки об оплате в личном кабинете. При изменении статуса на "Оплачиваем" все участники закупки получают уведомление через личные сообщения о необходимости произвести оплату.</li>
						<li><i>Отгрузка.</i> Поставщик готов отправить заказ организатору.</li>
						<li><i>Дозаказы.</i> Участники закупки могут добавлять позиции, но не отказываться от ранее сделанных заказах</li>
						<li><i>Стоп. Дозаказы.</i> Прием дозаказов организатором прекращен.</li>
						<li><i>В пути.</i> Заказ отправлен поставщиком на адрес организатора.</li>
						<li><i>Закрыта.</i> Закупка завершена и закрыта.</li>
						<li><i>Приехало.</i> Заказ прибыл в офис транспортной компании.</li>
						<li><i>Получено.</i> Заказ получен организатором.</li>
						<li><i>Разбираем.</i> Заказ рассортирован организатором и готов к выдачи.</li>
						<li><i>Закупка на каникулах.</i> Закупка временно приостановлена.</li>
					</ul>
					</li>
					<li><b>Баннер.</b> Организатор может указать здесь URL-адрес картинки, которая будет использоваться в качестве баннера на главной странице форума. Рекомендуемый размер изображения 200x118 точек. Адрес ссылки на изображение должен начинаться на <i>"http://"</i>.</li>
					<li><b>Каталоги.</b> Добавлять каталоги не обязательно, но при желании организатор может выбрать их из списка ранее созданных. Это сделает процесс покупки более удобным для пользователей. О том, как создаются и редактируются каталоги, см. пункт 4.2 справки.</li>
				</ul>
				</p>
				<p>
				После создания закупки в указанном организатором разделе форума автоматически появится соответствующая тема, содержащая стандартные правила, условия, список каталогов и таблицу заказов. Шаблон текста первого сообщения в такой теме стандартный для всех создаваемых на сайте закупок и может редактироваться администратором.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-proc-1.png"></div>
				</p>
				<p>
				Организатор может редактировать созданную тему, как свою тему на форуме, добавляя свои условия или редактируя стандартный текст. Также он может изменять любые условия закупки через личный кабинет: название, описание, адрес сайта и т.д. Все изменения будут отражены и в теме закупки.
				</p>
				<p>
				Внизу страницы "Закупки" расположен блок "Код вставки". Данный BB код можно использовать для рекламы своих закупок в своей подписи (или в любом другом сообщении на форуме). 
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-orgarea-5.png"></div>
				</p>
				<p>
				Просто скопируйте этот код и вставьте в подпись в профиле своего аккаунта. Тогда список закупок, которые ведет организатор будет отображаться под каждым его сообщением. Таким образом он сможет рекламировать свои покупки просто общаясь на форуме с другими участниками. Допускается использовать и другие BB коды, чтобы изменять цвет или размер шрифта данной подписи.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-orgarea-6.png"></div>
				</p>
				<p>
				В любой момент организатор может посмотреть отчет по своим закупкам. Для этого в личном кабинете организатора на странице "Закупки" перейдите по ссылке "Отчет" в таблице закупок. Откроется список участников вашей закупки с указанием количества и расчетом стоимости их заказов.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-report.png"></div>
				</p>
				<p>Иногда может потребоваться изменить значение орг. процента только для одного определенного участника закупки. Например, если в условиях сказано, что при заказе больше определенной суммы, организаторский процент снижается, то организатор может воспользоваться этой формой и уменьшить орг. процент для тех участников, которые выполнили это условие.</p>
				<p>Также в отчете отображается информация о дате и сумме произведенной оплаты. Если участник закупки не указал в своем личном кабинете факт оплаты, то это может сделать и организатор.</p>
				<p>Внизу таблицы участников закупки отображается итоговая информация об общей сумме заказа, рассчитывается комиссия сайта и прибыль организатора.</p>
				<p>В блоке "Код вставки" содержится BB код отчета (без имен и номеров телефонов) для вставки в тему закупки. В сообщении данный код отображается как таблица с информацией о суммах к оплате.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-payment.png"></div></p>
				<p>С помощью кнопки "Печать" организатор может напечатать вкладыши для пакетов с заказами, на которых будут указаны ник участника закупки, имя организатора, район раздачи, перечень всех позиций, перечисленная и оставшаяся сумма к оплате. 
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-print.png"></div></p>
				</p>
			</li>	
			<li>
				<a name="orgparameters"></a>
				<span>Ряды и размеры</span>
				<p>
				На странице параметров можно указывать размеры, цвета, формы и другие параметры товаров. 
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-orgarea-7.png"></div>
				</p>
				<p>Добавьте новую линейку параметров, указав название, обозначение в каталоге и перечислив через запятую все возможные значения данного параметра. По названию будет осуществляться связь этого параметра с каталогом или с каким-либо товаром в отдельности. Как это сделать описано в пунктах 4.2 и 4.3 справки. Обозначение в каталоге - это слово, которое будет видеть пользователь, делая выбор в каталоге. Например, если ему нужно выбрать размер, то введите в это поле слово "Размер". При этом в поле "Возможные значение" перечислите через запятую все размеры, доступные для выбора. Используя этот набор значений будет сформирован выпадающий список, из которого пользователь будет выбирать подходящий размер, делая выбор в каталоге. Аналогично размерам можно задавать наборы значений такие как цвет, объем, длина, форма и т.д.</p>
			</li>
			<li>
				<a name="orgparser"></a>
				<span>Парсер</span>
				<p>
				Парсер - это программа, предназначенная для автоматического наполнения каталогов товаров используя сайты поставщиков. <font color=red>Внимание! Работа парсеров зависит от сторонних сайтов и в случае каких-либо изменений на этих сайтах, парсер может стать неработоспособным.</font>
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-orgarea-8.png"></div>
				</p>
				<p>С помощью парсера вы можете выгружать товары в существующие или новые каталоги напрямую из интернет-магазинов или с сайтов поставщиков. На данный момент доступны парсеры следующих магазинов: 
				<ul>
					<li><a href="http://sima-land.ru" target="_blank">Сима-ленд</li></a>
					<li><a href="http://stok-m.ru" target="_blank">Сток Маркет</li></a>
					<li><a href="http://natali37.ru" target="_blank">Натали</li></a>
					<li><a href="http://votonia.ru" target="_blank">ВотОнЯ</li></a>
					<li><a href="http://sp-savage.ru" target="_blank">Savage</li></a>
					<li><a href="http://volgoshoes.ru" target="_blank">Волгошуз</li></a>
					<li><a href="http://odezhda-master.ru" target="_blank">Одежда Мастер</li></a>
					<li><a href="http://байрон.рф" target="_blank">Байрон</li></a>
					<li><a href="http://ikea.com/ru/ru" target="_blank">Икея</li></a>
				</ul>
				</p><p>Скопируйте и вставьте в поле "Список URL-адресов" ссылки на страницы с товарами. Каждый адрес должен начинаться с новой строки. Тип парсера будет определен автоматически, причем при необходимости в один каталог можно выгружать товары из разных магазинов. Если в выбранном каталоге уже есть товары, то в него будут добавлены новые позиции. В результате работы парсера вы будете перенаправлены на страницу заполненного новыми товарами каталога.</p>
				<p>Вместо списка URL адресов при необходимости вы можете вставить искодхный код страницы с товарами. В этом случае тип парсера также определится автоматически. Чтобы скопировать html код нажмите правой кнопкой мыши на странице сайта магазина, выберите пункт "Просмотр кода страницы", скопируйте весь текст исходного кода в открывшемся окне и вставьте в поле "Список URL-адресов". Далее нажмите кнопку "Выполнить". Использование исходного кода вместо URL адресов может оказаться полезным, когда первый способ по каким-либо причинам не работает. Или, например, когда на сайтах некоторых магазинов отображаются актуальные цены только после авторизации. В этом случае также лучше использовать выгрузку через исходный код страницы, так как она будет содержать правильные цены. 
				</p>
			</li>			
			<li>
				<a name="orgsp"></a>
				<span>Участвую в закупках</span>
				<p>
				На данной странице отображается список всех закупок, в которых вы делаете закупки, как участник закупки. Также выводится статус каждой закупки и расчет стоимости заказа с указанием количества, организаторского процента и транспортных расходов. В столбце "Оплата" вы можете указать сумму, которую перечислили организатору. В этом случае организатор закупки будет проинформирован о дате и сумме оплаты через свой личный кабинет, а также получит уведомление на почту.
				<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-memberarea2.png"></div>
				</p>
			</li>			
		</ol>
		</p>	
	</li>
	<li>
		<a name="admin"></a><b>Личный кабинет администратора СП</b>
		<p>		
		На странице "Администрирование СП" администратор может запросить список всех закупок на сайте за указанный диапазон дат. По умолчанию отображаются закупки за последний месяц. По каждой закупке доступны детальные отчеты. 
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-adminarea-1.png"></div>		
		</p>
	</li>	
	<li>
		<a name="order"></a><b>Как делать заказы</b>
		<p>		
		Для начала войдите в раздел совместных закупок форума и выберите интересующую вас закупку. Возле каждого названия темы вы увидите статус закупки и уровень собранности в процентах.
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-buy-1.png"></div>		
		</p>
		<p>
		Зайдя в тему вы увидите описание условий закупки, список каталогов и таблицу заказов.  
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-proc-1.png"></div>
		</p>
		<p>
		Выберите каталог и перейдите по ссылке. Откроется страница с товарами данного каталога. Вы можете найти интересующий вас товар по названию или по артикулу воспользовавшись стройкой поиска в верхней части экрана.  
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-buy-2.png"></div>
		</p>
		<p>
		Просмотрите каталог и определитесь с желаемой покупкой. Выберите требуемый вам размер или цвет и укажите количество. После нажатия на кнопку "Положить в корзину" товар добавится в таблицу покупок. А организатор закупки получит уведомление на почту о новом заказе в его закупке.
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-buy-3.png"></div>
		</p>
		<p>
		Если в закупке нет каталогов, то вы можете совершить покупку через строку заказа в нижней части таблицы. Просто введите адрес ссылки на страницу товара (не обязательно), во втором столбце укажите название покупки, ее размер, цвет и другие характиристики (если требуется) и цену. И нажмите кнопку "Заказать".
		</p>
		<p>
		Кроме этого, если вы увидите в таблице какой-либо товар, заказанный другим участником закупки, то вы также можете его заказать нажав на ссылку "И мне тоже нужно". 
		</p>		
	</li>	
	<li>
		<a name="vkontakte"></a><b>Интеграция с социальной сетью ВКонтакте</b>
		<p>	<a href="https://new.vk.com/editapp?act=create">Создайте</a> 2 приложения ВКонтакте. Выберите тип первого приложения "IFrame/Flash", введите название "Совместные покупки". Тип второго приложения "Веб-сайт", а в качестве названия можно использовать адрес вашего сайта. Первое приложение IFrame свяжите с вашим сообществом ВКонтакте и введите для него следующие настройки.
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/spset-vk-1.png"></div>			
		</p>
		<p>	Настройте второе приложение, как показано на картинке ниже. На вашем сайте в настройках модификации введите ID приложения и Защищенный ключ из этого приложения	"Веб-сайт".
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/spset-vk-2.png"></div>
		</p>
		<p>		
		Участники закупок могут делать заказы прямо на сайте ВКонтакте. Все заказы хранятся и отображаются в единой таблице, организатору доступен общий отчет по всем заказам, сделанным как на сайте, так и ВКонтакте.
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-vk-1.png"></div>		
		<div class="sp_pic"><img alt="SPMod settings" src="/SPMod/images/sp/sphelp-vk-2.png"></div>
		</p>
	</li>		
	<li>
		<a name="buy"></a><b>Купить модификацию</b>
		<p>
		Бесплатную версию модификации совместных покупок можно скачать с <a href="http://custom.simplemachines.org/mods/index.php?mod=4109">официального сайта SMF</a>.
		Чтобы купить платную версию, напишите заявку на адрес sale@spmod.ru с указанием адреса сайта, где вы планируете использование данного мода. Стоимость полной лицензии составляет <b>10 тыс. р.</b> и включает в себя техническую поддержку и все дальнейшие обновления. Возможна рассрочка на 5 месяцев.	
		</p>
	</li>	
</ol>
</div>
';

// Admin Area
$txt['spmod_admin_area_desc'] = 'Здесь собраны все настройки относящиеся к системе Совместных Покупок.';
$txt['spmod_admin_area_settings'] = 'Настройки';
$txt['spmod_admin_area_settings_desc'] = 'Общие настройки cовместных покупок.';
$txt['spmod_admin_area_banner'] = 'Баннеры';
$txt['spmod_admin_area_banner_desc'] = 'Настройки отображения баннеров и слайдера.';
$txt['spmod_admin_area_vkontakte'] = 'Приложение ВКонтакте';
$txt['spmod_admin_area_vkontakte_desc'] = 'Настройки приложения ВКонтакте.';
$txt['spmod_admin_area_beget'] = 'Beget';
$txt['spmod_admin_area_beget_desc'] = 'Настройки хостинга Beget для выполнения cron-задач.';
$txt['spmod_admin_area_permissions'] = 'Права доступа';
$txt['spmod_admin_area_permissions_desc'] = 'Настройки прав доступа групп пользователей к функциям СП.';
$txt['spmod_admin_area_info'] = 'Информация и поддержка';
$txt['spmod_admin_area_info_desc'] = 'Дополнительная информация о системе и версиях программного обеспечения.';

// Admin Area: Info
$txt['spmod_admin_area_version_spmod'] = 'Версия SPmod';
$txt['spmod_admin_area_version'] = 'Версия';
$txt['spmod_admin_area_version_curl'] = 'Версия PHP Curl';
$txt['spmod_admin_area_forum_url'] = 'Адрес форума';
$txt['spmod_admin_area_forum_protocol'] = 'Веб-протокол';
$txt['spmod_admin_area_forum_version'] = 'Версия форума';
$txt['spmod_admin_area_forum_theme'] = 'Тема оформления';
$txt['spmod_admin_area_forum_lang'] = 'Язык форума';
$txt['spmod_admin_area_forum_utf8'] = 'Используется UTF-8';
$txt['spmod_admin_area_phpinfo'] = 'Показать все настройки PHP';

?>